import functools

from flask import abort, request, session

from refrigerator.utils.time import check_date_format

def make_response(status_code=200, success=True, data={}, errors=[]):
  response = {}
  if success:
     response['success'] = True
     response['data'] = data
  else:
     response['success'] = False
     response['errors'] = errors
  return response, status_code


def type_check_errors(received_dict, query_string=False, **kwargs):
	'''
	Check to see if all kwargs match their counterparts in received_dict.
	Does not require all kwargs to be present in received_dict.
	You can join this list to the list of errors for your response.
	Returns a list:
		If all checks pass, return []
		If any checks fail, return a handy list of errors [
			f"Incorrect format for {field}. Expected {required_type}.",
			...
		]

	Option: query_string:
		Use query_string switch if you are passing in request.args.
		Type checking is not as strong for query strings.
		For example, almost any value passed to a boolean field will be accepted.
		Mostly useful for integers.
	'''
	errors = []
	for field, required_type in kwargs.items():
		if field in received_dict:
			#If we get a query_string
			#just try to cast it to the specified type.
			if query_string:
				try:
					required_type(received_dict[field])
				except:
					errors.append(f"Incorrect format for {field}. Expected {required_type}.")
			#Otherwise, do a more rigorous type check.
			elif not type(received_dict[field]) == required_type:
				errors.append(f"Incorrect format for {field}. Expected {required_type}.")
	return errors

def validate_data(constraints=(), date_fields=(), query_string=False, required_fields=(), **kwargs):
	"""
	Wrapper to validate the data from an API request.
	If any checks fail, calls `abort(400)` and supplies all generated errors.

	Basic use case is to type-check the values in request_dict against **kwargs.

	:params kwargs: Key/value pairs to type check.
		Formatted like `key=required_type`,
		where `key` is the name of a supported parameter in request_dict,
		and `required_type` is a Python built-in type object (like "str").

	:param constraints: Tuple of 3-length Tuples like (constrained_field, validator_function, error_description):
		`constrained_field` is a key that appears in `kwargs`. *Note*: if not in `kwargs`, will not be checked.
		`validator_function` is a function that:
			- takes one argument: `request.json[constrained_field]` (or `request.args[constrained_field]` if `query_string`)
			- returns a boolean
		`error_description` is the error message that will be added to the list of errors if the check fails.
	:param date_fields: Tuple of fields that should match `refrigerator.utils.time_utils.DATE_FORMAT`.
	:param query_string: Set to True if you want to validate request.args.
		Type checking is not as strong for query strings.
		For example, almost any value passed to a boolean field will be accepted.
		Mostly useful for integers.
	:param required_fields: Tuple of required fields.
	"""
	wrapper_kwargs = kwargs
	def outer(route):
		@functools.wraps(route)
		def inner(*args, **kwargs):
			request_dict = request.args if query_string else request.json
			errors = []
			for key, required_type in wrapper_kwargs.items():
				if key in request_dict:
					# If we get a query_string
					# just try casting to the required type.
					if query_string:
						try:
							required_type(request_dict[key])
						except:
							errors.append(f"Incorrect format for {key}. Expected {required_type}.")

					# Otherwise, make sure it's the exact type requested.
					elif not type(request_dict[key]) == required_type:
						errors.append(f"Incorrect format for {key}. Expected {required_type}.")

					# If the key is in date_fields, make sure it matches our required format.
					if key in date_fields:
						if not check_date_format(request_dict[key]):
							errors.append(f"Incorrect format for {key}. Should be like 'Oct 21, 1929'.")

					# Only check the constraints if the type checks pass.
					# Otherwise we might get unexpected results from the validator function.
					if not errors:
						for c in constraints:
							# Each constraint is a Tuple of 3 items:
							# (constrained_field, validator_function, error_description)
							if key == c[0]:
								# Cast to required type before validation in case it's a query string.
								if not c[1](required_type(request_dict[key])):
									errors.append(c[2])

				else:
					# If the key is not in request_dict, make sure it's not required.
					if key in required_fields:
						errors.append(f"Required Field {key} Not Provided.")

			if errors:
				abort(400, errors)

			return route(*args, **kwargs)

		return inner

	return outer

def require_authentication(route):
	'''
	Wrapper for routes that require authentication.
	If there is no active user session, return a 401 "Unauthorized" response.
	'''
	@functools.wraps(route)
	def wrapped_route(**kwargs):
		if not 'user_id' in session:
			errors = ["Unauthorized.", ]
			success = False
			status_code = 401
			data = {}
			return make_response(status_code, success, data, errors)

		return route(**kwargs)

	return wrapped_route

def owns_resource(resource):
	'''
	Wrapper for routes to a specific resource.
	Ensures that the resource is owned by the logged-in user.

	resource is a db.Model that has a user_id column.

	The wrapped function must take a keyword argument that matches {tablename}_id,
	where tablename is the value of resource.__tablename__

	If the logged-in user does not own the resource, aborts with a 401.
	If resource does not exist with the requested id, aborts with a 404.
	'''
	def outer(route):
		@functools.wraps(route)
		def inner(**kwargs):
			#Make sure the resource has a user_id column.
			if not getattr(resource, "user_id", None):
				raise TypeError("refrigerator.utils.response_utils.owns_resource() requires the resource to contain a user_id column.")

			#Get the id of the resource from the wrapped function kwargs.
			for k, v in kwargs.items():
				if k == f"{resource.__tablename__}_id":
					resource_id = v
					break
			else:
				raise TypeError("refrigerator.utils.response_utils.owns_resource() requires the wrapped function to have a keyword argument that matches {resource.__tablename__}_id")

			#Get the resource. This will abort with a 404 if the resource does not exist.
			res = resource.query.get_or_404(resource_id, description="Fave Not Found.")

			#Abort with a 401 if the user does not own the resource.
			if session['user_id'] != res.user_id:
				abort(401, "Unauthorized.")

			#Return the wrapped route if all checks pass!
			return route(**kwargs)

		return inner

	return outer
