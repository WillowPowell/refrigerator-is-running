import React from 'react';
import PropTypes from 'prop-types';

import Field from './helpers/Field';

/**
* Accepts the same props as `Field`, but defaults appropriate values.
*/
export default function NumberInput({
  value, // React state getter
  setValue, // React state setter
  name,
  label,
  placeholder,
  min,
  borderColor,
  iconLeft,
  iconRight,
  ...props // Additional props to pass into the `input` tag, like `required`.
}) {
  return (
    <Field
      name={name}
      label={label}
      iconLeft={iconLeft}
      iconRight={iconRight}
    >
      <input
        value={value}
        onChange={(e) => setValue(parseInt(e.target.value, 10))}
        name={name}
        id={`${name}-input`}
        type="number"
        min={min}
        className={`input${borderColor ? ` is-${borderColor}` : ''}`}
        placeholder={placeholder}
        {...props}
      />
    </Field>
  );
}

NumberInput.propTypes = {
  value: PropTypes.number.isRequired,
  setValue: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  borderColor: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(null),
  ]),
  iconLeft: PropTypes.string,
  iconRight: PropTypes.string,
  min: PropTypes.number,
};

NumberInput.defaultProps = {
  borderColor: 'primary',
  iconLeft: null,
  iconRight: null,
  placeholder: '42',
  min: 0,
};
