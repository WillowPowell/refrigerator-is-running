# Setting up for Development

After you've cloned the repo, you have a few steps to get set up for back end development.

## Install Dependencies
This project uses `pipenv` to manage virtual environments. To install dependencies, run:

```bash
$ cd flask-api
$ pipenv install --dev
```

## Populate environment variables
Copy the `.env.example` file and rename it to `.env`. Populate the variables in the file and they will be loaded by `dotenv` when you run in development.

### DATABASE_URL
You can use any format supported by [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/#connection-uri-format).

The fastest way to set up for development is to use:

```
sqlite:////local/path/to/project/flask-api/instance/refrigerator.db
```

### Twitch Credentials
The Refrigerator connects to [IGDB](https://api-docs.igdb.com/) for all video game data. IGDB uses Twitch as its auth provider, so you need to set up a Twitch account and register an application in order to get the keys you need.

1. Make an account at <https://www.twitch.tv/user/signup>
2. Set up 2-factor authentication at <https://www.twitch.tv/settings/security>
3. Go to <https://dev.twitch.tv/console/apps/create> to register an application. Use the following settings:
  * Name: `$AnyNameWorks!`
  * OAuth Redirect URLs: `http://localhost`
  * Category: "Application Integration"
4. Click "Manage" for your new application.
5. Copy the Client ID to your `.env` file.
6. Click "New Secret" and copy the Client Secret to your `.env` file.
7. Rejoice!

### Email Server Variables
The Refrigerator sends error logs via SMTP. In the future, this will also be used for email address verification and password reset.

Fill out your mail server credentials in the `.env`.

### Flask Variables
These variables must be set before starting the app, not in the `.env` file.

```bash
$ export FLASK_APP=refrigerator FLASK_ENV=development
$ pipenv run flask run
```

Alternatively, you can use a [.flaskenv](https://flask.palletsprojects.com/en/1.1.x/cli/#environment-variables-from-dotenv) file to make it more convenient.

## Apply Database Migrations
Migrations should be run when you're first setting up, or any time there are changes to the SQLAlchemy models.
```bash
$ pipenv run flask db upgrade
```
## Create New Migration
If you make a change to any of the models, you need to create a new migration.
```bash
$ pipenv run flask db migrate -m "Made excellent change"
```
The generated file should be edited if necessary, and commited to version control.

## Start your development environment
```bash
$ pipenv run flask run
```
The development server should start, and you should see logging information telling you that the app started successfully. If you navigate to http://localhost:5000 you should get the apt response "Welcome to the Refrigerator"!

## Run the tests
The Refrigerator maintains an extensive test suite of the entire API.
```bash
$ pipenv run pytest
```
The whole suite of tests should be able to run if you've completed the configuration steps above. The `pytest-dotenv` plugin is used, so the appropriate variables are loaded in for the tests.

If you did not set up the Twitch credentials yet, some of the tests will fail. There is a single test file that actually interacts with the Twitch API to make sure that connectivity is up and running. All of the other tests have the API connection stubbed out with the `monkeypatch` fixture.

## Test Coverage
The Refrigerator maintains a very high test coverage percentage. At the time this is being written, it sits at 99%.

After making changes, run:

```bash
$ pipenv run coverage run -m pytest`
$ pipenv run coverage report > tests/coverage.txt
```

The Refrigerator team will not approve merge requests that lower the test coverage.

# Deploying to Production
The back end is deployed in production on Heroku [here](https://desolate-reef-45627.herokuapp.com/). It is deployed independently from the front end, and the front end proxies api requests to the back end as necessary.

The following instructions walk you through creating a new Refrigerator on Heroku.

## Creating the app
Assuming you have `heroku` installed, and have an account that's logged in, run:
```bash
$ heroku create -a my-refrigerator-backend -r heroku-backend
```
### Flags:
* -a: The app name that will be created in your Heroku account.
* -r: The name of the git remote that will be added.

If you change these flags, modify the commands below appropriately.

## Deploying code
Since the front end and back end are housed in the same repo, you need to use `git subtree push` to deploy code and make updates:

```bash
$ git subtree push --prefix flask-api heroku-backend main
```

To add flags when pushing:

```bash
$ git push heroku-backend `git subtree split --prefix flask-api main`:main --flag
```

replacing `--flag` with the desired flags.

## Setting Environment Variables
Follow the instructions in the [Heroku documentation](https://devcenter.heroku.com/articles/config-vars) to set the following environment variables.

### SECRET_KEY
To generate a good secret key check out the [Flask documentation](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY)

### TWITCH_CLIENT_ID & TWITCH_CLIENT_SECRET
You should set up a new application in the Twitch Developer account for production. Follow the same instructions as above for setting up an application and obtaining the required keys.

> **Note:** The `OAuth Redirect URL` is a required field in the Twitch console in order to create a new app. However, The Refrigerator uses the [OAuth Client Credentials Flow](https://dev.twitch.tv/docs/authentication/getting-tokens-oauth#oauth-client-credentials-flow), which does not make use of a redirect URL.
>
> In the future, additional interaction with the Twitch API (like adding authentication via OpenID Connect) may necessitate adding a different redirect URL when deploying to production.

### DATABASE_URL
Add the `heroku-postgresql` addon to the app, and the `DATABASE_URL` variable will be set automatically. The format provided is deprecated by SQLAlchemy, but there's workaround implemented in `config.py`.

### MAIL variables
You can use one of the Heroku email add-ons, or any other email provider that supports SMTP. If you use an add-on, the variable names will not match what the app needs, so you will still have to populate them manually.

### Other variables
* FLASK_APP=refrigerator
* FLASK_ENV=production
* INFO_LOG=STREAM

INFO_LOG must be set to STREAM because Heroku's ephemeral file system might delete log files at any time.

## Access Production Logs:
```bash
$ heroku logs -a my-refrigerator-backend
```

Add the `--tail` flag to monitor the logs in real-time.

## Rejoice
Don't forget to rejoice in your accomplishments, and, above all: **Enjoy your new Refrigerator!**

# Linux Deployment on DigitalOcean
Set up the server like DigitalOcean says.

## Switch to `venv`
Yeah, pipenv is great and all, but show me a single tutorial that uses it.

```
pipenv install
pipenv run pip freeze > requirements.txt
```

Now you can `venv` like the tutorials say.

Make sure you install the refrigerator:

```
source venv/bin/activate
pip install -r requirements.txt
pip install .
```

## Gunicorn command
```
gunicorn -b 0.0.0.0:5000 -w 4 "refrigerator:create_app()"
```

```
ExecStart=/home/home/willow-janet/refrigerator-is-running/flask-api/venv/bin/gunicorn --workers 4 --bind unix:refrigerator.sock -m 007 "refrigerator:create_app()"
```
## service file

```
[Unit]
Description=Gunicorn instance serving refrigerator api.
After=network.target

[Service]
User=willow-janet
Group=www-data
WorkingDirectory=/home/willow-janet/refrigerator-is-running/flask-api
Environment="PATH=/home/willow-janet/refrigerator-is-running/flask-api/venv/bin" "GUNICORN_CMD_ARGS="-- workers 4 --bind unix:refrigerator.sock -m 007"
ExecStart=/home/willow-janet/refrigerator-is-running/flask-api/venv/bin/gunicorn "refrigerator:create_app()"

[Install]
WantedBy=multi-user.target
```
