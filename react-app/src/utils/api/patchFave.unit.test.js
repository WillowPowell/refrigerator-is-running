import fetchMock from 'fetch-mock-jest';

import { withCover as fave } from 'fixtures/fave';

import getFaves from './getFaves';
import patchFave from './patchFave';

jest.mock('./getFaves');

const faveIndex = 0;
const favesDispatch = jest.fn();
const newPhrase = 'It is better than I even thought!';
const updatedFave = {
  ...fave,
  phrase: newPhrase,
};

class PatchFaveMocker {
  constructor(targets) {
    this.matcher = {
      url: `${process.env.REACT_APP_API_URL}/api/faves/${fave.id}`,
      method: 'PATCH',
      body: targets,
    };
    this.successResponse = {
      body: {
        success: true,
        data: updatedFave,
      },
      status: 200,
    };
    this.failResponse = {
      body: {
        success: false,
        errors: ['Unauthorized'],
      },
      status: 401,
    };
  }
}

// Teardown
afterEach(() => fetchMock.restore());

it('calls the api and returns the data', async () => {
  const patchFaveMocker = new PatchFaveMocker({ phrase: newPhrase });
  fetchMock.mock(patchFaveMocker.matcher, patchFaveMocker.successResponse);
  expect.assertions(2);
  // Act
  const [response, responseJSON] = await patchFave(
    fetch,
    favesDispatch,
    fave.id,
    faveIndex,
    {
      phrase: newPhrase,
    },
  );
  // Assert
  expect(response.ok).toBe(true);
  expect(responseJSON.data).toEqual(updatedFave);
});

it('calls getFaves on success', async () => {
  const patchFaveMocker = new PatchFaveMocker({ phrase: newPhrase });
  fetchMock.mock(patchFaveMocker.matcher, patchFaveMocker.successResponse);
  expect.assertions(1);
  // Act
  await patchFave(
    fetch,
    favesDispatch,
    fave.id,
    faveIndex,
    {
      phrase: newPhrase,
    },
  );
  // Assert
  expect(getFaves).toHaveBeenCalledWith(favesDispatch, fetch);
});

it('does not call getFaves on failure', async () => {
  const patchFaveMocker = new PatchFaveMocker({ phrase: newPhrase });
  fetchMock.mock(patchFaveMocker.matcher, patchFaveMocker.failResponse);
  expect.assertions(1);
  // Act
  await patchFave(
    fetch,
    favesDispatch,
    fave.id,
    faveIndex,
    {
      phrase: newPhrase,
    },
  );
  // Assert
  expect(getFaves).not.toHaveBeenCalled();
});
