import faveShape from './faveShape';
import faveWizardCacheShape from './faveWizardCacheShape';
import gameShape from './gameShape';

export {
  faveShape,
  faveWizardCacheShape,
  gameShape,
};
