module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
    'plugin:jest/recommended',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'import',
    'jest',
    'react-hooks',
    'jsx-a11y',
  ],
  rules: {
    'no-alert': 'off',
    'react/jsx-filename-extension': 'off',
    'react/jsx-props-no-spreading': ['error', { html: 'ignore' }],
    'react/jsx-no-undef': ['error', { allowGlobals: true }],
    'import/no-extraneous-dependencies': [
      'error',
      { devDependencies: ['**/*.test.js', '**/setupTests.js'] },
    ],
    'jest/expect-expect': [
      'error',
      { assertFunctionNames: ['expect', 'expectComponentMounted'] },
    ],
  },
  settings: {
    'import/resolver': {
      node: {
        paths: ['src'],
      },
    },
  },
};
