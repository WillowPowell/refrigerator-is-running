export default function reduceErrors(errors) {
  return errors.reduce((error, acc) => `${acc}\n\n${error}`);
}
