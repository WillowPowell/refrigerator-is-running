from flask import session

from tests.utils import check_response


#Helpers
def logout(client):
	return client.delete("/api/sessions")


#Tests
def test_success(logged_in_client):
	"""
	If there is an active session,
	should get a 200 response and the session should be deleted.
	"""
	with logged_in_client as c:
		response = logout(c)
		assert not session
		check_response(response, 201)


def test_no_session(client):
	"""`client` fixture is not logged in, so we expect a 404."""
	response = logout(client)
	check_response(response, 404, success=False, error="Not Found.")
