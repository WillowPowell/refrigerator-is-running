import fetchMock from 'fetch-mock-jest';

import { withCover as fave } from 'fixtures/fave';

import deleteFave from './deleteFave';
import getFaves from './getFaves';

jest.mock('./getFaves');

const favesDispatch = jest.fn();

class DeleteFaveMocker {
  constructor() {
    this.matcher = {
      url: `${process.env.REACT_APP_API_URL}/api/faves/${fave.id}`,
      method: 'DELETE',
    };
    this.successResponse = {
      body: {
        success: true,
        data: fave,
      },
      status: 200,
    };
    this.failResponse = {
      body: {
        success: false,
        errors: ['Unauthorized'],
      },
      status: 401,
    };
  }
}

// Teardown
afterEach(() => fetchMock.restore());

it('calls the api and returns the data', async () => {
  const deleteFaveMocker = new DeleteFaveMocker();
  fetchMock.mock(deleteFaveMocker.matcher, deleteFaveMocker.successResponse);
  expect.assertions(2);
  // Act
  const [response, responseJSON] = await deleteFave(
    fetch,
    favesDispatch,
    fave.id,
  );
  // Assert
  expect(response.ok).toBe(true);
  expect(responseJSON.data).toEqual(fave);
});

it('calls getFaves on success', async () => {
  const deleteFaveMocker = new DeleteFaveMocker();
  fetchMock.mock(deleteFaveMocker.matcher, deleteFaveMocker.successResponse);
  expect.assertions(1);
  // Act
  await deleteFave(
    fetch,
    favesDispatch,
    fave.id,
  );
  // Assert
  expect(getFaves).toHaveBeenCalledWith(favesDispatch, fetch);
});

it('does not call getFaves on failure', async () => {
  const deleteFaveMocker = new DeleteFaveMocker();
  fetchMock.mock(deleteFaveMocker.matcher, deleteFaveMocker.failResponse);
  expect.assertions(1);
  // Act
  await deleteFave(
    fetch,
    favesDispatch,
    fave.id,
  );
  // Assert
  expect(getFaves).not.toHaveBeenCalled();
});
