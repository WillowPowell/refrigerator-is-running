const withCover = {
  cover_height: 1871,
  cover_url: 'https://images.igdb.com/igdb/image/upload/t_thumb/co1p92.jpg',
  cover_url_big: 'https://images.igdb.com/igdb/image/upload/t_cover_big/co1p92.jpg',
  cover_width: 1403,
  created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
  first_release_date: 'Oct 29, 2009',
  id: 104,
  igdb_id: 2136,
  igdb_url: 'https://www.igdb.com/games/bayonetta',
  name: 'Bayonetta',
  summary: 'A member of an ancient witch clan and possessing powers beyond the comprehension of mere mortals, Bayonetta faces-off against countless angelic enemies, many reaching epic proportions, in a game of 100% pure, unadulterated all-out action. Outlandish finishing moves are performed with balletic grace as Bayonetta flows from one fight to another. With magnificent over-the-top action taking place in stages that are a veritable theme park of exciting attractions, Bayonetta pushes the limits of the action genre, bringing to life its fast-paced, dynamic climax combat.',
  twitch_id: 246130,
  updated_at: null,
};

export default withCover;
