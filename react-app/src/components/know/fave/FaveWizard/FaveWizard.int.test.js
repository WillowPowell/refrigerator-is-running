/* global customRender */
import React from 'react';
import { act } from 'react-dom/test-utils';
import { screen, waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { addFave, getFaves, searchGames } from 'utils/api';

import { withCover as fave } from 'fixtures/fave';
import {
  searchResults,
  withCover as game,
  noCover as gameWithConflict,
} from 'fixtures/game';
import { registered } from 'fixtures/user';

import FaveWizard from '.';

jest.mock('utils/api');

const goodQuery = 'Bayonetta';
const badQuery = 'Jiminy';
const hardCodedSearchLimit = 10;
const hardCodedSearchOffset = 0;

beforeEach(() => {
  searchGames.mockImplementation(async (query, limit, offset) => {
    const response = { ok: false };
    const responseJSON = { errors: ['mock error'] };
    if (limit === hardCodedSearchLimit && offset === hardCodedSearchOffset) {
      if (query === goodQuery) {
        response.ok = true;
        responseJSON.data = searchResults;
      } else if (query === badQuery) {
        responseJSON.errors = ['Not Found'];
      }
    }
    return [response, responseJSON];
  });
  addFave.mockImplementation(async (_authFetch, gameId, topFave, phrase, hoursPlayed) => {
    const response = {};
    const responseJSON = {};
    const validCall = (
      typeof _authFetch === 'function'
        && gameId === game.id
        && topFave === fave.top_fave
        && phrase === fave.phrase
        && hoursPlayed === fave.hours_played
    );
    if (validCall) {
      response.ok = true;
      responseJSON.data = fave;
    } else if (gameId === gameWithConflict.id) {
      response.ok = false;
      responseJSON.errors = ['Conflict.'];
    }
    return [response, responseJSON];
  });
  customRender(<FaveWizard />, {
    authState: { user: registered.data },
    favesState: {},
    faveWizardState: {},
  });
});

function openModal() {
  userEvent.click(screen.getByRole('button', { name: 'Add Fave' }));
  return waitFor(() => expect(screen.getByRole('dialog', { name: 'Add Fave Wizard' })).toHaveClass('is-active'));
}

function inputQuery(str) {
  userEvent.type(screen.getByRole('searchbox', { name: 'Search' }), str);
}

async function selectGameAndContinue(myGame) {
  userEvent.click(screen.getByRole('row', {
    name: new RegExp(`^[0-9]+. ${myGame.name}$`),
  }));
  await waitFor(() => expect(screen.getByRole('button', { name: 'Continue' })).not.toHaveAttribute('disabled'));
  userEvent.click(screen.getByRole('button', { name: 'Continue' }));
  return waitFor(() => expect(screen.getByRole('banner', { name: 'Add A Fave: Step 2' })).toBeInTheDocument());
}

async function submitGameSearch() {
  const goButton = screen.getByRole('button', { name: 'Go' });
  userEvent.click(goButton);
  await waitFor(() => expect(goButton).not.toHaveClass('is-loading'));
  return waitFor(() => expect(screen.getByRole('table', { name: 'Search Results' })).toBeInTheDocument());
}

function fillOutFaveFields(phrase, myGame) {
  userEvent.type(screen.getByRole('textbox', { name: `Why do you love ${myGame.name}?` }), phrase);
  userEvent.type(screen.getByRole('spinbutton', { name: 'Hours Played' }), String(fave.hours_played));
}

function performSubmit() {
  userEvent.click(screen.getByRole('button', { name: 'Submit' }));
}

async function completeWizard({
  phrase = fave.phrase,
  myGame = game,
} = {}) {
  return act(async () => {
    await openModal();
    inputQuery(goodQuery);
    await submitGameSearch();
    await selectGameAndContinue(myGame);
    fillOutFaveFields(phrase, myGame);
    performSubmit();
  });
}

it('handles successful completion', async () => {
  await completeWizard();
  expect(window.alert).toHaveBeenCalledWith(`Successfully refrigerated ${searchResults[0].name}`);
  await waitFor(() => expect(screen.queryByRole('dialog', { name: 'Add Fave Wizard' })).not.toBeInTheDocument());
  expect(getFaves).toHaveBeenCalledTimes(1);
});

it('handles error response', async () => {
  await completeWizard({ myGame: gameWithConflict });
  expect(window.alert).toHaveBeenCalledWith('Conflict.');
  expect(screen.queryByRole('dialog', { name: 'Add Fave Wizard' })).toBeInTheDocument();
});

it('focuses the searchbox when the modal is opened', async () => {
  await act(async () => {
    await openModal();
    await waitFor(() => expect(document.activeElement).toHaveClass('input'));
    userEvent.type(document.activeElement, goodQuery);
  });
  expect(screen.getByRole('searchbox', { name: 'Search' })).toHaveValue(goodQuery);
});

it('does not allow garbage input', async () => {
  await completeWizard({ phrase: ' ' });
  expect(addFave).not.toHaveBeenCalled();
});

it('goes back to step 1', async () => {
  await act(async () => {
    await openModal();
    inputQuery(goodQuery);
    await submitGameSearch();
    await selectGameAndContinue(game);
    fillOutFaveFields(fave.phrase, game);
    userEvent.click(screen.getByRole('button', { name: 'Go Back' }));
  });
  expect(screen.getByRole('table', { name: 'Search Results' })).toBeInTheDocument();
});

it('disables the submit button if the phrase is removed', async () => {
  await act(async () => {
    await openModal();
    inputQuery(goodQuery);
    await submitGameSearch();
    await selectGameAndContinue(game);
    fillOutFaveFields(fave.phrase, game);
    const phraseInput = screen.getByRole('textbox', { name: `Why do you love ${game.name}?` });
    phraseInput.setSelectionRange(0, fave.phrase.length);
    userEvent.type(phraseInput, '{backspace}');
  });
  expect(screen.getByRole('button', { name: 'Submit' })).toHaveAttribute('disabled');
});
