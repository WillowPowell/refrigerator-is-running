import fetchMock from 'fetch-mock-jest';

import { responses } from 'fixtures/session';

import checkSession from './checkSession';

const checkSessionMatcher = {
  url: '/api/sessions',
  method: 'GET',
};
const dispatch = jest.fn();

afterEach(() => {
  fetchMock.restore();
});

it('calls the API and returns the response', async () => {
  // Arrange
  fetchMock.mock(checkSessionMatcher, responses.checkSessionSuccess);
  expect.assertions(2);
  // Act
  const [response, responseJSON] = await checkSession(() => {});
  // Assert
  expect(response.ok).toBe(true);
  expect(responseJSON).toEqual(responses.checkSessionSuccess.body);
});

it('calls SET_USER on success', async () => {
  // Arrange
  fetchMock.mock(checkSessionMatcher, responses.checkSessionSuccess);
  expect.assertions(2);
  // Act
  await checkSession(dispatch);
  // Assert
  expect(dispatch).toHaveBeenCalledTimes(2);
  expect(dispatch).toHaveBeenCalledWith({
    type: 'SET_USER',
    payload: responses.checkSessionSuccess.body.data,
  });
});

it('calls SET_SESSION_CHECKED', async () => {
  // Arrange
  fetchMock.mock(checkSessionMatcher, responses.checkSessionFail);
  expect.assertions(2);
  // Act
  await checkSession(dispatch);
  // Assert
  expect(dispatch).toHaveBeenCalledTimes(1);
  expect(dispatch).toHaveBeenCalledWith({
    type: 'SET_SESSION_CHECKED',
    payload: true,
  });
});
