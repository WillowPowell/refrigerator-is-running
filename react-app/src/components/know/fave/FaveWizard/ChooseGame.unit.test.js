/* global expectComponentMounted */
/* global customRender */
/* global mockComponents */
import React from 'react';
import { screen } from '@testing-library/dom';

import {
  ClickableTable, SubTitle, SearchForm,
} from 'components/show';
import initialState from 'context/faveWizard/initialState';
import { searchResults } from 'fixtures/game';

import gameTableColumns from './gameTableColumns';
import ChooseGame from './ChooseGame';

jest.mock('components/show');

beforeEach(() => {
  mockComponents({ ClickableTable, SubTitle, SearchForm });
});

const subTitleProps = { children: 'Search for your favourite game.' };
const searchFormProps = {
  defaultQuery: initialState.lastSearch.query,
  inputRef: {
    current: null,
  },
  handleSubmit: expect.any(Function),
  isLoading: initialState.isLoading,
};
const clickableTableProps = {
  caption: 'Search Results',
  data: searchResults,
  selectedState: initialState.fave.game,
  columns: gameTableColumns,
  tableClass: 'mt-2 is-fullwidth',
  onClick: expect.any(Function),
};

it('renders with no search data', () => {
  customRender(<ChooseGame />, {
    authState: {},
    faveWizardState: {},
  });
  expectComponentMounted({ SubTitle }, { props: subTitleProps });
  expectComponentMounted({ SearchForm }, { props: searchFormProps });
  expect(screen.queryByTestId('ClickableTable')).not.toBeInTheDocument();
});

it('renders with search data', () => {
  customRender(<ChooseGame />, {
    authState: {},
    faveWizardState: {
      lastSearch: {
        query: 'Bayonetta',
        data: searchResults,
      },
    },
  });
  expectComponentMounted({ ClickableTable }, { props: clickableTableProps });
  expectComponentMounted({ SearchForm }, {
    props: {
      ...searchFormProps,
      defaultQuery: 'Bayonetta',
    },
  });
});

it('renders with a selected game', () => {
  customRender(<ChooseGame />, {
    authState: {},
    faveWizardState: {
      fave: {
        game: searchResults[0],
      },
      lastSearch: {
        query: 'Bayonetta',
        data: searchResults,
      },
    },
  });
  expectComponentMounted({ ClickableTable }, {
    props: {
      ...clickableTableProps,
      selectedState: searchResults[0],
    },
  });
});
