/* global customRender */
import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { withCover as fave } from 'fixtures/fave';

import { deleteFave } from 'utils/api';

import DeleteFaveButton from './DeleteFaveButton';

jest.mock('utils/api');

function button() {
  return screen.getByRole('button', { name: 'Delete' });
}

beforeEach(() => {
  customRender(<DeleteFaveButton faveId={fave.id} />, {
    authState: {},
    favesState: {},
  });
});

it('renders', () => {
  expect(button()).toBeInTheDocument();
});

it('deletes the fave if the user confirms', () => {
  window.confirm = jest.fn(() => true);
  userEvent.click(button());
  expect(deleteFave).toHaveBeenCalledWith(
    expect.any(Function),
    expect.any(Function),
    fave.id,
  );
});

it('does not delete the fave if they do not', () => {
  window.confirm = jest.fn(() => false);
  userEvent.click(button());
  expect(deleteFave).not.toHaveBeenCalled();
});
