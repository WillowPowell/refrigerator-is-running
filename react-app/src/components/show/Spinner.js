import React from 'react';

export default function Spinner() {
  return (
    <div className="container">
      <span className="button is-text is-fullwidth is-large is-loading">Loading</span>
    </div>
  );
}
