/* global customRender */
/* global expectComponentMounted */
/* global mockComponents */
/* global mockPushHistory */
import React from 'react';
import { screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { getFavesResults as faves, withCover, noCover } from 'fixtures/fave';

import { IconButton } from 'components/show';

import EditFaveModal from '../EditFaveModal';
import DeleteFaveButton from '../DeleteFaveButton';

import ViewFavesList from '.';

jest.mock('../EditFaveModal');
jest.mock('../DeleteFaveButton');

beforeEach(() => {
  mockComponents({ EditFaveModal, DeleteFaveButton });
  customRender(<ViewFavesList faves={faves} />);
});

it('renders', () => {
  expect(screen.getByRole('table', { name: 'My Faves' })).toBeInTheDocument();
  const withCoverRow = within(screen.getByRole('link', { name: `1: ${withCover.game.name}` }));
  expect(withCoverRow.getByRole('img', { name: `${withCover.game.name} Cover Image` })).toBeInTheDocument();
  expect(withCoverRow.getByRole('button', { name: 'Edit' })).toBeInTheDocument();
  expect(withCoverRow.getByText(withCover.phrase)).toBeInTheDocument();
  expect(withCoverRow.getByText(withCover.hours_played)).toBeInTheDocument();
  const noCoverRow = within(screen.getByRole('link', { name: `2: ${noCover.game.name}` }));
  expect(noCoverRow.getByRole('img', { name: 'Placeholder Image' })).toBeInTheDocument();
  expect(noCoverRow.getByRole('button', { name: 'Edit' })).toBeInTheDocument();
  expect(noCoverRow.getByText(noCover.phrase)).toBeInTheDocument();
  expect(noCoverRow.getByText(noCover.hours_played)).toBeInTheDocument();
  faves.forEach((fave, i) => {
    expectComponentMounted({ DeleteFaveButton }, {
      i,
      props: {
        ariaLabel: 'Delete',
        className: 'button is-outlined is-primary',
        children: null,
        icon: 'fas fa-trash',
        iconSize: 'fa-lg',
        faveId: fave.id,
        node: IconButton,
      },
    });
  });
});

it('opens the modal when you click the button', () => {
  const index = 0;
  const withCoverRow = within(screen.getByRole('link', { name: `1: ${withCover.game.name}` }));
  userEvent.click(withCoverRow.getByRole('button', { name: 'Edit' }));
  expectComponentMounted({ EditFaveModal }, {
    props: {
      fave: faves[index],
      index,
      modalActive: true,
    },
    anyFuncProps: ['setModalActive'],
  });
});

it('navigates to the fave when you click the row', () => {
  const index = 0;
  userEvent.click(screen.getByRole('link', { name: `1: ${withCover.game.name}` }));
  expect(mockPushHistory).toHaveBeenCalledWith(`/fave/${index}`);
});
