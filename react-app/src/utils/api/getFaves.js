import apiHandler from './apiHandler';

/** Helper function to handle API calls to get user's faves. */
export default async function getFaves(favesDispatch, authFetch) {
  const response = await apiHandler(
    '/api/faves',
    {
      method: 'GET',
    },
    authFetch,
  );
  const responseJSON = await response.json();
  if (response.ok) {
    favesDispatch({
      type: 'SET_ALL_FAVES',
      payload: responseJSON.data,
    });
  } else {
    favesDispatch({
      type: 'SET_ALL_FAVES',
      payload: [],
    });
  }
  favesDispatch({
    type: 'SET_SESSION_CHECKED',
    payload: true,
  });
  return [response, responseJSON];
}
