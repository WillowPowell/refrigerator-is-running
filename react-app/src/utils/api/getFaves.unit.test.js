import fetchMock from 'fetch-mock-jest';

import { withCover as fave } from 'fixtures/fave';

import getFaves from './getFaves';

const dispatch = jest.fn();

class GetFaveMocker {
  constructor(data, errors = null, success = true) {
    this.matcher = {
      url: `${process.env.REACT_APP_API_URL}/api/faves`,
      method: 'GET',
    };
    this.successResponse = {
      body: {
        success,
        data,
        errors,
      },
      status: success ? 200 : 400,
    };
  }
}

// Teardown
afterEach(() => fetchMock.restore());

it('calls the api and returns the data', async () => {
  // Arrange
  const getFaveMocker = new GetFaveMocker(fave);
  fetchMock.mock(getFaveMocker.matcher, getFaveMocker.successResponse);
  expect.assertions(2);
  // Act
  const [response, responseJSON] = await getFaves(dispatch, fetch);
  // Assert
  expect(response.ok).toBe(true);
  expect(responseJSON.data).toEqual(fave);
});

it('calls dispatch twice on succcess', async () => {
  // Arrange
  const getFaveMocker = new GetFaveMocker(fave);
  fetchMock.mock(getFaveMocker.matcher, getFaveMocker.successResponse);
  expect.assertions(3);
  // Act
  await getFaves(dispatch, fetch);
  // Assert
  expect(dispatch).toHaveBeenCalledTimes(2);
  expect(dispatch).toHaveBeenCalledWith({
    type: 'SET_ALL_FAVES',
    payload: fave,
  });
  expect(dispatch).toHaveBeenCalledWith({
    type: 'SET_SESSION_CHECKED',
    payload: true,
  });
});

it('calls dispatch twice on failure', async () => {
  // Arrange
  const getFaveMocker = new GetFaveMocker(null, ['Not Found'], false);
  fetchMock.mock(getFaveMocker.matcher, getFaveMocker.successResponse);
  expect.assertions(3);
  // Act
  await getFaves(dispatch, fetch);
  // Assert
  expect(dispatch).toHaveBeenCalledTimes(2);
  expect(dispatch).toHaveBeenCalledWith({
    type: 'SET_ALL_FAVES',
    payload: [],
  });
  expect(dispatch).toHaveBeenCalledWith({
    type: 'SET_SESSION_CHECKED',
    payload: true,
  });
});
