# Flask API

The API routes live at:

`/api`
`.../users`
`.../sessions`
`.../games`
`.../faves`

## Users
### Register
*HTTP POST `api/users`*
*description:* Create a user.
*note:* A password is considered strong if it has 4 of the following 5 traits:
  - 8 characters length or more
  - 1 digit or more
  - 1 symbol or more
  - 1 uppercase letter or more
  - 1 lowercase letter or more

*url params:* none
*params:* 

  {
    email: str,
    name: str,
    password: str
  }
*return:*
on success status code `201`:

  {
    success: true
    data: {
      id: int,
      email: str,
      name: str,
      verified: bool,
      created_at: str: formatted like "Jan 21, 2021 15:34:23 UTC", corresponding to '%a, %d %b %Y %H:%M:%S UTC',
      updated_at: str: formatted like "Jan 21, 2021 15:34:23 UTC", corresponding to '%a, %d %b %Y %H:%M:%S UTC' or `null`
    }
  }

if email, name, or password not provided status code `400`:

  {
    success: false,
    errors: ['Required Field {field} Not Provided.']
  }

if password not strong status code `400`:

  {
    success: false,
    errors: ['Password Not Strong Enough.']
  }

if user already exists with email email status code `409`:

  {
    success: false,
    errors: ['User With Email Exists.']
  }

if any other errors occurs status code `500`:

  {
    success: false,
    errors: [...]
  }

## Sessions
### Login
*HTTP POST `api/sessions`*
*description:* create a session for the user
*url params:* none
*params:* 

  {
      email,
      password
  }
*return:*
on success set `session[user_id]` to `user.id` and return status code `201`:

  {
    success: true,
    data: {user}: see Users, Register
  }

if no user exists with email email status code `401`:

  {
    success: false,
    errors: ['Authentication Failed.']
  }

if password not match user password status code `401`:

  {
    success: false,
    errors: ['Authentication Failed.']
  }

if email or password not provided status code `400`:

  {
    success: false,
    errors: ['Required Field {field} Not Provided.']
  }

if any other errors occurs status code `500`:

  {
    success: false,
    errors: [...]
  }


### Log Out
*HTTP DELETE `api/sessions`*
*description:* clear the user's session from the server
*url params:* none
*params:* none
*return:*
on success status code `201`:

  {
    success: true
    data: {}
  }

if no session exists status code `404`
  
  {
    success: false,
    errors: ["Not Found."]
  }

if an errors occurs status code `500`:

  {
    success: false,
    errors: [...]
  }

### Check Session
*HTTP GET `api/sessions`*
*description:* check if there's an active session and return the user information for the session
*url params:* none
*params:* none
*return:*
on success status code `200`:

  {
    success: true,
    data: {user}: see Users, Register
    errors: []
  }

if no session exists status code `404`:

  {
    success: false,
    errors: ['Not Found.']
  }

if any other errors occurs status code `500`:

  {
    success: false,
    errors: [...]
  }

## Games
### Add game
*HTTP POST `/api/games`*
*description:* Add game to database.
*note:* All parameters except `name` are optional.
*url params:* none
*params:*

  {
		name: str,
		igdb_id: str,
		first_release_date: str: Date formatted like "Jan 21, 1998" corresponding to '%a %d, %Y'.,
		igdb_url: str,
		cover_url: str,
		twitch_id: str
  }
*return:*
on success status code `201`:

  {
    success: true,
    data: {
      id,
      name: str,
      first_release_date: str: Date formatted like "Jan 21, 1998" corresponding to '%a %d, %Y'.,
      cover_url: str,
      summary: str,
      igdb_id: str,
      igdb_url: str,
      twitch_id: str,
      created_at: str: formatted like "Jan 21, 2021 15:34:23 UTC", corresponding to '%a, %d %b %Y %H:%M:%S UTC',
      updated_at: str: formatted like "Jan 21, 2021 15:34:23 UTC", corresponding to '%a, %d %b %Y %H:%M:%S UTC'
    }
  }

if name not provided status code `400`:

  {
    success: false,
    errors: ['No name Provided']
  }

if incorrect data type provided status code `400`:

  {
    success: false,
    errors: ['Incorrect format for {field}. Expected {required_type}.']
  }

if first\_release\_date not in '%a %d, %Y' format `400`:

  {
    success: false,
    errors: ["Incorrect format for first_release_date. Should be like 'Oct 5, 1988'."]
  }

if game already exists with igdb_id `409`:

  {
    success: false,
    errors: ['Game {id} With IGDB ID {igdb_id} Exists']
  }

if game already exists with twitch_id `409`:

  {
    success: false,
    errors: ['Game {id} With Twitch ID {twitch_id} Exists']
  }

if any other errors occurs status code `500`:

  {
    success: false,
    errors: [...]
  }

### Search for game
*HTTP GET `/api/games`*
*description:* Search for games via igdb.
*note:* Use `offset` for paging - generally would be a multiple of `limit`. 
*url params:* none
*params:*

  {
		query: str,
		limit: int: number of results to request, max 30
		offset: int: number of results to skip
  }
*return:*
on success status code `200`:

  {
    success: true,
    results: [{}]: array of games that match the criteria.
  }

if required field not provided status code `400`:

  {
    success: false,
    errors: ['No {missing_field} Provided.']
  }

if `limit` > 30 status code `400`:

  {
    success: false,
    errors: ['{requested_limit} limit requested. Maximum 30.']
  }

if incorrect data type provided status code `400`:

  {
    success: false,
    errors: ['Incorrect format for {field}. Expected {required_type}.']
  }

if no games found `404`:

  {
    success: false,
    errors: ['No Results Found.']
  }

if unable to connect to IGDB status code `503`:
  {
    success: false,
    errors: ['Unable To Connect To IGDB.']
  }

if any other errors occurs status code `500`:

  {
    success: false,
    errors: [...]
  }

## Faves Endpoint
### Create Fave
*HTTP POST `api/faves`*
*description:*
Create a Fave. Faves are objects representing a user's special relationship with a game.

**Handling conflicts:** A conflict is a Fave that already exists with the same user\_id and game\_id. If a request that would create a conflict is received, it is handled like this:

  - if the additional data (ie, `top_fave`, `phrase`, and `hours_played`) exactly matches the existing Fave in the Fridge, the existing Fave is returned.
  - if the data differs, a `409` conflict response is returned. The error message will include the id of the conflicting Fave, which can be `PATCH`ed instead.

*url params:* none
*cookies*: `session[user_id]`
*params:* 

  {
    game_id: int,
    top_fave: boolean,
    phrase: str,
    hours_played: int
  }

*return:*
on success status code `201`:

  {
    success: true,
    data: {
      id: int,
      user_id: int,
      game_id: int,
      top_fave: boolean,
      phrase: str,
      hours_played: int,
      deleted: bool,
      created_at: str: formatted like "Jan 21, 2021 15:34:23 UTC", corresponding to '%a, %d %b %Y %H:%M:%S UTC',
      updated_at: str: formatted like "Jan 21, 2021 15:34:23 UTC", corresponding to '%a, %d %b %Y %H:%M:%S UTC' or `null`,
      deleted_at: str: formatted like "Jan 21, 2021 15:34:23 UTC", corresponding to '%a, %d %b %Y %H:%M:%S UTC' or `null`
    }
  }

if game does not exist with id game_id status code `404`:

  {
    success: false,
    errors: ['Game Not Found.']
  }

if incorrect data type provided status code `400`:

  {
    success: false,
    errors: ['Incorrect format for {field}. Expected {required_type}.']
  }

if game_id not provided status code `400`:

  {
    success: false,
    errors: ['No game_id Provided.']
  }

if not `session[user_id]` status_code `401`:

  {
    success: false,
    errors: ['Unauthorized.']
  }

if conflict (see description) `409`:

  {
    success: false,
    errors: ['Conflict with fave {fave_id}. Has game_id {game_id} and user_id {user_id}.']
  }

if any other errors occurs status code `500`:

  {
    success: false,
    errors: ['Incorrect format for {field}. Expected {required_type}.']
  }

### Edit fave
*HTTP Patch `api/faves/:fave_id`*
*description:*
Edit fave details.
`deleted` is special:
  `deleted` Faves will not be returned by GET faves by default.
  Toggling `deleted` also affects the `deleted_at` field.
*url params:* `fave_id: int`
*cookies*: `session.user`
*params:*

  {
    top_fave: boolean,
    phrase: str,
    hours_played: int,
    deleted: boolean
  }

*return:*
on success status code `200`:

  {
    success: true,
    fave: {}: the modified fave (see Create Fave for fields)
  }

if `game_id` sent status code `400`:

  {
    success: false,
    errors: ["Cannot edit {field}. Create a new Fave instead."]
  }

if any field sent with wrong data type status code `400`:

  {
    success: false,
    errors: ["Incorrect format for {field}. Expected {required_type}."]
  }

if `session[user_id]` does not match `fave.user_id` status code `401`:

  {
    success: false,
    errors: ["Unauthorized.]"
  }

if fave does not exist with id fave_id status code `404`:

  {
    success: false,
    errors: ['Fave Not Found']
  }

if any other errors occurs status code `500`:

  {
    success: false,
    errors: [...]
  }

### Delete fave
*HTTP DELETE `api/fave/:fave_id`*
*description:*
Delete a game from a user's faves.
By default, sets `deleted` to `True` and does not actually delete row from database.
Set `permanent` to `True` to delete the row.
*url params:* `fave_id: int`
*cookies*: `session.user`
*params:*:

  {
    permanent: bool
  }

if not `permanent` on success status code `200`:

  {
    success: true,
    data: {the deleted fave (see Create Fave for fields)}
  }

if `permanent` on success status code `200`:

  {
    success: true,
    data: {}
  }

if `session[user_id]` does not match `fave.user_id` status code `401`:

  {
    success: false,
    errors: ["Unauthorized.]"
  }

if fave does not exist with id fave_id status code `404`:

  {
    success: false,
    errors: ['Fave Not Found']
  }

if any other errors occurs status code `500`:

  {
    success: false,
    errors: [...]
  }

### Get faves
*HTTP GET `api/faves`*
*description:* Get a user's faves
*url params:* (all optional) {
  'limit' : int, (default 20)
  'offset' : int, (default 0)
  'deleted' : bool, (default False)
  'top_fave' : bool,
}
*cookies*: `session.user`
*params:* none
*return:*
on success status code `200`:

  {
    success: true,
    faves: [ : array of user's faves
	    { Fave (see Create Fave)},
      { Fave },
      ...
  	]
  }

if user_id does not exist `401`:

  {
    success: false,
    errors: ['Unauthorized User']
  }

if session.user does not exist `401`:

  {
    success: false,
    errors: ['Unauthorized.']
  }

if no faves exist with user\_id session.user.id status code `404`:

  {
    success: false,
    errors: ['No Faves Found']
  }

if any other errors occurs status code `500`:

  {
    success: false,
    errors: [...]
  }
