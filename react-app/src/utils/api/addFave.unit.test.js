import fetchMock from 'fetch-mock-jest';

import { withCover as fave } from 'fixtures/fave';

import addFave from './addFave';

class AddFaveMocker {
  constructor(data) {
    this.matcher = {
      url: '/api/faves',
      method: 'POST',
      body: {
        game_id: data.game_id,
        top_fave: data.top_fave,
        phrase: data.phrase,
        hours_played: data.hours_played,
      },
    };
    this.successResponse = {
      body: {
        success: true,
        data,
      },
      status: 201,
    };
  }
}

// Teardown
afterEach(() => fetchMock.restore());

it('calls the api and returns the data', async () => {
  // Arrange
  const addFaveMocker = new AddFaveMocker(fave);
  fetchMock.mock(addFaveMocker.matcher, addFaveMocker.successResponse);
  expect.assertions(2);
  // Act
  const [response, responseJSON] = await addFave(
    fetch,
    fave.game_id,
    fave.top_fave,
    fave.phrase,
    fave.hours_played,
  );
  // Assert
  expect(response.ok).toBe(true);
  expect(responseJSON.data).toEqual(fave);
});
