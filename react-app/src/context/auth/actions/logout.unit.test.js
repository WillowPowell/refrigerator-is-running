import { logout as apiCall } from 'utils/api';
import { waitFor } from '@testing-library/dom';

import logout from './logout';

jest.mock('utils/api');

const dispatch = jest.fn();
const history = {
  push: jest.fn(),
};
it('calls the api and history.push', async () => {
  apiCall.mockImplementation(async () => [
    { ok: true },
    {},
  ]);
  logout(dispatch, { history });
  expect(apiCall).toHaveBeenCalledWith(dispatch);
  await waitFor(() => expect(history.push).toHaveBeenCalledWith('/'));
});
