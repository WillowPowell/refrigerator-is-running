"""
The `games` route interacts with external APIs.
This file contains elements necessary for testing the route functionality without
engaging the external APIs more than necessary.
"""
import datetime

import pytest
import requests

from refrigerator.app import create_app, db
from refrigerator.models.handlers import get_twitch_auth
from refrigerator.models.games import Game
from refrigerator.models.handlers import IGDB

from tests.utils import MockResponse


class MockIGDBResults():
	"""
	Use this class to mock a series of responses to IGDB API requests.

	`__init__` keyword parameters are in the format `(data, status_code)`.
	They control the responses returned by the various
	IGDB endpoints hit by the Refrigerator.

	:attr query: The string that must be passed in the `search` parameter in order to return `search_results`.
	:attr search_results: The response to return from the IGDB "/games" endpoint, if "search" is sent in the payload.
	:attr games: The response to return from the IGDB "/games" endpoint, if "search" is *not* sent in the payload.
	:attr covers: The response to return from the IGDB "/covers" endpoint.
	:attr external_games: The response to return from the IGDB "/external_games" endpoint.
	"""
	def __init__(self, query, search_results=([], 200), games=([], 200),
		covers=([], 200), external_games=([], 200)):
		self.query = query
		self.search_results = search_results
		self.games = games
		self.covers = covers
		self.external_games = external_games

	def _make_response(self, url, **kwargs):
		"""
		Prepare a MockResponse to an IGDB API request.
		"""
		# Pull the endpoint off the end of the url.
		endpoint = url[len("https://api.igdb.com/v4/"):]
		# Convert the received payload into soemthing we can work with.
		payload = self._parse_payload(kwargs["data"])

		# Select the correct dataset.
		if endpoint == "games" and "search" in payload:
			# If the wrong `query` value is passed, the test was probably set up wrong.
			assert payload["search"] == self.query, "Incorrect query, check your test configuration."
			dataset = self.search_results[0]
			status_code = self.search_results[1]
		else:
			dataset = getattr(self, endpoint)[0]
			status_code=getattr(self, endpoint)[1]
		# Reduce the dataset.
		# Handle paging.
		offset = payload.get("offset", 0)
		dataset = dataset[offset:offset + payload["limit"]]
		# Only return items that match the "where" filters.
		if "where" in payload:
			for k, v in payload["where"].items():
				if type(v) == list:
					dataset = [el for el in dataset if el[k] in v]
				else:
					dataset = [el for el in dataset if el[k] == v]
		# Filter out fields that were not requested.
		# "fields" defaults to "id"
		fields = payload.get("fields", ["id", ])
		# "*" means all fields in APICalypse, and will override other requested fields.
		if "*" not in payload["fields"]:
			for i, el in enumerate(dataset):
				dataset[i] = {k: v for (k, v) in el.items() if k in payload["fields"]}

		return MockResponse(url=url, json_data=dataset, status_code=status_code)

	def _parse_payload(self, payload):
		"""
		IGDB requests are formatted in the APICalypse syntax.

		:param payload: An APICalypse request.
			An example search request could look like: "fields id,updated_at;search \"Bayonetta\";limit 20;offset 0;"
			An example of a non-search request could look like:
				"fields game,id;where game = (2136,2135,76888,28058,64207,51154,136077,51150,51187,136898,127258) & category = 14;limit 11;"
		"""
		payload_dict = {}
		# Split at the semicolons.
		for el in payload[:-1].split(";"):
			# Split at the spaces and put in the dict.
			divider = el.find(" ")
			payload_dict[el[0:divider]] = el[divider + 1:]

		# Each of the fields behaves somewhat uniquely, so handle them all separately.
		# Convert `limit` and `offset` into integers.
		if "limit" in payload_dict:
			payload_dict["limit"] = int(payload_dict["limit"])
		if "offset" in payload_dict:
			payload_dict["offset"] = int(payload_dict["offset"])
		# `search` has to contain quotes, so strip those.
		if "search" in payload_dict:
			payload_dict["search"] = payload_dict["search"].strip("\"")
		# `fields` is comma-separated, make it into a nice list.
		if "fields" in payload_dict:
			payload_dict["fields"] = payload_dict["fields"].split(",")
		# The `where` field is pretty bonkers.
		# It can have multiple values, and they can be formatted differently.
		if "where" in payload_dict:
			filters = {}
			# Split the string into a list of filters.
			for el in payload_dict["where"].split(" & "):
				# Split at the equal signs and put in the dict.
				divider = el.find(" = ")
				filters[el[0:divider]] = el[divider + 3:]
			for k, v in filters.items():
				# If it ends with ")", it's a list of values.
				# Ignore the brackets and split at the commas.
				if v[0] == "(":
					filters[k] = v[1:-1].split(",")
					# Cast them to integers if possible, as they are probably ids.
					# Only do so if we can do it for all of them,
					# otherwise they should probably all stay strings.
					int_list = []
					for v2 in filters[k]:
						try:
							int_list.append(int(v2))
						except:
							break
					else:
						filters[k] = int_list
				# If it's not a list, try casting it to an int.
				# Otherwise, just leave it as a string!
				else:
					try:
						filters[k] = int(v)
					except:
						pass
				# Finally we replace the original string with the new filters dict.
				# Phew!
				payload_dict["where"] = filters
		return payload_dict

	def mock_request(self, method, url, **kwargs):
		"""`monkeypatch` this onto `requests.request` to use this mock."""
		return self._make_response(url, **kwargs)


@pytest.fixture
def mock_search(monkeypatch):
	"""
	Fixture to customize `MockIGDBResults` for specific testing needs.
	See documentation of `MockIGDBResults` for parameter information.

	Patches `requests.request` and `IGDB._make_headers`
	to prevent any communication with external APIs.
	"""

	def outer(query,
			search_results=([], 200), games=([], 200),
			covers=([], 200), external_games=([], 200)):

		igdb_mocker = MockIGDBResults(query,
			search_results=search_results,
			games=games,
			covers=covers,
			external_games=external_games)

		monkeypatch.setattr(requests, "request", igdb_mocker.mock_request)
		monkeypatch.setattr(IGDB, "_make_headers", lambda self: {})

		def inner(client, query_string):
			return client.get("/api/games", query_string=query_string)

		return inner

	return outer


@pytest.fixture
def bayonetta_search(mock_search, bayonetta_search_results, bayonetta_games,
		bayonetta_covers, bayonetta_external_games):
	"""A version of search that returns data from the Bayonetta fixtures in this file."""

	return mock_search(
		"Bayonetta",
		search_results=(bayonetta_search_results, 200),
		games=(bayonetta_games, 200),
		covers=(bayonetta_covers, 200),
		external_games=(bayonetta_external_games, 200))


@pytest.fixture
def smash_bros_search(mock_search, smash_search, smash_games,
		smash_covers, smash_external_games):
	"""A version of search that returns data from the Smash Bros fixtures in this file."""

	return mock_search(
		"Smash Bros",
		search_results=(smash_search, 200),
		games=(smash_games, 200),
		covers=(smash_covers, 200),
		external_games=(smash_external_games, 200))



# The fixtures in ALL_CAPS are the expected data to be returned by correct usage of the search fixtures.
@pytest.fixture
def BAYONETTA_DATA():
	return {
		"name": "Bayonetta",
		"first_release_date": "Oct 29, 2009",
		"cover_url": "https://images.igdb.com/igdb/image/upload/t_thumb/co1p92.jpg",
		"cover_url_big": "https://images.igdb.com/igdb/image/upload/t_cover_big/co1p92.jpg",
		"cover_width": 1403,
		"cover_height": 1871,
		"summary": "A member of an ancient witch clan "
			+ "and possessing powers beyond the comprehension of mere mortals, "
			+ "Bayonetta faces-off against countless angelic enemies, "
			+ "many reaching epic proportions, in a game of "
			+ "100% pure, unadulterated all-out action. "
			+ "Outlandish finishing moves are performed with balletic grace "
			+ "as Bayonetta flows from one fight to another. "
			+ "With magnificent over-the-top action taking place "
			+ "in stages that are a veritable theme park of exciting attractions, "
			+ "Bayonetta pushes the limits of the action genre, "
			+ "bringing to life its fast-paced, dynamic climax combat.",
		"igdb_id": 2136,
		"igdb_url": "https://www.igdb.com/games/bayonetta",
		"twitch_id": 246130
	}


@pytest.fixture
def BAYONETTA_CLIMAX_DATA():
	return {
		"name": "Bayonetta: Nonstop Climax Edition",
		"first_release_date": None,
		"cover_url": None,
		"cover_url_big": None,
		"cover_width": None,
		"cover_height": None,
		"summary": None,
		"igdb_id": 136898,
		"igdb_url": "https://www.igdb.com/games/bayonetta-nonstop-climax-edition",
		"twitch_id": 1992080
	}


@pytest.fixture
def SMASH_64_DATA():
	return {
		"cover_height": 800,
		"cover_url": "https://images.igdb.com/igdb/image/upload/t_thumb/co2tso.jpg",
		"cover_url_big": "https://images.igdb.com/igdb/image/upload/t_cover_big/co2tso.jpg",
		"cover_width": 600,
		"first_release_date": "Jan 21, 1999",
		"igdb_id": 1626,
		"igdb_url": "https://www.igdb.com/games/super-smash-bros",
		"name": "Super Smash Bros.",
		"summary": "Super Smash Bros. is a crossover fighting video game between several different Nintendo franchises, "
			+ "and the first installment in the Super Smash Bros. series. Players must defeat their opponents multiple times "
			+ "in a fighting frenzy of items and power-ups. Super Smash Bros. is a departure from the general genre of fighting games: "
			+ "instead of depleting an opponent's life bar, the players seek to knock opposing characters off a stage. "
			+ "Each player has a damage total, represented by a percentage, which rises as the damage is taken.",
		"twitch_id": 246327,
	}


@pytest.fixture
def SMASH_WII_U_DATA():
	return {
		"cover_height": 2000,
		"cover_url": "https://images.igdb.com/igdb/image/upload/t_thumb/co213x.jpg",
		"cover_url_big": "https://images.igdb.com/igdb/image/upload/t_cover_big/co213x.jpg",
		"cover_width": 1500,
		"first_release_date": "Nov 21, 2014",
		"igdb_id": 9602,
		"igdb_url": "https://www.igdb.com/games/super-smash-bros-for-wii-u",
		"name": "Super Smash Bros. for Wii U",
		"summary": "Super Smash Bros. for Wii U is the fourth console-based entry in the popular Super Smash Bros. series of fighting games. "
			+ "With an even larger roster of fighters than its predecessors and the introduction eight player battles, "
			+ "Smash for Wii U continues to add to the formula that gamers have loved since 1999.",
		"twitch_id": 245258,
	}


@pytest.fixture
def MODIFIED_SMASH_64_DATA():
	return {
		"cover_height": 400,
		"cover_url": "https://duckduckgo.com/?q=smash+bros&t=h_&iax=images&ia=images",
		"cover_width": 300,
		"first_release_date": "Jan 21, 1998",
		"igdb_id": 9602,
		"igdb_url": "https://www.igdb.com/games/super-smash-brothers",
		"name": "Super Smash Brothers",
		"summary": "It was, like, revo*loosh*unary!",
		"twitch_id": 1,
	}


@pytest.fixture()
def modified_smash_data(app):
	"""
	Create two entries with "old" data.
	Different values than returned by IGDB search on Jan 24, 2021
	IGDB ID not modified.
	updated_at is made to be earlier than the updated_at value reported by IGDB.
	"""
	smash_n64 = Game(name="Super Smash Brothers", igdb_id=1626)
	smash_n64.cover_height = 400
	smash_n64.cover_width = 300
	smash_n64.cover_url = "https://duckduckgo.com/?q=smash+bros&t=h_&iax=images&ia=images"
	smash_n64.first_release_date = datetime.date(year=1998, month=1, day=21)
	smash_n64.igdb_url = "https://www.igdb.com/games/super-smash-brothers"
	smash_n64.summary = "Notice how the name of the game is not in the summary?"
	smash_n64.twitch_id = 1
	smash_n64.updated_at = datetime.datetime(year=1998, month=1, day=21, hour=12)
	db.session.add(smash_n64)
	smash_wii_u = Game(name="Super Smash Bros. for Wii U", igdb_id=9602)
	smash_wii_u.cover_height = 1000
	smash_wii_u.cover_width = 4500
	smash_wii_u.cover_url = "https://www.smashbros.com/wiiu-3ds/us/"
	smash_wii_u.first_release_date = datetime.date(year=2012, month=11, day=21)
	smash_wii_u.igdb_url = "https://www.igdb.com/games/super-smash-bros-for-wii-me"
	smash_wii_u.name = "Super Smash Bros. for Wii Me"
	smash_wii_u.summary = "Oh, no! No name in the summary?!"
	smash_wii_u.twitch_id = 2
	smash_wii_u.updated_at = datetime.datetime(year=2000, month=1, day=21, hour=14)
	db.session.add(smash_wii_u)
	db.session.commit()


@pytest.fixture()
def updated_at_in_future(app, modified_smash_data):
	"""
	Set updated_at in future.
	Will always be later than updated_at returned by IGDB.
	"""
	smash_n64 = Game.query.filter_by(igdb_id=1626).first()
	smash_n64.updated_at = datetime.datetime.now() + datetime.timedelta(days=4)
	db.session.add(smash_n64)
	smash_wii_u = Game.query.filter_by(igdb_id=9602).first()
	smash_wii_u.updated_at = datetime.datetime.now() + datetime.timedelta(days=4)
	db.session.add(smash_wii_u)
	db.session.commit()


@pytest.fixture
def bayonetta_search_results():
	"""
	Mock results of hitting IGDB "/games" endpoint on Feb 24, 2021,
	with "fields id,updated_at;search "Bayonetta";limit 20;offset 0;"

	This mocks calling IGDB().call()
	"""
	return [
		{
			"id": 2136,
			"updated_at": 1613912379
		},
		{
			"id": 2135,
			"updated_at": 1613944505
		},
		{
			"id": 76888,
			"updated_at": 1601596800
		},
		{
			"id": 28058,
			"updated_at": 1610989492
		},
		{
			"id": 64207,
			"updated_at": 1604620800
		},
		{
			"id": 51154,
			"updated_at": 1534809600
		},
		{
			"id": 136077,
			"updated_at": 1598400000
		},
		{
			"id": 51150,
			"updated_at": 1600905600
		},
		{
			"id": 51187,
			"updated_at": 1600905600
		},
		{
			"id": 136898,
			"updated_at": 1604620800
		},
		{
			"id": 127258,
			"updated_at": 1598400000
		}
	]


@pytest.fixture
def bayonetta_games():
	"""
	Mock results of hitting IGDB "/games" endpoint on Feb 24, 2021,
	with "fields name,first_release_date,summary,id,url;where id = (2136,2135,76888,28058,64207,51154,136077,51150,51187,136898,127258);limit 11;"

	This mocks calling IGDB().call()
	"""
	return [
		{
			"id": 2135,
			"age_ratings": [
				4651,
				28023
			],
			"category": 0,
			"first_release_date": 1411171200,
			"name": "Bayonetta 2",
			"summary": "The witching hour strikes again. Brimming with intricate battles that take place in, on and all over epic set pieces, Bayonetta 2 finds our sassy heroine battling angels and demons in unearthly beautiful HD. You’re bound to love how it feels to string together combos with unimaginable weapons and to summon demons using Bayonetta’s Umbran Weave in this frantic stylized action game.",
			"url": "https://www.igdb.com/games/bayonetta-2"
		},
		{
			"id": 2136,
			"age_ratings": [
				28024,
				28025
			],
			"category": 0,
			"first_release_date": 1256774400,
			"name": "Bayonetta",
			"summary": "A member of an ancient witch clan and possessing powers beyond the comprehension of mere mortals, Bayonetta faces-off against countless angelic enemies, many reaching epic proportions, in a game of 100% pure, unadulterated all-out action. Outlandish finishing moves are performed with balletic grace as Bayonetta flows from one fight to another. With magnificent over-the-top action taking place in stages that are a veritable theme park of exciting attractions, Bayonetta pushes the limits of the action genre, bringing to life its fast-paced, dynamic climax combat.",
			"url": "https://www.igdb.com/games/bayonetta"
		},
		{
			"id": 28058,
			"category": 0,
			"first_release_date": 1422835200,
			"name": "8-Bit Bayonetta",
			"summary": "\"SEGA’s favourite Umbran Witch climaxes her way onto PC! Thrill to her signature beehive do in 8-bit pixelated glory! Jump! Shoot! Score! Being bad never felt so retro. \" \n \nOriginally used as a mini-game on the 404 page of Platinum Games Japanese website in 2015. \n  \nSEGA\u0027s take on 2017 April fool joke. Ultimately the game was released on Steam to share an announcement website that has a countdown clock ending on April 11th.",
			"url": "https://www.igdb.com/games/8-bit-bayonetta"
		},
		{
			"id": 51150,
			"category": 0,
			"name": "Bayonetta 2: Special Edition",
			"url": "https://www.igdb.com/games/bayonetta-2-special-edition"
		},
		{
			"id": 51154,
			"category": 0,
			"first_release_date": 1416787200,
			"name": "Bayonetta 2 - Bonus Edition",
			"summary": "Bayonetta 2 (ベヨネッタ 2 Beyonetta Tsū?) is an action hack and slash video game developed by Platinum Games and published by Nintendo for the Wii U, with Sega as the franchise owners serving as its advisor. It is the direct sequel to the 2009 game, Bayonetta, and was directed by Yusuke Hashimoto and produced by Atsushi Inaba, under supervision by series creator Hideki Kamiya. It was announced on September 13, 2012, and will be exclusive to the Wii U, unlike the previous game which was only available on the PlayStation 3 and Xbox 360. The titular character, Bayonetta, sports a new costume and hairstyle and the game itself features a new two-player mode. The game is also the second Bayonetta product to receive Japanese voiceovers, using the same cast that voiced the Bayonetta: Bloody Fate anime film by Gonzo. The game was released in September 2014 and includes a port of the original Bayonetta as a separate disc inside the case. \n \nAlso Available \nBayonetta 2: Bonus Edition \nBayonetta 2: Special Edition \n \nBayonetta 2: First Print Edition \nDisc versions of both Bayonetta and Bayonetta 2, an art book, exclusive embossed packaging.",
			"url": "https://www.igdb.com/games/bayonetta-2-bonus-edition"
		},
		{
			"id": 51187,
			"category": 0,
			"name": "Bayonetta 2: First Print Edition",
			"url": "https://www.igdb.com/games/bayonetta-2-first-print-edition"
		},
		{
			"id": 64207,
			"category": 3,
			"name": "Bayonetta \u0026 Vanquish Pack",
			"summary": "A rumored/leaked bundle from Sega and platinum games to be released on Xbox One and PlayStation 4.",
			"url": "https://www.igdb.com/games/bayonetta-and-vanquish-pack"
		},
		{
			"id": 76888,
			"category": 0,
			"name": "Bayonetta 3",
			"summary": "Bayonetta is back. Bayonetta 3 is currently in development exclusively for Nintendo Switch.",
			"url": "https://www.igdb.com/games/bayonetta-3"
		},
		{
			"id": 127258,
			"age_ratings": [
				26543,
				27281,
				27282
			],
			"category": 3,
			"first_release_date": 1581984000,
			"name": "Bayonetta \u0026 Vanquish 10th Anniversary Bundle",
			"summary": "Experience the genesis of the Bayonetta series with the original action-adventure. Take advantage of Bayonetta’s arsenal of skills to hack, slash, and open fire upon hordes of celestial foes. \n \nPlay as space soldier Sam Gideon in the hit sci-fi shooter, Vanquish. Equipped with BLADE, the experimental weapon system that can scan and copy existing weapons, he must infiltrate conquered space colony Providence and defeat legions of future-tech enemies.",
			"url": "https://www.igdb.com/games/bayonetta-and-vanquish-10th-anniversary-bundle"
		},
		{
			"id": 136077,
			"category": 0,
			"name": "Bayonetta: Special Edition",
			"url": "https://www.igdb.com/games/bayonetta-special-edition"
		},
		{
			"id": 136898,
			"category": 3,
			"name": "Bayonetta: Nonstop Climax Edition",
			"url": "https://www.igdb.com/games/bayonetta-nonstop-climax-edition"
		}
	]


@pytest.fixture
def bayonetta_covers():
	"""
	Mock results of hitting IGDB "/covers" endpoint on Feb 24, 2021,
	with "fields game,url,height,width;where game = (2136,2135,76888,28058,64207,51154,136077,51150,51187,136898,127258);limit 11;"

	This mocks calling IGDB().call()
	"""
	return [
		{
			"id": 28058,
			"game": 46370,
			"height": 948,
			"image_id": "uwlu3ga2ztxfmsubf55k",
			"url": "//images.igdb.com/igdb/image/upload/t_thumb/uwlu3ga2ztxfmsubf55k.jpg",
			"width": 939,
			"checksum": "cb7db335-c43e-8502-19db-9f0f370a298e"
		},
		{
			"id": 51154,
			"game": 21501,
			"height": 398,
			"image_id": "mvx5cl0i0veotgggfmzc",
			"url": "//images.igdb.com/igdb/image/upload/t_thumb/mvx5cl0i0veotgggfmzc.jpg",
			"width": 280,
			"checksum": "84a08c3f-d34b-aaa5-849a-19c9cef9e405"
		},
		{
			"id": 51187,
			"game": 43233,
			"height": 311,
			"image_id": "ai9awozmmvgp6vwem7vh",
			"url": "//images.igdb.com/igdb/image/upload/t_thumb/ai9awozmmvgp6vwem7vh.jpg",
			"width": 350,
			"checksum": "749f7384-a9f3-4512-6329-d19d903b3395"
		},
		{
			"id": 76888,
			"game": 39213,
			"height": 512,
			"image_id": "co1nbs",
			"url": "//images.igdb.com/igdb/image/upload/t_thumb/co1nbs.jpg",
			"width": 512,
			"checksum": "062ba68f-5559-ef10-9f25-fec4d0fe6716"
		},
		{
			"id": 127258,
			"game": 80135,
			"height": 800,
			"image_id": "co2q6y",
			"url": "//images.igdb.com/igdb/image/upload/t_thumb/co2q6y.jpg",
			"width": 600,
			"checksum": "bc713385-f480-d667-44ab-0e6f2d6aade2"
		},
		{
		"id": 79382,
		"game": 2136,
		"height": 1871,
		"image_id": "co1p92",
		"url": "//images.igdb.com/igdb/image/upload/t_thumb/co1p92.jpg",
		"width": 1403,
		"checksum": "d4d41d46-6b1e-9ad0-0adb-b2f5bfcc96b9"
		}
	]


@pytest.fixture
def bayonetta_external_games():
	"""
	Mock results of hitting IGDB "/external_games" endpoint on Feb 24, 2021,
	with "fields *;where game = (2136,2135,76888,28058,64207,51154,136077,51150,51187,136898,127258) & category = (14, 1, 11);"

	This mocks calling IGDB().call()
	"""
	return [
		{
			"id": 246130,
			"category": 14,
			"created_at": 1519257600,
			"game": 2136,
			"name": "Bayonetta",
			"uid": "18933",
			"updated_at": 1519257600,
			"url": "https://www.twitch.tv/directory/game/Bayonetta",
			"checksum": "8acc85b5-fae1-836d-ed51-ecf9782b4b01"
		},
		{
			"id": 247057,
			"category": 14,
			"created_at": 1519257600,
			"game": 2135,
			"name": "Bayonetta 2",
			"uid": "68015",
			"updated_at": 1519257600,
			"url": "https://www.twitch.tv/directory/game/Bayonetta 2",
			"checksum": "b5c0f883-b0fc-3072-34ef-c9bf05d17025"
		},
		{
			"id": 213226,
			"category": 11,
			"created_at": 1516752000,
			"game": 2136,
			"name": "BAYONETTA",
			"uid": "xbox36066acd000-77fe-1000-9115-d80253450813",
			"updated_at": 1516752000,
			"url": "http://marketplace.xbox.com/en-US/Product/BAYONETTA/66acd000-77fe-1000-9115-d80253450813",
			"year": 2009,
			"checksum": "a8bc3962-efc3-0d20-094f-206add8f7162"
		},
		{
			"id": 5245,
			"category": 1,
			"created_at": 1494460800,
			"game": 2136,
			"name": "Bayonetta",
			"uid": "460790",
			"updated_at": 1494979200,
			"checksum": "4644ccf1-f61f-7ac3-1302-6cca82d9fcda"
		},
		{
			"id": 1907063,
			"category": 14,
			"created_at": 1595980800,
			"game": 76888,
			"name": "Bayonetta 3",
			"uid": "500621",
			"updated_at": 1598400000,
			"url": "https://www.twitch.tv/directory/game/Bayonetta%203",
			"checksum": "9917c379-5310-8e8b-bad8-28ac3876413d"
		},
		{
			"id": 1926087,
			"category": 14,
			"created_at": 1596067200,
			"game": 28058,
			"name": "8-Bit Bayonetta",
			"uid": "496455",
			"updated_at": 1598400000,
			"url": "https://www.twitch.tv/directory/game/8-Bit%20Bayonetta",
			"checksum": "895390f2-d44a-2af5-6d1a-20cc4169168f"
		},
		{
			"id": 1992080,
			"category": 14,
			"created_at": 1604620800,
			"game": 136898,
			"name": "Bayonetta: Nonstop Climax Edition",
			"uid": "1817765583",
			"updated_at": 1604620800,
			"url": "https://www.twitch.tv/directory/game/Bayonetta%3A%20Nonstop%20Climax%20Edition",
			"checksum": "ad46cdee-c048-638b-3186-b71b3b323d41"
		},
		{
			"id": 1966473,
			"category": 14,
			"created_at": 1604620800,
			"game": 64207,
			"name": "Bayonetta \u0026 Vanquish Pack",
			"uid": "960935768",
			"updated_at": 1604620800,
			"url": "https://www.twitch.tv/directory/game/Bayonetta%20%26%20Vanquish%20Pack",
			"checksum": "4d161087-dc9f-da44-bc3c-20770ab1d7cf"
		}
	]


@pytest.fixture
def smash_search():
	return [
		{
			"id": 1626,
			"updated_at": 1614669331
		},
		{
			"id": 9602,
			"updated_at": 1615845868
		}
	]


@pytest.fixture
def smash_games():
	return [
	{
		"id": 1626,
		"first_release_date": 916876800,
		"name": "Super Smash Bros.",
		"summary": "Super Smash Bros. is a crossover fighting video game between several different Nintendo franchises, and the first installment in the Super Smash Bros. series. Players must defeat their opponents multiple times in a fighting frenzy of items and power-ups. Super Smash Bros. is a departure from the general genre of fighting games: instead of depleting an opponent\u0027s life bar, the players seek to knock opposing characters off a stage. Each player has a damage total, represented by a percentage, which rises as the damage is taken.",
		"url": "https://www.igdb.com/games/super-smash-bros"
	},
	{
		"id": 9602,
		"first_release_date": 1416528000,
		"name": "Super Smash Bros. for Wii U",
		"summary": "Super Smash Bros. for Wii U is the fourth console-based entry in the popular Super Smash Bros. series of fighting games. With an even larger roster of fighters than its predecessors and the introduction eight player battles, Smash for Wii U continues to add to the formula that gamers have loved since 1999.",
		"url": "https://www.igdb.com/games/super-smash-bros-for-wii-u"
	}
]


@pytest.fixture
def smash_covers():
	return [
	{
		"id": 94749,
		"game": 9602,
		"height": 2000,
		"url": "//images.igdb.com/igdb/image/upload/t_thumb/co213x.jpg",
		"width": 1500
	},
	{
		"id": 131928,
		"game": 1626,
		"height": 800,
		"url": "//images.igdb.com/igdb/image/upload/t_thumb/co2tso.jpg",
		"width": 600
	}
]

@pytest.fixture
def smash_external_games():
	return [
	{
		"id": 246327,
		"category": 14,
		"game": 1626
	},
	{
		"id": 245258,
		"category": 14,
		"game": 9602
	}
]
