import { registered } from 'fixtures/user';

export const checkSessionSuccess = {
  body: {
    data: registered.data,
    success: true,
  },
  status: 201,
};

export const checkSessionFail = {
  body: {
    errors: ['Not Found.'],
    success: false,
  },
  status: 404,
};

export const loginSuccess = {
  body: {
    data: registered.data,
    success: true,
  },
  status: 200,
};

export const loginFail = {
  body: {
    errors: ['Authentication Failed.'],
    success: false,
  },
  status: 401,
};

export const logoutSuccess = {
  body: {
    data: {},
    success: true,
  },
  status: 201,
};

export const logoutFail = {
  body: {
    errors: ['Not Found.'],
    success: false,
  },
  status: 404,
};
