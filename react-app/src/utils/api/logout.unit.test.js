import fetchMock from 'fetch-mock-jest';

import { responses } from 'fixtures/session';

import logout from './logout';

const logoutMatcher = {
  url: '/api/sessions',
  method: 'DELETE',
};
const dispatch = jest.fn();

afterEach(() => fetchMock.restore());

// Tests
it('sends the data to the api and returns the response', async () => {
  // Arrange
  fetchMock.mock(logoutMatcher, responses.logoutSuccess);
  expect.assertions(2);
  // Act
  const [response, responseJSON] = await logout(() => {});
  // Assert
  expect(response.ok).toBe(true);
  expect(responseJSON).toEqual(responses.logoutSuccess.body);
});

it('calls CLEAR_USER dispatch', async () => {
  // Arrange
  fetchMock.mock(logoutMatcher, responses.logoutSuccess);
  expect.assertions(1);
  // Act
  await logout(dispatch);
  // Assert
  expect(dispatch).toHaveBeenCalledWith({
    type: 'CLEAR_USER',
  });
});
