echo "🌷 Welcome to your deployment. We will be deploying the refrigerator!\
 You know, they say that the refrigerator was running all this time,\
 but it's only now I feel it humming in my hands....."
DRY_RUN=true
while getopts 'c' opt; do
	case "$opt" in
		c)
			DRY_RUN=false
			;;
		*) echo 'Error in command line parsing' >&2
           exit 1
	esac
done
if "$DRY_RUN"; then
	echo "🐇 This is a Dry Run, as c flag was not passed. Pass -c to run for real."
else
	echo "🐉 c flag passed. We are running for real."
fi

if test -f "requirements.txt"; then
    echo "💔 requirements.txt exists. This is a Pipfile shop goddamnit....wait, it's prod time. I suppose it's a Pipfile.lock shop!"
    exit 1
else
	echo "💜 Oh good, you haven't given us a requirements.txt. Pipfile.lock for the win..."
fi

if [ "$(which pipenv)" == "" ]
then
	echo "💔 pipenv not found"
	exit 1
else
	echo "💜 pipenv is installed, good"
fi

echo "🌸 Checking for existing virtualenv"
case "No virtualenv has been created" in
  *$(pipenv --venv)*)
	echo "🧰 No virtualenv found. Creating one for you now, which will reflect current state of Pipfile.lock"
	pipenv install --ignore-pipfile
    ;;
esac

check_packages() {
	case "$2" in
		added)
			echo "$1" | awk '/</ {print $2}'
			;;
		removed)
			echo "$1" | awk '/>/ {print $2}'
			;;
	esac
}
echo "😱 Let's have a look at package changes"
PIPENV_DIFF=$(diff  \
	<(echo "$(pipenv -q requirements --exclude-markers)" ) \
	<(echo "$(pipenv -q run pip freeze)") -i
)
echo "🧰 Added packages:"
echo "  😊 Please ignore a line that reads \"-i\""
echo "  😊 Don't worry if you see setuptools in here"
check_packages "$PIPENV_DIFF" "added"

echo "🧻 Removed packages"
check_packages "$PIPENV_DIFF" "removed"

if "$DRY_RUN"; then
	echo "🐇 Dry run"
	echo "   🐇 Not syncing from Pipfile.lock"
	echo "   🐇 Not restarting refrigerator service"
else
	echo "🐉 Oh gurl, this is not a dry run!"
	echo "🧻 running pipenv clean"
	pipenv clean
	echo "🧰 running pipenv sync"
	pipenv sync

	echo "🏂 Restarting the Refrigerator service!"
	sudo systemctl daemon-reload
	sudo systemctl restart refrigerator
fi

echo "😅 Running \`systemctl is-active refrigerator\`"
case "$(systemctl is-active refrigerator)" in
	active)
		echo "🌠 Refrigerator is running!"
		;;
	inactive)
		echo "😳 Hmm, seems that refrigerator is inactive. Maybe you still need to create the service?"
		;;
	failed)
		echo "😰😰😰 OH NO 😰😰😰"
		echo "   😰 The Refrigerator is NOT running!"
		echo "   😰 \`systemctl is-active refrigerator\` returned \"failed\""
		echo "   😰 might want to consider rolling back to a better commit and trying again :/"
esac

echo "🦋 If you're just setting up, here are some variables you might need for refrigerator.service:"
echo "   🦠 Working Directory is: ${PWD}"
echo "   🦠 The venv location is: $(pipenv --venv -q)"
