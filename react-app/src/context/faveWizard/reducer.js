import { makeReducer } from 'utils';

import initialState from './initialState';

const reducer = makeReducer(initialState);

export default reducer;
