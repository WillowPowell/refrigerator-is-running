import datetime

from refrigerator.models.faves import Fave
from refrigerator.utils.time import check_datetime_format

from tests.utils import check_response

#Constants

#ORIGINAL constants are the values set for the Fave returned by the `registered_fave` fixture.
ORIGINAL_PHRASE = "I like that the cat is named after that rascal from that book."
ORIGINAL_HOURS_PLAYED = 100
ORIGINAL_TOP_FAVE = False

UPDATED_PHRASE = "I've started imagining that his name was Levin."
UPDATED_HOURS_PLAYED = 200
UPDATED_TOP_FAVE = True

#Helpers
def edit_fave(client, fave_id, body):
	return client.patch(f"/api/faves/{fave_id}", json=body)


#Tests
def test_success_one_field(logged_in_client, registered_fave):
	response = edit_fave(logged_in_client, registered_fave.id, {
			"top_fave" : UPDATED_TOP_FAVE
		})
	check_response(response, 200)
	fave = response.json.get("data")
	assert fave
	#Check that only the requested field has changed.
	assert fave["top_fave"] == UPDATED_TOP_FAVE
	assert fave["phrase"] == ORIGINAL_PHRASE
	#Make sure we are successfully setting the `updated_at` field.
	assert check_datetime_format(fave["updated_at"])


def test_success_multiple_fields(logged_in_client, registered_fave):
	response = edit_fave(logged_in_client, registered_fave.id, {
			"top_fave" : UPDATED_TOP_FAVE,
			"phrase" : UPDATED_PHRASE,
			"hours_played" : UPDATED_HOURS_PLAYED
		})
	check_response(response, 200)
	fave = response.json.get("data")
	#Check that all of the requested fields have changed.
	assert fave["top_fave"] == UPDATED_TOP_FAVE
	assert fave["phrase"] == UPDATED_PHRASE
	assert fave["hours_played"] == UPDATED_HOURS_PLAYED


def test_success_delete(logged_in_client, registered_fave):
	"""Setting deleted to True should update deleted_at field as well."""
	response = edit_fave(logged_in_client, registered_fave.id, {
			"deleted" : True
		})
	check_response(response, 200)
	fave = response.json.get("data")
	assert fave["deleted"] == True
	#Make sure deleted_at has been set and sent in correct format.
	assert check_datetime_format(fave["deleted_at"])
	assert registered_fave.deleted


def test_success_undelete(logged_in_client, deleted_fave):
	"""
	If the Fave is already deleted,
	setting deleted to False should update the deleted_at field to None.
	"""
	response = edit_fave(logged_in_client, deleted_fave.id, {
			"deleted" : False
		})
	check_response(response, 200)
	fave = response.json.get("data")
	assert fave["deleted"] == False
	#Setting deleted to false should set "deleted_at" to None.
	assert fave["deleted_at"] == None
	assert not deleted_fave.deleted


def test_not_logged_in(client, registered_fave):
	response = edit_fave(client, registered_fave.id, {
			"deleted" : True
		})
	check_response(response, 401, error="Unauthorized.", success=False)
	assert not registered_fave.deleted


def test_wrong_user(logged_in_client_2, registered_fave):
	"""
	Users are not allowed to edit each other's Faves.
	`registered_fave` is owned by `registered_user`.
	`logged_in_client_2` is not allowed to edit it.
	"""
	response = edit_fave(logged_in_client_2, registered_fave.id, {
			"deleted" : True
		})
	check_response(response, 401, error="Unauthorized.", success=False)
	assert not registered_fave.deleted


def test_not_found(logged_in_client, registered_user):
	"""
	This test does not access `registered_fave` fixture.
	This means there are no  Faves to access :(
	"""
	response = edit_fave(logged_in_client, 1, {
			"deleted" : True
		})
	check_response(response, 404, error="Fave Not Found.", success=False)


def test_edit_game_id(logged_in_client, registered_user, registered_fave):
	"""
	Editing `game_id` is explicitly not allowed.
	"""
	response = edit_fave(logged_in_client, 1, {
			"game_id" : 1234
		})
	check_response(response, 400, error="Cannot edit game_id. Create a new Fave instead.", success=False)


def test_bad_data(logged_in_client, registered_user, registered_fave):
	"""
	Gotta follow the rules.
	"""
	response = edit_fave(logged_in_client, registered_fave.id, {
			"top_fave" : "True"
		})
	check_response(response, 400, error="Incorrect format for top_fave. Expected <class 'bool'>.", success=False)
