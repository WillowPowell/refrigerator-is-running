import React from 'react';
import PropTypes from 'prop-types';

import Field from './helpers/Field';

/**
* Accepts the same props as `Field`, but defaults appropriate values.
*/
export default function TextArea({
  value, // React state getter
  setValue, // React state setter
  name,
  label,
  borderColor,
  ...props // Additional props to pass into the `input` tag, like `required`.
}) {
  return (
    <Field
      name={name}
      label={label}
    >
      <textarea
        value={value}
        onChange={(e) => setValue(e.target.value)}
        name={name}
        id={`${name}-input`}
        className={`textarea${borderColor ? ` is-${borderColor}` : ''}`}
        {...props}
      />
    </Field>
  );
}

TextArea.propTypes = {
  value: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  borderColor: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(null),
  ]),
};

TextArea.defaultProps = {
  borderColor: 'primary',
};
