# Welcome to The Refrigerator
A website to manage your favourite video games. Deployed on Heroku at <https://refrigerator.willowpowell.com>.

## Overview
The Refrigerator allows users to search for, select, and manage their favourite video games.

### Front End

The front end is a JavaScript application using [React](https://reactjs.org/), bootstrapped with [Create React App](https://create-react-app.dev/), and using the [Bulma](https://bulma.io/) CSS framework.

The testing suite for the front end uses the [Jest](https://jestjs.io/) test runner and the [Testing Library](https://testing-library.com/) framework.

### Back End

The back end is a Python application using the [Flask](https://flask.palletsprojects.com/) microframework. Notably, it interacts with the [IGDB API](https://api-docs.igdb.com/) to search for video games, which requires authentication through a [Twitch Developer](https://dev.twitch.tv/docs/authentication) app.

The testing suite for the backend uses the [Pytest](https://docs.pytest.org/) framework.

## Deployment

The front end and back end are two separate applications, and are deployed separately. For more information on setting up for development and deploying to production, check the documentation in the `react-app` and `flask-api` folders.

## Features

### Search for your favourite game:
<img src="./screenshots/ChooseGameBrowser.png" aria-label="The user searches for Smash Bros and select Smash Bros Ultimate." width="800" />

### Tell us why you love it:
<img src="./screenshots/EditFaveBrowser.png" aria-label="The user enters 'I dominate the competition' as the reason they love the game, and that they have played for 400 hours." width="800" />

### Generate excitement:
<img src="./screenshots/ViewFave.png" aria-label="The cover image for Smash Bros Ultimate, along with additional information about the game. The user's phrase 'I dominate the competition' is displayed in the classic Super Nintendo font." width="800" />

### Manage all your favourite games in one place:
<img src="./screenshots/FavesList.png" aria-label="A list of the user's favourite games: GRIS, Minecraft, and Smash Bros Ultimate, with options to edit and delete the faves." width="800" />

### Use it on mobile too:
<img src="./screenshots/FavesListMobile.png" aria-label="On mobile, less information is shown but the delete and edit buttons are still available." width="500" />
