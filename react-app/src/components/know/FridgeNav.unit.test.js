/* global customRender */
/* global mockPushHistory */
import React from 'react';
import { act } from 'react-dom/test-utils';
import { fireEvent, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { logout } from 'utils/api';
import { registered } from 'fixtures/user';
import { lotsOfFaves as favesData } from 'fixtures/fave';

import logo from 'utils/Refrigerator-Horizontal-Logo.png';
import FridgeNav from './FridgeNav';

jest.mock('utils/api');

function renderFridgeNav({ authState = {}, favesState = {} } = {}) {
  customRender(<FridgeNav />, {
    authState,
    favesState,
  });
}

it('renders without a user', () => {
  renderFridgeNav();
  expect(screen.getByAltText('Refrigerator logo.')).toHaveAttribute('src', logo);
  expect(screen.queryByRole('button', { name: 'Log Out' })).not.toBeInTheDocument();
  expect(screen.queryByRole('link', { name: 'View All Faves' })).not.toBeInTheDocument();
});

it('renders with a user', () => {
  renderFridgeNav({ authState: { user: registered.data } });
  expect(screen.getByRole('button', { name: 'expand menu' })).not.toHaveClass('is-active');
  expect(screen.getByRole('navigation', { name: 'menu' })).not.toHaveClass('is-active');
  expect(screen.getByRole('button', { name: 'Log Out' })).toBeInTheDocument();
});

it('renders with a user and the burger is clicked', () => {
  renderFridgeNav({ authState: { user: registered.data } });
  act(() => { userEvent.click(screen.getByRole('button', { name: 'expand menu' })); });
  expect(screen.getByRole('button', { name: 'expand menu' })).toHaveClass('is-active');
  expect(screen.getByRole('navigation', { name: 'menu' })).toHaveClass('is-active');
});

it('logs out with a click', async () => {
  renderFridgeNav({ authState: { user: registered.data } });
  userEvent.click(screen.getByRole('button', { name: 'Log Out' }));
  expect(logout).toHaveBeenCalledWith(expect.any(Function));
  await waitFor(() => expect(mockPushHistory).toHaveBeenCalledWith('/'));
});

it('logs out with a keypress', async () => {
  renderFridgeNav({ authState: { user: registered.data } });
  fireEvent.keyDown(
    screen.getByRole('button', { name: 'Log Out' }),
    { key: 'Enter', code: 'Enter' },
  );
  expect(logout).toHaveBeenCalledWith(expect.any(Function));
  await waitFor(() => expect(mockPushHistory).toHaveBeenCalledWith('/'));
});

it('renders with lots of faves', () => {
  renderFridgeNav({
    authState: { user: registered.data },
    favesState: { faves: favesData },
  });
  expect(screen.getByText('More Faves')).toBeInTheDocument();
  favesData.forEach(async (fave, i) => {
    const link = screen.queryByRole('link', { name: fave.game.name });
    expect(link).toBeInTheDocument();
    userEvent.click(link);
    await waitFor(() => expect(mockPushHistory).toHaveBeenCalledWith(`/fave/${i}`));
  });
});

it('renders with one fave', () => {
  renderFridgeNav({
    authState: { user: registered.data },
    favesState: { faves: [favesData[0]] },
  });
  expect(screen.getByRole('link', { name: favesData[0].game.name })).toBeInTheDocument();
  expect(screen.queryByText('More Faves')).not.toBeInTheDocument();
  expect(screen.queryByText('View All Faves')).toBeInTheDocument();
});

it('closes the menu when the location changes', async () => {
  renderFridgeNav({
    authState: { user: registered.data },
    favesState: { faves: [favesData[0]] },
  });
  userEvent.click(screen.getByRole('button', { name: 'expand menu' }));
  userEvent.click(screen.getByRole('link', { name: favesData[0].game.name }));
  await waitFor(() => expect(screen.getByRole('button', { name: 'expand menu' })).not.toHaveClass('is-active'));
  await waitFor(() => expect(screen.getByRole('navigation', { name: 'menu' })).not.toHaveClass('is-active'));
});
