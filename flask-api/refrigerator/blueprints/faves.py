from flask import Blueprint, request, session, abort
from sqlalchemy.orm import joinedload

from refrigerator.app import db
from refrigerator.models.faves import Fave
from refrigerator.models.users import User
from refrigerator.models.games import Game

from refrigerator.utils.response import make_response, owns_resource, require_authentication, type_check_errors

bp = Blueprint('faves', __name__, url_prefix='/api/faves')


@bp.route('', methods=('POST', 'GET'))
@require_authentication
def faves():
	if request.method == 'POST':
		return create_fave(request)

	return get_faves(request)

def get_faves(request):
	"""
	Get a user's Faves.
	Faves are filtered by url params and session[user_id].
	Allowed url_params:
		limit=20, - number of results to return
		offset=0, - pagination, usually a multiple of limit or 0
		deleted=False, - default to not returning deleted Faves
		top_fave - can filter by only Top Faves.
	Returns a list of Fave dicts.
	"""
	status_code = 200
	success = True
	data = []
	errors = []
	allowed_params = {
		'limit' : int,
		'offset' : int,
		'deleted' : bool,
		'top_fave' : bool
	}
	errors += type_check_errors(request.args, query_string=True, **allowed_params)
	if errors:
		success = False
		status_code = 400
		return make_response(status_code, success, data, errors)
	#Build filters
	filters = {}
	#Boolean options will be set to False if any value other than "True" (case insensitive) is sent.
	#deleted defaults to False.
	filters['deleted'] = False
	if 'deleted' in request.args and request.args['deleted'].lower() == "true":
		filters['deleted'] = True
	#Only add top_fave to filters if specifically requested.
	if 'top_fave' in request.args:
		if request.args['top_fave'].lower() == "true":
			filters['top_fave'] = True
		else:
			filters['top_fave'] = False
	#Get the Faves. If no faves match filters, return 404.
	faves = Fave.query.options(joinedload(Fave.game)) \
		.filter_by(user_id=session['user_id'], **filters) \
		.limit(int(request.args.get('limit', 20))) \
		.offset(int(request.args.get('offset', 0))) \
		.all()
	if not faves:
		status_code = 404
		success = False
		errors.append("No Faves Found.")
		return make_response(status_code, success, data, errors)

	#If we found some Faves, add them to the response data and return!
	data = [fave.to_response_dict(include_game_dict=True) for fave in faves]
	return make_response(status_code, success, data, errors)

def create_fave(request):
	fave_fields = {
		'game_id': int,
		'top_fave': bool,
		'phrase': str,
		'hours_played': int
	}
	status_code = 201
	success = True
	data = {}
	errors = []

	#Validate the request data.
	#game_id is required.
	if not 'game_id' in request.json:
		errors.append("No game_id Provided.")
	#Typecheck request fields.
	errors += type_check_errors(request.json, game_id=int, top_fave=bool, phrase=str, hours_played=int)
	if errors:
		success = False
		status_code = 400
		return make_response(status_code, success, data, errors)

	#Find the related resources.
	user = User.query.get(session['user_id'])
	game = Game.query.get_or_404(request.json['game_id'], description="Game Not Found.")

	# Check to see if a conflicting Fave already exists.
	conflict = Fave.query.filter_by(user_id=user.id, game_id=game.id).first()
	if conflict:
		# If fave was deleted, un-delete and edit it.
		if conflict.deleted == True:
			conflict.deleted = False
			for f in ("top_fave", "phrase", "hours_played"):
				setattr(conflict, f, request.json.get(f))
			db.session.add(conflict)
			db.session.commit()
			data = conflict.to_response_dict()
			return make_response(status_code, success, data, errors)

		# If the data is different than what we have in store, return a 409.
		# User should edit the conflicting Fave instead.
		for f in ("top_fave", "phrase", "hours_played"):
			if not getattr(conflict, f) == request.json.get(f):
				errors.append(f"Conflict with fave {conflict.id}. Has game_id {game.id} and user_id {user.id}.")
				success = False
				status_code = 409
				return make_response(status_code, success, data, errors)
		# Otherwise, it's probably just a duplicate request.
		# Return the conflicting Fave instead.
		else:
			data = conflict.to_response_dict()
			return make_response(status_code, success, data, errors)

	# If we're all good, make and return the new Fave!
	fave = Fave(
		user=user,
		game=game,
		top_fave=request.json.get('top_fave'),
		phrase=request.json.get('phrase'),
		hours_played=request.json.get('hours_played'))
	db.session.add(fave)
	db.session.commit()
	data = fave.to_response_dict()
	return make_response(status_code, success, data, errors)

@bp.route('/<int:faves_id>', methods=('PATCH', 'DELETE'))
@require_authentication
@owns_resource(Fave)
def my_fave(faves_id):
	if request.method == 'DELETE':
		return delete_fave(faves_id, request)

	return edit_fave(faves_id, request)

def edit_fave(faves_id, request):
	"""
	Logged in users can edit their own Faves.
	Cannot edit `game_id`.
	Editable fields:
		'top_fave': bool
		'phrase': str
		'hours_played': int
		'deleted': bool
	`deleted` is special:
		Setting to True also sets deleted_at to the current time.
		Setting to True will omit this Fave from GET requests, by default.
	"""
	editable_fields = {
		'top_fave': bool,
		'phrase': str,
		'hours_played': int,
		'deleted': bool
	}
	status_code = 200
	success = True
	data = {}
	errors = []

	#Handle bad requests.
	if 'game_id' in request.json:
		errors.append("Cannot edit game_id. Create a new Fave instead.")
	errors += type_check_errors(request.json, **editable_fields)
	if errors:
		abort(400, errors)

	#Make the edits and return the edited Fave.
	#Iterate over `editable_fields` to avoid any not-allowed changes.
	#We know the Fave exists because the @owns_resource decorator checks this for us.
	fave = Fave.query.options(joinedload(Fave.game)).get(faves_id)
	for field in editable_fields.keys():
		if field in request.json and request.json[field] != getattr(fave, field):
			setattr(fave, field, request.json[field])
	db.session.add(fave)
	db.session.commit()
	#Create the response!
	data = fave.to_response_dict(include_game_dict=True)
	return make_response(status_code, success, data, errors)

def delete_fave(faves_id, request):
	"""
	Delete a Fave!
	request contains:
		permanent=False:
			If True, deletes the Fave from the database.
			If False, just sets `Fave.deleted` to True.
	"""
	status_code = 200
	success = True
	data = {}
	errors = []
	fave = Fave.query.get(faves_id)

	if request.content_type == 'application/json' and request.get_json().get('permanent') == True:
		db.session.delete(fave)
		db.session.commit()
	else:
		fave.deleted = True
		db.session.add(fave)
		db.session.commit()
		data = fave.to_response_dict()
	return make_response(status_code, success, data, errors)
