import json
import datetime

from refrigerator.models.games import Game

from tests.utils import check_game, check_response

#Helpers
def add_game(client, body):
	return client.post("/api/games", json=body)

#Constants
NAME_TO_ADD = "Smudgie Goes to Seattle"
RELEASE_DATE_TO_ADD = "Oct 17, 2001"
COVER_URL_TO_ADD = "https://lh3.googleusercontent.com/pw/ACtC-3csnK70zE3Vo1QvEqoMoROYnbwUy0ISL4JgLzFoRLKcS-KV2kgnjbfpXRTa5f-ooEj7h2GUSxcFwh0AE43jDMZa2X6Ityeh4bQxwI0AHt1jhGQSC7gxJHcfV3vJPL682mGjbx12ytCKvUoB8Asp3yax2A=w1204-h903-no"
COVER_WIDTH_TO_ADD = 1204
COVER_HEIGHT_TO_ADD = 903
SUMMARY_TO_ADD = "It was kinda rainy."
IGDB_ID_TO_ADD = 678
IGDB_URL_TO_ADD = "https://www.igdb.com/games/smudgie-goes-to-seattle"
TWITCH_ID_TO_ADD = 901

#Tests
def test_success(client):
	response = add_game(client, {
		"name" : NAME_TO_ADD,
		"first_release_date" : RELEASE_DATE_TO_ADD,
		"cover_url" : COVER_URL_TO_ADD,
		"cover_width" : COVER_WIDTH_TO_ADD,
		"cover_height" : COVER_HEIGHT_TO_ADD,
		"summary" : SUMMARY_TO_ADD,
		"igdb_id" : IGDB_ID_TO_ADD,
		"igdb_url" : IGDB_URL_TO_ADD,
		"twitch_id" : TWITCH_ID_TO_ADD
  })
	check_response(response, 201)
	game = response.json["data"]
	assert game
	check_game(
		game,
		name=NAME_TO_ADD,
		first_release_date=RELEASE_DATE_TO_ADD,
		cover_url=COVER_URL_TO_ADD,
		cover_width=COVER_WIDTH_TO_ADD,
		cover_height=COVER_HEIGHT_TO_ADD,
		summary=SUMMARY_TO_ADD,
		igdb_id=IGDB_ID_TO_ADD,
		igdb_url=IGDB_URL_TO_ADD,
		twitch_id=TWITCH_ID_TO_ADD
	)
	game = Game.query.filter_by(name=NAME_TO_ADD).first()
	assert game
	assert repr(game) == f"<Game {repr(game.name)}>"

def test_no_name(client):
	response = add_game(client, {
		"first_release_date" : RELEASE_DATE_TO_ADD,
		"cover_url" : COVER_URL_TO_ADD,
		"cover_width" : COVER_WIDTH_TO_ADD,
		"cover_height" : COVER_HEIGHT_TO_ADD,
		"summary" : SUMMARY_TO_ADD,
		"igdb_id" : IGDB_ID_TO_ADD,
		"igdb_url" : IGDB_URL_TO_ADD,
		"twitch_id" : TWITCH_ID_TO_ADD
		})
	check_response(response, 400, success=False, error="Required Field name Not Provided.")
	assert not Game.query.filter_by(name=NAME_TO_ADD).first()

def test_bad_data(client):
	response = add_game(client, {
		"name" : 12345,
		"first_release_date" : RELEASE_DATE_TO_ADD,
		"cover_url" : COVER_URL_TO_ADD,
		"cover_width" : COVER_WIDTH_TO_ADD,
		"cover_height" : COVER_HEIGHT_TO_ADD,
		"summary" : SUMMARY_TO_ADD,
		"igdb_id" : IGDB_ID_TO_ADD,
		"igdb_url" : IGDB_URL_TO_ADD,
		"twitch_id" : TWITCH_ID_TO_ADD
		})
	check_response(response, 400, success=False, error="Incorrect format for name. Expected <class 'str'>.")
	assert not Game.query.filter_by(name=NAME_TO_ADD).first()
	response = add_game(client, {
		"name" : NAME_TO_ADD,
		"first_release_date" : RELEASE_DATE_TO_ADD,
		"cover_url" : COVER_URL_TO_ADD,
		"cover_width" : COVER_WIDTH_TO_ADD,
		"cover_height" : COVER_HEIGHT_TO_ADD,
		"summary" : SUMMARY_TO_ADD,
		"igdb_id" : str(IGDB_ID_TO_ADD),
		"igdb_url" : IGDB_URL_TO_ADD,
		"twitch_id" : TWITCH_ID_TO_ADD
		})
	check_response(response, 400, success=False, error="Incorrect format for igdb_id. Expected <class 'int'>.")
	assert not Game.query.filter_by(name=NAME_TO_ADD).first()

def test_bad_date_format(client):
	response = add_game(client, {
		"name" : NAME_TO_ADD,
		"first_release_date" : "Thu, 13 Dec 2018 00:00:00 GMT",
		"cover_url" : COVER_URL_TO_ADD,
		"cover_width" : COVER_WIDTH_TO_ADD,
		"cover_height" : COVER_HEIGHT_TO_ADD,
		"summary" : SUMMARY_TO_ADD,
		"igdb_id" : IGDB_ID_TO_ADD,
		"igdb_url" : IGDB_URL_TO_ADD,
		"twitch_id" : TWITCH_ID_TO_ADD
		})
	check_response(response, 400, success=False, error="Incorrect format for first_release_date. Should be like 'Oct 21, 1929'.")
	assert not Game.query.filter_by(name=NAME_TO_ADD).first()

def test_igdb_id_conflict(client, registered_game):
	response = add_game(client, {
		"name" : NAME_TO_ADD,
		"first_release_date" : RELEASE_DATE_TO_ADD,
		"cover_url" : COVER_URL_TO_ADD,
		"cover_width" : COVER_WIDTH_TO_ADD,
		"cover_height" : COVER_HEIGHT_TO_ADD,
		"summary" : SUMMARY_TO_ADD,
		"igdb_id" : registered_game.igdb_id,
		"igdb_url" : IGDB_URL_TO_ADD,
		"twitch_id" : TWITCH_ID_TO_ADD
		})
	check_response(response, 409, success=False, error=f"Game {registered_game.id} With IGDB ID {registered_game.igdb_id} Exists")
	assert not Game.query.filter_by(name=NAME_TO_ADD).first()

def test_twitch_id_conflict(client, registered_game):
	response = add_game(client, {
		"name" : NAME_TO_ADD,
		"first_release_date" : RELEASE_DATE_TO_ADD,
		"cover_url" : COVER_URL_TO_ADD,
		"cover_width" : COVER_WIDTH_TO_ADD,
		"cover_height" : COVER_HEIGHT_TO_ADD,
		"summary" : SUMMARY_TO_ADD,
		"igdb_id" : IGDB_ID_TO_ADD,
		"igdb_url" : IGDB_URL_TO_ADD,
		"twitch_id" : registered_game.twitch_id
		})
	check_response(response, 409, success=False, error=f"Game {registered_game.id} With Twitch ID {registered_game.twitch_id} Exists")
	assert not Game.query.filter_by(name=NAME_TO_ADD).first()
