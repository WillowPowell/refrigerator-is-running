"""The tests in this file actually engage TwitchAuth and IGDB APIs."""

import pytest

from refrigerator.models.games import Game
from refrigerator.models.handlers import get_twitch_auth

from tests.utils import check_game, check_response, find_game

# Constants
QUERY_STRING = {
		"query": "GRIS",
		"limit": 20,
		"offset": 0
	}


def test_success(twitch_enabled_client):
	"""
	Make sure our IGDB connection is up and running
	and we can search for the beautiful game 'GRIS'.
	"""
	response = twitch_enabled_client.get('/api/games', query_string=QUERY_STRING)
	check_response(response, 200)
	gris = find_game(response.json["data"], 22917)
	check_game(gris)
	assert Game.query.filter_by(igdb_id=22917).first()


def test_handle_error(monkeypatch, twitch_enabled_client):
	"""
	If we have a bad access token and try to hit the IGDB API,
	the access token should be updated.
	"""
	twitch_auth = get_twitch_auth()
	monkeypatch.setattr(twitch_auth, "access_token", "invalid!")
	response = twitch_enabled_client.get('/api/games', query_string=QUERY_STRING)
	check_response(response, 200)
	# Override `token_validated_at` to get around the 30 second cache.
	monkeypatch.setattr(twitch_auth, "token_validated_at", None)
	assert twitch_auth.valid_access_token
