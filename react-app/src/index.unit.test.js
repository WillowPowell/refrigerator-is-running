/**
* This test protects against unexpected changes to index.js
*/
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import { AuthProvider } from 'context/auth';
import { FavesProvider } from 'context/faves';

import App from './App';

jest.mock('react-dom', () => ({
  render: jest.fn(),
}));

it('renders the App with the correct wrapper and calls reportWebVitals', () => {
  const container = document.createElement('div');
  container.id = 'root';
  document.body.appendChild(container);
  // eslint-disable-next-line global-require, import/extensions
  require('./index.js');
  expect(ReactDOM.render).toHaveBeenCalledWith((
    <React.StrictMode>
      <Router>
        <AuthProvider>
          <FavesProvider>
            <App />
          </FavesProvider>
        </AuthProvider>
      </Router>
    </React.StrictMode>),
  container);
});
