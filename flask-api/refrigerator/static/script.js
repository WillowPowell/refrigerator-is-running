document.addEventListener('DOMContentLoaded', function() {
  document.getElementById("mainBurger").addEventListener("click", function(){
    this.classList.toggle("is-active");
    document.getElementById(this.dataset.target).classList.toggle("is-active");
  });
  document.getElementById("refrigerator-modal-background").addEventListener("click", function(){
    document.getElementById("refrigerator-modal").classList.toggle("is-active");
  });
  document.getElementById("refrigerator-modal-close").addEventListener("click", function(){
    document.getElementById("refrigerator-modal").classList.toggle("is-active");
  });
  addListenerToClass("click","add-game-button",addGameButtons);
});

function addListenerToClass(event,targetClass,callback,parent=document) {
  //add event listener to elements of a given class
  //defaults to targeting the entire document
  //@param event: string: event to listen for
  //@param targetClass: str: the class of the elements to target
  //@param callback: function: the function to call when event detected
  //@param parent: HTML element: parent of the elements to target
  if (parent) {
    const list = parent.getElementsByClassName(targetClass);
    for (let i = 0; i < list.length; i++) {
      list[i].addEventListener(event,callback)
    }
  }
}

function addGameButtons() {
  const button = this;
  button.classList.toggle("is-loading");
  const gameId = button.dataset.game;
  fetch('add_game',{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({game_to_add : gameId })
  })
  .then(response => response.json())
  .then(data => {
    if ( data.error ) {
      errorModal("Error","Failed to add game.");
      button.classList.toggle("is-loading");
    } else {
      button.outerHTML = "Successfully added as game #" + data.game_added + "."
    }
  });
}

function errorModal(header,body) {
  document.getElementById("refrigerator-modal").classList.toggle("is-active");
  document.getElementById("error-header").textContent = header;
  document.getElementById("error-body").textContent = body;
}
