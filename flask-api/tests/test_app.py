import os
import logging
from logging import StreamHandler
from logging.handlers import SMTPHandler, TimedRotatingFileHandler
from smtplib import SMTPServerDisconnected

import pytest

from flask import Flask

from refrigerator.app import twitch_auth, FridgeSMTPHandler

from tests.utils import check_response


def test_prod_logging(caplog, monkeypatch, safe_create_app, test_config):
	"""
	Make sure log handlers are being added as expected.

	Actually adding the log handlers seems to break pytest's internals,
	so we patch `addHandler` after setting up the pytest logger,
	and just keep track of which handlers would have been added.
	"""
	test_config["TEST_PROD_LOGGING"] = True
	handlers = []

	def mock_add_handler(self, hdlr):
		handlers.append(hdlr)

	with caplog.at_level(logging.INFO):
		monkeypatch.setattr(logging.Logger, "addHandler", mock_add_handler)
		app = safe_create_app(test_config=test_config)
		client = app.test_client()
		client.get("/fake")

	assert len(handlers) == 3
	assert isinstance(handlers[0], StreamHandler)
	assert isinstance(handlers[1], TimedRotatingFileHandler)
	assert isinstance(handlers[2], FridgeSMTPHandler)
	assert handlers[0].level == 50
	assert handlers[1].level == 20
	assert handlers[2].level == 40
	assert "Found mail server configuration. Creating SMTPHandler." in caplog.text
	assert f"SMTPHandler set up for {app}. Error messages will be sent to {app.config['MAIL_ADMINS']}." in caplog.text
	assert f"{app} logging configured. Log handlers: " in caplog.text
	assert "Finished building refrigerator!" in caplog.text


def test_fridge_smtp_handler(caplog, capsys, monkeypatch, test_config):
	"""
	Make sure our SMTPHandler subclass behaves as expected.

	This avoids sending real emails,
	and simply keeps track of the data that would have been sent.
	"""
	msg = {}

	def mock_emit(error):

		def inner(self, record):
			if error:
				# This recreates the pattern in `SMTPHandler.emit`.
				try:
					raise error
				except Exception:
					self.handleError(record)
			else:
				msg["MAILHOST"] = self.mailhost
				msg["PORT"] = self.mailport
				msg["From"] = self.fromaddr
				msg["To"] = ','.join(self.toaddrs)
				msg['Subject'] = self.getSubject(record)
				msg["CONTENT"] = (self.format(record))

		return inner

	def create_app(error=None):
		"""
		Set up a Flask app with FridgeSMTPHandler,
		and have the handler raise `error`.
		"""
		monkeypatch.setattr(SMTPHandler, "emit", mock_emit(error))
		app = Flask(__name__)
		app.config.from_mapping(test_config)
		mail_handler = FridgeSMTPHandler(app,
			mailhost=(app.config["MAIL_SERVER"], app.config["MAIL_PORT"]),
			fromaddr=app.config["MAIL_USERNAME"],
			toaddrs=app.config["MAIL_ADMINS"],
			subject="TEST Refrigerator Log")
		mail_handler.setLevel(logging.ERROR)
		app.logger.addHandler(mail_handler)
		with caplog.at_level(logging.DEBUG):
			app.logger.error("TEST ERROR")

	# Don't raise an error, and test the logging and the message contents.
	create_app()
	assert f"Sending log to {test_config['MAIL_ADMINS']}" in caplog.text
	assert "Finished sending error logs by email." in caplog.text
	assert msg["MAILHOST"] == test_config["MAIL_SERVER"]
	assert msg["PORT"] == test_config["MAIL_PORT"]
	assert msg["From"] == test_config["MAIL_USERNAME"]
	assert msg["To"] == ','.join(test_config["MAIL_ADMINS"])
	assert msg['Subject'] == "TEST Refrigerator Log: ERROR in test_app"
	assert msg["CONTENT"] == "TEST ERROR"

	# Test custom error handling for `SMTPServerDisconnected`.
	create_app(error=SMTPServerDisconnected)
	captured = capsys.readouterr()
	assert not captured.err
	assert "Likely trying to send a duplicate email. " in caplog.text
	assert "ERROR in test_app" in caplog.text

	# Make sure we haven't broken `SMTPHandler`'s Exception handling.
	create_app(error=Exception)
	captured = capsys.readouterr()
	assert captured.err.startswith("--- Logging error ---")


def test_dev_logging(caplog, monkeypatch, safe_create_app, test_config):
	monkeypatch.setenv("FLASK_ENV", "development")
	with caplog.at_level(logging.DEBUG):
		app = safe_create_app(test_config)
	assert "Started in development mode." in caplog.text


def test_handle_exception(caplog, client):
	"""Test generic HTTPException handler."""
	with caplog.at_level(logging.ERROR):
		response = client.get('/fake')
	check_response(response, 404, success=False,
		error="The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again.")
	assert "Caught HTTPException" in caplog.text


def test_test_config(safe_create_app, test_config):
	"""Make sure `safe_create_app` is actually safe!"""
	app = safe_create_app(test_config=test_config)
	assert app.instance_path == test_config["TEST_INSTANCE_PATH"]
	assert not getattr(twitch_auth, "access_token", None)


def test_welcome(client):
	response = client.get('/')
	assert response.json["data"] == "Welcome to The Refrigerator"
