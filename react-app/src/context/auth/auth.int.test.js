import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { act, renderHook } from '@testing-library/react-hooks';

import { registered } from 'fixtures/user';
import { AuthProvider, useAuthContext } from './context';

import initialState from './initialState';

const wrapper = ({ route, testInitialState, children }) => (
  <MemoryRouter initialEntries={[route || '/']}>
    <AuthProvider testInitialState={testInitialState || null}>
      {children}
    </AuthProvider>
  </MemoryRouter>
);

it('renders with initial state', () => {
  const { result } = renderHook(() => useAuthContext(), { wrapper });
  expect(result.current.authState).toEqual(initialState);
});

it('sets user', () => {
  const { result } = renderHook(() => useAuthContext(), { wrapper });
  act(() => {
    result.current.authDispatch({
      type: 'SET_USER',
      payload: registered.data,
    });
  });
  expect(result.current.authState.user).toEqual(registered.data);
});

it('clears user', () => {
  const { result } = renderHook(() => useAuthContext(), {
    wrapper,
    initialProps: {
      testInitialState: {
        user: registered.data,
      },
    },
  });
  expect(result.current.authState.user).toEqual(registered.data);
  act(() => {
    result.current.authDispatch({
      type: 'CLEAR_USER',
    });
  });
  expect(result.current.authState.user).toEqual(null);
});

it('sets sessionChecked', () => {
  const { result } = renderHook(() => useAuthContext(), {
    wrapper,
  });
  act(() => {
    result.current.authDispatch({
      type: 'SET_SESSION_CHECKED',
      payload: true,
    });
  });
  expect(result.current.authState).toEqual({
    ...initialState,
    sessionChecked: true,
  });
});
