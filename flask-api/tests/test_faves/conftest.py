import pytest

from refrigerator.app import db
from refrigerator.models.games import Game
from refrigerator.models.faves import Fave


@pytest.fixture
def pages_of_faves(registered_user):
	"""
	Create a large number of Games and Faves.
	Facilitates testing paging.
	"""
	for i in range(50):
		game = Game(
			name="Zariel Goes To Avernus",
			summary="She stayed a lot longer than expected."
		)
		db.session.add(game)
		#Must commit to create id.
		db.session.commit()
		fave = Fave(
			user_id=registered_user.id,
			game_id=game.id,
			phrase="I can't help but wonder what she's up to now.",
			hours_played=1000000,
			top_fave=True
		)
		db.session.add(fave)
	db.session.commit()


@pytest.fixture()
def registered_fave(registered_user, registered_game):
	fave = Fave(
		user_id=registered_user.id,
		game_id=registered_game.id,
		phrase="I like that the cat is named after that rascal from that book.",
		hours_played=100,
		top_fave=False
	)
	db.session.add(fave)
	db.session.commit()
	return fave


@pytest.fixture()
def registered_fave_2(registered_user, registered_game_2):
	#Supplying this fixture and registered_fave
	#Gives registered_user 2 faves.
	fave = Fave(
		user_id=registered_user.id,
		game_id=registered_game_2.id,
		phrase="It kinda looks like she wanted to be alone.",
		hours_played=31,
		top_fave=True
	)
	db.session.add(fave)
	db.session.commit()
	return fave


@pytest.fixture
def deleted_fave(registered_fave):
	registered_fave.deleted = True
	db.session.add(registered_fave)
	db.session.commit()
	return registered_fave


@pytest.fixture
def registered_fave_3(registered_game, registered_user_2):
	"""
	Create a fave of registered game for registered user 2.
	This will help for authorization tests.
	"""
	fave = Fave(
		user_id=registered_user_2.id,
		game_id=registered_game.id,
		phrase="I always enjoy a story of homecoming.",
		hours_played=5,
		top_fave=False
	)
	db.session.add(fave)
	db.session.commit()
	return fave
