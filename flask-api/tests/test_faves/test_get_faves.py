from tests.utils import check_response

#Helpers
def get_faves(client, query_string_dict):
	return client.get("/api/faves", query_string=query_string_dict)

#Tests
def test_success_default(logged_in_client, registered_fave, registered_fave_2, registered_fave_3):
	"""
	By default we don't send any querystring parameters.
	The default filters are:
		session["user_id"]
		limit=20
		offset=0
		deleted=False
	We've supplied 3 Fave fixtures:
		registered_fave - owned by registered_user
		registered_fave_2 - owned by registered_user
		registered_fave_3 - owned by registered_user_2
	session["user_id"] should filter out registered_fave_3
	None of the other filters meaningfully apply to the supplied fixtures.
	So we should get 2 results.
	"""
	response = get_faves(logged_in_client, {})
	check_response(response, 200)
	#First time we're returning a list, so check that.
	assert isinstance(response.json["data"], list)
	#Make sure we're getting 2 Faves.
	assert len(response.json["data"]) == 2
	for fave in response.json["data"]:
		assert "id" in fave

def test_deleted(logged_in_client, deleted_fave, registered_fave_2):
	"""
	Test the behaviour of the deleted filter.
	By default, deleted Faves are not returned.
	Fixtures supply us with a user with 2 faves, 1 deleted.
	"""
	response = get_faves(logged_in_client, {})
	check_response(response, 200)
	#Make sure we're only getting 1 Fave.
	assert len(response.json["data"]) == 1
	#Make sure it's the right one.
	assert response.json["data"][0]["id"] == registered_fave_2.id

	#Do it again but set explicitly to False.
	response = get_faves(logged_in_client, {
			"deleted" : False
		})
	#Should have same results.
	check_response(response, 200)
	assert len(response.json["data"]) == 1
	assert response.json["data"][0]["id"] == registered_fave_2.id

	#Now set the deleted filter to True.
	response = get_faves(logged_in_client, {
			"deleted" : True
		})
	check_response(response, 200)
	#Make sure we're only getting 1 Fave.
	assert len(response.json["data"]) == 1
	#Make sure it's the right one.
	assert response.json["data"][0]["id"] == deleted_fave.id

def test_top_fave(logged_in_client, registered_fave, registered_fave_2):
	"""
	Test the behaviour of the top_fave filter.
	This filter has no default, so the "default" case is handled in test_success_default.
	Fixtures supply us with a user with 2 faves:
		registered_fave has top_fave=False
		registered_fave_2 has top_fave=True
	"""
	response = get_faves(logged_in_client, {
			"top_fave" : False
		})
	check_response(response, 200)
	#Make sure we're only getting 1 Fave.
	assert len(response.json["data"]) == 1
	#Make sure it's the right one.
	assert response.json["data"][0]["id"] == registered_fave.id

	#Now do the same thing again, but set to True.
	response = get_faves(logged_in_client, {
			"top_fave" : True
		})
	check_response(response, 200)
	#Make sure we're only getting 1 Fave.
	assert len(response.json["data"]) == 1
	#Make sure it's the right one.
	assert response.json["data"][0]["id"] == registered_fave_2.id

def test_paging(logged_in_client, registered_user, pages_of_faves):
	"""
	Paging controlled by limit and offset parameters.
		limit defaults to 20
		offset defaults to 0
		pages_of_faves has 50 faves.
	"""
	#Default should return 20 results.
	response = get_faves(logged_in_client, {})
	assert len(response.json["data"]) == 20
	#Use the limit parameter to get 15 results.
	response = get_faves(logged_in_client, {
			"limit" : 15
		})
	page_1 = response.json["data"]
	assert len(page_1) == 15
	#Use offset to get page_2.
	response = get_faves(logged_in_client, {
			"limit" : 15,
			"offset" : 15
		})
	page_2 = response.json["data"]
	assert len(page_2) == 15
	#Make sure no overlap in the two pages.
	for p1_result in page_1:
		for p2_result in page_2:
			assert p1_result["id"] != p2_result["id"]
	#The last page should only have 5 results.
	response = get_faves(logged_in_client, {
			"limit" : 15,
			"offset" : 45
		})
	assert len(response.json["data"]) == 5

def test_unauthorized(client):
	response = get_faves(client, {})
	check_response(response, 401, error="Unauthorized.", success=False)

def test_no_faves(logged_in_client, registered_user_2):
	response = get_faves(logged_in_client, {})
	check_response(response, 404, error="No Faves Found.", success=False)

def test_bad_data(logged_in_client, registered_user):
	"""
	We're only testing "limit" and "offset".
	The other options are boolean, and any value can be supplied.
	"""
	response = get_faves(logged_in_client, {
			"limit" : "Twenty",
			"offset" : "None"
		})
	check_response(response, 400, success=False, error="Incorrect format for limit. Expected <class 'int'>.")
	check_response(response, 400, success=False, error="Incorrect format for offset. Expected <class 'int'>.")
