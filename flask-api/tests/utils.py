from requests.models import Response

from refrigerator.utils.response import validate_data
from refrigerator.utils.time import check_date_format, check_datetime_format


class MockResponse(Response):
	"""Simple copy of `Response` where the `json` method, `url`, and `status_code` are overwritten."""

	def __init__(self, url="", json_data={}, status_code=200):
		super().__init__()
		self.json_data = json_data
		self.status_code = status_code
		self.url = url

	def json(self):
		return self.json_data


def check_response(response, status_code=int, success=True, error=""):
	assert str(status_code) in response.status, f"{status_code} not found in {response.status}"
	assert response.json['success'] == success, f"{response.json['success']} Does Not Match {success}"
	if error:
		assert error in response.json['errors'], f"{error} Not Found In {response.json['errors']}"
	else:
		assert 'errors' not in response.json, f"'errors' Found In {response.json}"


def check_user(response, email, name, verified=False, updated_at=False):
	user = response.json["data"]
	assert user
	assert "id" in user
	assert "password" not in user
	assert user["email"] == email
	assert user["name"] == name
	assert user["verified"] == verified
	assert check_datetime_format(user["created_at"])
	assert check_datetime_format(user["updated_at"]) if updated_at else user["updated_at"] == None


def check_game(game, updated=False, **kwargs):
	'''
	Handy tool for game checking.
	param: game = A game object returned by the refrigerator API.
	kwargs: key/value pairs will be checked against the returned game.
	'''
	game_fields = [
		"id",
		"name",
		"first_release_date",
		"cover_url",
		"cover_url_big",
		"cover_width",
		"cover_height",
		"summary",
		"igdb_id",
		"igdb_url",
		"twitch_id",
		"created_at",
		"updated_at"
	]
	for gf in game_fields:
		assert gf in game, f"{gf} not in {game}"
	for f in game:
		assert f in game_fields, f"unexpected field {k} included in response"
	for k, v in kwargs.items():
		assert game[k] == v, f"{k} field does not match. {game[k]} != {v}"
	if game["first_release_date"] != None:
		assert check_date_format(game["first_release_date"])
	else:
		assert game["first_release_date"] == None
	assert check_datetime_format(game["created_at"])
	if updated:
		assert check_datetime_format(game["updated_at"])

def find_game(data, igdb_id):
	for game in data:
		if 'igdb_id' in game:
			if game['igdb_id'] == igdb_id:
				return game
