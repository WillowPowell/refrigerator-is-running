/* global customRender */
/* global mockPushHistory */
import React from 'react';
import { screen, waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { register } from 'utils/api';
import { unregistered } from 'fixtures/user';

import RegistrationForm from './RegistrationForm';

jest.mock('utils/api');
beforeEach(() => {
  customRender(<RegistrationForm />);
});

it('renders', () => {
  // Assert
  expect(screen.getByText(/register/i)).toBeInTheDocument();
  expect(screen.getByRole('textbox', { name: 'What should we call you?' })).toBeInTheDocument();
  expect(screen.getByRole('textbox', { name: "What's your email address?" })).toBeInTheDocument();
  expect(screen.getByLabelText(/password/i)).toBeInTheDocument();
  expect(screen.getByRole('button', { name: 'Submit' })).toBeInTheDocument();
  expect(screen.getByRole('link', { name: 'Log In' })).toBeInTheDocument();
});

it('renders and the user types their name', () => {
  // Act
  userEvent.type(screen.getByRole('textbox', { name: 'What should we call you?' }), unregistered.name);
  // Assert
  // Header and test input should change.
  expect(screen.getByRole('textbox', { name: 'What should we call you?' }))
    .toHaveValue(unregistered.name);
});

it('submits successfully', async () => {
  register.mockImplementation(async (name, email, password) => {
    if (name === unregistered.name
        && email === unregistered.email
        && password === unregistered.password) {
      const response = { ok: true };
      const responseJSON = { data: unregistered.data };
      return [response, responseJSON];
    }
    return undefined;
  });
  window.alert = jest.fn();
  // Act
  userEvent.type(screen.getByRole('textbox', { name: 'What should we call you?' }), unregistered.name);
  userEvent.type(screen.getByRole('textbox', { name: "What's your email address?" }), unregistered.email);
  userEvent.type(screen.getByLabelText(/password/i), unregistered.password);
  userEvent.click(screen.getByRole('button', { name: 'Submit' }));
  // Assert
  await waitFor(() => expect(window.alert).toHaveBeenCalledWith(
    `New account created for ${unregistered.email}. Please log in.`,
  ));
  await waitFor(() => expect(mockPushHistory).toHaveBeenCalledWith('/user/login'));
});

it('submits and gets an error', async () => {
  register.mockImplementation(async (name, email, password) => {
    if (name === unregistered.name
        && email === unregistered.email
        && password === 'password') {
      const response = { ok: false };
      const responseJSON = { errors: ['Passsword Not Strong Enough.'] };
      return [response, responseJSON];
    }
    return undefined;
  });
  window.alert = jest.fn();
  // Act
  userEvent.type(screen.getByRole('textbox', { name: 'What should we call you?' }), unregistered.name);
  userEvent.type(screen.getByRole('textbox', { name: "What's your email address?" }), unregistered.email);
  userEvent.type(screen.getByLabelText(/password/i), 'password');
  userEvent.click(screen.getByRole('button', { name: 'Submit' }));
  // Assert
  await waitFor(() => expect(window.alert).toHaveBeenCalledWith('Passsword Not Strong Enough.'));
});
