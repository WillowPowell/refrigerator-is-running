import unregistered from './unregistered';

export const registerSuccess = {
  body: {
    data: unregistered.data,
    success: true,
  },
  status: 201,
};
export const registerFail = {
  body: {
    errors: ['Password Not Strong Enough.'],
    success: false,
  },
  status: 400,
};
