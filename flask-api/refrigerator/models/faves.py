import datetime

from refrigerator.app import db
from refrigerator.utils.time import DATETIME_FORMAT

class Fave(db.Model):
	__tablename__ = "faves"
	id = db.Column(db.Integer, primary_key=True)

	user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
	user = db.relationship('User', back_populates='faves')

	game_id = db.Column(db.Integer, db.ForeignKey('games.id'), nullable=False)
	game = db.relationship('Game', back_populates='faves')

	top_fave = db.Column(db.Boolean)
	phrase = db.Column(db.String)
	hours_played = db.Column(db.Integer)

	deleted = db.Column(db.Boolean, nullable=False, default=False)

	created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow())
	updated_at = db.Column(db.DateTime, onupdate=datetime.datetime.utcnow())

	def __update_deleted_at(context):
		'''
		If the deleted field is being updated, update deleted_at as well.
		'''
		if 'deleted' in context.get_current_parameters():
			fave = Fave.query.get(context.get_current_parameters()['faves_id'])
			if context.get_current_parameters()['deleted'] == True:
				return datetime.datetime.utcnow()
			return None

	deleted_at = db.Column(db.DateTime, onupdate=__update_deleted_at)

	def __repr__(self):
		return f"<Fave {self.id}>"

	def to_response_dict(self, include_game_dict=False):
		res = {
			"id" : self.id,
			"user_id" : self.user_id,
			"game_id" : self.game_id,
			"top_fave" : self.top_fave,
			"phrase" : self.phrase,
			"hours_played" : self.hours_played,
			"deleted" : self.deleted,
			"created_at" : datetime.datetime.strftime(self.created_at, DATETIME_FORMAT),
			"updated_at" : datetime.datetime.strftime(self.updated_at, DATETIME_FORMAT) if self.updated_at else None,
			"deleted_at" : datetime.datetime.strftime(self.deleted_at, DATETIME_FORMAT) if self.deleted_at else None
		}
		if include_game_dict:
			res["game"] = self.game.to_response_dict()
		return res
