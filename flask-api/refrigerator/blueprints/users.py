from flask import Blueprint, request
from werkzeug.security import generate_password_hash

from refrigerator.app import db
from refrigerator.models.users import User, password_strength
from refrigerator.utils.response import validate_data, make_response

bp = Blueprint("users", __name__, url_prefix="/api/users")


@bp.route("", methods=("POST", ))
@validate_data(required_fields=("name", "email", "password"),
	constraints=(("password", password_strength, "Password Not Strong Enough"), ),
	name=str, email=str, password=str)
def register():
	status_code = 201
	success = True
	data = {}
	errors = []
	if User.query.filter_by(email=request.json["email"]).first():
		errors.append("User With Email Exists.")
		status_code = 409
		success = False
		return make_response(status_code, success, data, errors)
	user = User(
		email=request.json["email"],
		password=generate_password_hash(request.json["password"]),
		name=request.json["name"])
	db.session.add(user)
	db.session.commit()
	data = user.to_response_dict()
	return make_response(status_code, success, data, errors)
