import { makeReducer } from 'utils';
import initialState from './initialState';

function favesReducer(currentState, action) {
  switch (action.type) {
    case 'SET_FAVE': {
      const updatedFaves = currentState.faves;
      updatedFaves[action.index] = action.payload;
      return {
        ...currentState,
        faves: updatedFaves,
      };
    }
    case 'SET_ALL_FAVES': {
      return {
        ...currentState,
        faves: action.payload,
      };
    }
    case 'SET_SESSION_CHECKED': {
      return {
        ...currentState,
        sessionChecked: action.payload,
      };
    }
    default: {
      throw new Error(`favesReducer received an unexpected action ${action.type}`);
    }
  }
}

const reducer = makeReducer(initialState, favesReducer);

export default reducer;
