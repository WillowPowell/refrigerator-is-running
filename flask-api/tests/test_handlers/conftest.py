import os
import pytest
import datetime

from refrigerator.app import create_app, db
from refrigerator.models.handlers import get_twitch_auth
from refrigerator.models.games import Game

from tests.utils import MockResponse


@pytest.fixture
def mock_twitch_auth_responses():

	def outer(revoke_response=({}, 200),
		update_response=({"access_token":"abcd123"}, 200),
		valid_response=({}, 200)):

		def inner(*args, **kwargs):
			r = MockResponse(url=args[1])

			if r.url == "https://id.twitch.tv/oauth2/revoke":
				r.json_data = revoke_response[0]
				r.status_code = revoke_response[1]
			if r.url == "https://id.twitch.tv/oauth2/token":
				r.json_data = update_response[0]
				r.status_code = update_response[1]
			if r.url == "https://id.twitch.tv/oauth2/validate":
				r.json_data = valid_response[0]
				r.status_code = valid_response[1]

			return r

		return inner

	return outer


@pytest.fixture
def twitch_enabled_client(test_config):
	"""Create an app with the full config, including setting up TwitchAuth."""

	app = create_app(test_config=test_config)

	with app.app_context():
		db.create_all()

		twitch_auth = get_twitch_auth()
		token_at_setup = twitch_auth.access_token

		yield app.test_client()

		# Try to make sure we revoke any access tokens generated during testing.
		# However, if multiple tokens were generated, this will only revoke two.
		if token_at_setup != twitch_auth.access_token:
			twitch_auth.revoke_access_token(token=token_at_setup)
		get_twitch_auth().revoke_access_token()

		db.session.remove()
		db.drop_all()
