import os

from dotenv import load_dotenv

load_dotenv()

class Config(object):
	SECRET_KEY = os.getenv("SECRET_KEY")

	DATABASE_URL = os.getenv("DATABASE_URL")

	TEST_PROD_LOGGING = os.getenv("TEST_PROD_LOGGING") is not None

	# SQLAlchemy has deprecated the prefix 'postgres://', but it's the default on Heroku.
	if DATABASE_URL.startswith('postgres://'):
		SQLALCHEMY_DATABASE_URI = DATABASE_URL.replace('postgres://', 'postgresql://')
	else:
		SQLALCHEMY_DATABASE_URI = DATABASE_URL

	SQLALCHEMY_TRACK_MODIFICATIONS = False

	TWITCH_CLIENT_ID = os.getenv("TWITCH_CLIENT_ID")
	TWITCH_CLIENT_SECRET = os.getenv("TWITCH_CLIENT_SECRET")
	TWITCH_ACCESS_TOKEN = os.getenv("TWITCH_ACCESS_TOKEN")

	INFO_LOG = os.getenv("INFO_LOG")

	LOG_FORMATTER = "[%(asctime)s] %(levelname)s in %(module)s: %(message)s"

	LOG_ROTATION_UNIT = os.getenv("LOG_ROTATION_UNIT") or "d"
	LOG_ROTATION_INTERVAL = os.getenv("LOG_ROTATION_INTERVAL") or 1
	LOG_ROTATION_BACKUPS = os.getenv("LOG_ROTATION_BACKUPS") or 5

	MAIL_SERVER = os.getenv("MAIL_SERVER")
	MAIL_PORT = int(os.getenv("MAIL_PORT") or 25)
	MAIL_USE_TLS = os.getenv("MAIL_USE_TLS") is not None
	MAIL_USERNAME = os.getenv("MAIL_USERNAME")
	MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")
	MAIL_FROM_ADDRESS = os.getenv("MAIL_FROM_ADDRESS")

	if os.getenv("MAIL_ADMINS"):
		MAIL_ADMINS = [email for email in os.getenv("MAIL_ADMINS").split(",")]
	else:
		MAIL_ADMINS = None
