from flask import session

from tests.utils import check_response, check_user

#Helpers
def check_session(client):
	return client.get("api/sessions")


#Tests
def test_check_session_success(logged_in_client, REGISTERED_EMAIL, REGISTERED_NAME):
	"""
	If there is an active session,
	should get a 200 response along with user data.
	"""
	with logged_in_client:
		response = check_session(logged_in_client)
		check_response(response, 200)
		check_user(response, REGISTERED_EMAIL, REGISTERED_NAME)


def test_check_session_failure(client):
	"""`client` fixture is not logged in, so we expect a 404."""
	with client:
		response = check_session(client)
		check_response(response, 404, success=False, error="Not Found.")
