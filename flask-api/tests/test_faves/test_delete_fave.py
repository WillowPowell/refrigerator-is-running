import datetime

from refrigerator.models.faves import Fave
from refrigerator.utils.time import check_datetime_format

from tests.utils import check_response

#Constants

#Helpers
def delete_fave(client, fave_id, body):
	return client.delete(f"/api/faves/{fave_id}", json=body)

#Tests
def test_success_default(logged_in_client, registered_fave):
	"""
	By default, deleting a Fave:
		sets fave.deleted to True
		updates the deleted_at field
	"""
	response = delete_fave(logged_in_client, registered_fave.id, {})
	check_response(response, 200)
	fave = response.json["data"]
	#The deleted Fave should be in the response.
	assert fave["id"] == registered_fave.id
	#It should have deleted set to True.
	assert response.json["data"]["deleted"] == True
	#Make sure we are successfully setting the `deleted_at` field.
	assert check_datetime_format(fave["deleted_at"])

def test_success_permanent(logged_in_client, registered_fave):
	"""
	Use the permanent field to permanently delete a Fave.
	"""
	response = delete_fave(logged_in_client, registered_fave.id, {
			"permanent" : True
		})
	check_response(response, 200)
	assert response.json["data"] == {}
	assert not Fave.query.get(registered_fave.id)
