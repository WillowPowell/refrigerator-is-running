from refrigerator.models.users import User
from refrigerator.utils.time import check_datetime_format

from tests.utils import check_response

#Constants
EMAIL_TO_REGISTER = "witty_willow@example.com"
NAME_TO_REGISTER = "Willow the Witty"
PASSWORD_TO_REGISTER = "bxbdmxb2|xe0xfdxcbq*gxb2W"


#Helpers
def register(client, body):
	return client.post("/api/users", json=body)


#Tests
def test_success(client):
	response = register(client, {
		"email": EMAIL_TO_REGISTER,
		"name": NAME_TO_REGISTER,
		"password": PASSWORD_TO_REGISTER
	})
	check_response(response, 201)
	user = response.json["data"]
	assert user
	assert "id" in user
	assert "password" not in user
	assert user["email"] == EMAIL_TO_REGISTER
	assert user["name"] == NAME_TO_REGISTER
	assert user["verified"] == False
	assert check_datetime_format(user["created_at"])
	assert user["updated_at"] == None

	user = User.query.filter_by(email=EMAIL_TO_REGISTER).first()
	assert user
	assert repr(user) == f"<User {repr(user.email)}>"


def test_missing_data(client):
	response = register(client, {
		"name": NAME_TO_REGISTER,
		"password": PASSWORD_TO_REGISTER
		})
	check_response(response, 400, success=False, error="Required Field email Not Provided.")
	response = register(client, {
		"email": EMAIL_TO_REGISTER,
		"name": NAME_TO_REGISTER
		})
	check_response(response, 400, success=False, error="Required Field password Not Provided.")
	response = register(client, {
		"email": EMAIL_TO_REGISTER,
		"password": PASSWORD_TO_REGISTER
		})
	check_response(response, 400, success=False, error="Required Field name Not Provided.")
	assert not User.query.filter_by(email=EMAIL_TO_REGISTER).first()


def test_weak_password(client):
	response = register(client, {
		"email": EMAIL_TO_REGISTER,
		"name": NAME_TO_REGISTER,
		"password": "password"
	})
	check_response(response, 400, success=False, error="Password Not Strong Enough")
	assert not User.query.filter_by(email=EMAIL_TO_REGISTER).first()


def test_bad_data(client):
	response = register(client, {
		"email": 1234,
		"name": NAME_TO_REGISTER,
		"password": PASSWORD_TO_REGISTER
	})
	check_response(response, 400, success=False, error="Incorrect format for email. Expected <class 'str'>.")
	response = register(client, {
		"email": EMAIL_TO_REGISTER,
		"name": 5678,
		"password": PASSWORD_TO_REGISTER
	})
	check_response(response, 400, success=False, error="Incorrect format for name. Expected <class 'str'>.")
	response = register(client, {
		"email": EMAIL_TO_REGISTER,
		"name": NAME_TO_REGISTER,
		"password": 9101112
	})
	check_response(response, 400, success=False, error="Incorrect format for password. Expected <class 'str'>.")


def test_email_conflict(client, registered_user, REGISTERED_EMAIL):
	response = register(client, {
		"email": REGISTERED_EMAIL,
		"name": NAME_TO_REGISTER,
		"password": PASSWORD_TO_REGISTER
	})
	check_response(response, 409, success=False, error="User With Email Exists.")
