import { login as apiCall } from 'utils/api';
import { waitFor } from '@testing-library/dom';

import { registered } from 'fixtures/user';

import login from './login';

jest.mock('utils/api');

const dispatch = jest.fn();
const history = {
  replace: jest.fn(),
};

beforeEach(() => {
  apiCall.mockImplementation(async (d, email, password) => {
    const response = { ok: false };
    const responseJSON = { errors: ['Unexpected Error.'] };
    if (email === registered.email) {
      if (password === registered.password) {
        response.ok = true;
        responseJSON.data = registered.data;
      } else if (password === 'notmypassword') {
        responseJSON.errors = ['Authentication Failed.'];
      }
    }
    return [response, responseJSON];
  });
});

it('handles a successful login with default target', async () => {
  const loginAction = login(registered.email, registered.password);
  loginAction(dispatch, { history });
  expect(apiCall).toHaveBeenCalledWith(dispatch, registered.email, registered.password);
  await waitFor(() => expect(history.replace).toHaveBeenCalledWith('/'));
});

it('handles a successful login with target supplied', async () => {
  const loginAction = login(registered.email, registered.password, '/selidor');
  loginAction(dispatch, { history });
  await waitFor(() => expect(history.replace).toHaveBeenCalledWith('/selidor'));
});

it('handles a failed login', async () => {
  const loginAction = login(registered.email, 'notmypassword');
  loginAction(dispatch, { history });
  await waitFor(() => expect(window.alert).toHaveBeenCalledWith('Authentication Failed.'));
});
