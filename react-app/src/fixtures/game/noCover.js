const noCover = {
  cover_height: null,
  cover_url: null,
  cover_url_big: null,
  cover_width: null,
  created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
  first_release_date: null,
  id: 109,
  igdb_id: 64207,
  igdb_url: 'https://www.igdb.com/games/bayonetta-and-vanquish-pack',
  name: 'Bayonetta & Vanquish Pack',
  summary: 'A rumored/leaked bundle from Sega and platinum games to be released on Xbox One and PlayStation 4.',
  twitch_id: 1966473,
  updated_at: null,
};

export default noCover;
