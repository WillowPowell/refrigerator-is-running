from flask import Blueprint
bp = Blueprint('welcome', __name__)

@bp.route('/', methods=(['GET', ]))
def welcome():
	"""
	"the welcome page
	":return: a welcome message
	"""
	return {"data" : "Welcome to The Refrigerator, Oct 16, 2022."}, 200
