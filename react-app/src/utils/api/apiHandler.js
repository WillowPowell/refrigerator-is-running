export default function apiHandler(resource, init, myFetch = fetch) {
  return myFetch(process.env.REACT_APP_API_URL + resource, {
    ...init,
    credentials: 'include',
  });
}
