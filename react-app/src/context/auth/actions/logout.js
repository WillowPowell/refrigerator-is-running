import { logout as apiCall } from 'utils/api';

export default async function logout(dispatch, { history }) {
  await apiCall(dispatch);
  history.push('/');
}
