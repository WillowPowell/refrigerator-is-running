/* global mockPushHistory */
import { act, renderHook } from '@testing-library/react-hooks';

import useCustomReducer from './useCustomReducer';

const mockInitialState = {
  someKey: 'foo',
  someOtherKey: 'Guybrush',
};
const mockInitHelper = {
  secretKey: 'humungous',
};
const mockTarget = '/selidor';

function mockReducer(currentState, action) {
  switch (action.type) {
    case 'SET_SOME_KEY': {
      return {
        ...currentState,
        someKey: action.payload,
      };
    }
    default: {
      throw new Error('Unrecognized action type.');
    }
  }
}

function mockInit(iS) {
  return {
    ...iS,
    ...mockInitHelper,
  };
}

function mockThunkAction(dispatch, { history }) {
  dispatch({
    type: 'SET_SOME_KEY',
    payload: 'baz',
  });
  history.push(mockTarget);
}

it('sets up initialState without init', () => {
  const { result } = renderHook(() => useCustomReducer(mockReducer, mockInitialState));
  expect(result.current[0]).toEqual(mockInitialState);
});

it('sets up initialState with init', () => {
  const { result } = renderHook(() => useCustomReducer(mockReducer, mockInitialState, mockInit));
  expect(result.current[0]).toEqual({
    ...mockInitialState,
    ...mockInitHelper,
  });
});

it('dispatches a normal action', () => {
  const { result } = renderHook(() => useCustomReducer(mockReducer, mockInitialState));
  act(() => {
    result.current[1]({
      type: 'SET_SOME_KEY',
      payload: 'bar',
    });
  });
  expect(result.current[0]).toEqual({
    ...mockInitialState,
    someKey: 'bar',
  });
});

it('dispatches a thunk action and provides history', () => {
  const { result } = renderHook(() => useCustomReducer(mockReducer, mockInitialState));
  act(() => {
    result.current[1](mockThunkAction);
  });
  expect(result.current[0]).toEqual({
    ...mockInitialState,
    someKey: 'baz',
  });
  expect(mockPushHistory).toHaveBeenCalledWith(mockTarget);
});
