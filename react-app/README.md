# Setting up for Development

After you've cloned the repo, you have a few steps to get set up for front end development. You should set up the back end first, as the front end relies on communication with the API.

The instructions below assume you already have the back end running at <http://localhost:5000>

## Install Dependencies
To install dependencies, run:

```bash
$ cd react-app
$ npm install --include=dev
```

## Scripts
Most of the available scripts are straight from `create-react-app`.

### Start your development environment
```bash
$ npm start
```
This will compile the app and open <http://localhost:3000> in a new browser tab.

### Run the tests
```bash
$ npm test
```
You don't need to have the back end running to run the tests, as the test suite stubs out `fetch` throughout to avoid API calls.

By default, this only runs tests related to changed files. To run all tests:
```bash
$ npm test -- --watchAll
```
> Note the `--` before the `--watchAll` flag. You need to follow this pattern any time you want to pass flags to `jest`.

## Test Coverage
The Refrigerator maintains a very high test coverage percentage. At the time this is being written, it sits at 96% for the front end.

After making changes, run:

```bash
$ npm run coverage
```
The Refrigerator team will not approve merge requests that lower the test coverage.

# Deploying to Production
The front end is deployed in production on Heroku [here](https://refrigerator.willowpowell.com). It is deployed independently from the back end, and proxies api requests to the back end as necessary.

The following instructions walk you through creating a new Refrigerator front end on Heroku.

## Creating the app
Use the [Heroku Buildpack for create-react-app](https://github.com/mars/create-react-app-buildpack) for the frontend.

Assuming you have `heroku` installed, and have an account that's logged in, run:
```bash
$ heroku create -a my-refrigerator-frontend -r heroku-frontend --buildpack mars/create-react-app
```

### Flags:
* -a: The app name that will be created in your Heroku account.
* -r: The name of the git remote that will be added.

If you change these flags, modify the commands below appropriately.

## Deploying the code
Since the front end and backend are housed in the same repo, you need to use `git subtree push` to deploy code and make updates:

```bash
$ git subtree push --prefix react-app heroku-frontend main
```

To add flags when pushing:

```bash
$ git push heroku-frontend `git subtree split --prefix react-app main`:main --flag
```

replacing `--flag` with the desired flags.

## Setting Environment Variables
Follow the instructions in the [Heroku documentation](https://devcenter.heroku.com/articles/config-vars) to set the following variables:

### API_URL
The url for the back end. For example, if you deployed to <https://my-refrigerator-backend.herokuapp.com>, you should set the config var to that URL.

## Access Production Logs:
```bash
$ heroku logs -a my-refrigerator-frontend
```

Add the `--tail` flag to monitor the logs in real-time.

## Rejoice
Don't forget to rejoice in your accomplishments, and, above all: **Enjoy your new Refrigerator!**
