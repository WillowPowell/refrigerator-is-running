from flask import Blueprint, g, request, session
from werkzeug.security import check_password_hash

from refrigerator.models.users import User
from refrigerator.utils.response import validate_data, make_response

bp = Blueprint("sessions", __name__, url_prefix="/api/sessions")


@bp.route("", methods=("DELETE", "GET", "POST"))
def sessions():
	if request.method == "DELETE":
		return logout(request)

	if request.method == "POST":
		return login(request)

	return check_session(request)


@validate_data(required_fields=("email", "password"), email=str, password=str)
def login(request):
	status_code = 201
	success = True
	data = {}
	errors = []
	user = User.query.filter_by(email=request.json["email"]).first()
	if not user or not check_password_hash(user.password, request.json["password"]):
		status_code = 401
		success = False
		errors.append("Authentication Failed.")
		return make_response(status_code, success, data, errors)
	session.clear()
	session["user_id"] = user.id
	data = user.to_response_dict()
	return make_response(status_code, success, data, errors)


def logout(request):
	status_code = 201
	success = True
	data = {}
	errors = []
	if session:
		session.clear()
	else:
		status_code = 404
		success = False
		errors.append("Not Found.")
	return make_response(status_code, success, data, errors)


def check_session(request):
	status_code = 200
	success = True
	data = {}
	errors = []
	if session:
		data = User.query.get(session["user_id"]).to_response_dict()
	else:
		status_code = 404
		success = False
		errors.append("Not Found.")
	return make_response(status_code, success, data, errors)

@bp.before_app_request
def load_logged_in_user():
	user_id = session.get("user_id")
	if user_id:
		user = User.query.filter_by(id=user_id).first()
		g.user = {
			"id" : user.id,
			"name" : user.name,
			"email" : user.email,
			"created_at" : user.created_at
		}
