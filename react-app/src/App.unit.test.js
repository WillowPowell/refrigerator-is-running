/* global customRender */
/* global expectComponentMounted */
/* global mockComponents */
import React from 'react';
import { Redirect as MockRedirect } from 'react-router-dom';
import { screen, waitFor } from '@testing-library/dom';

import { checkSession, getFaves } from 'utils/api';

import {
  FridgeNav,
  HomePage,
} from 'components/know';
import {
  LoginForm,
  RegistrationForm,
} from 'components/know/user';
import {
  FaveWizard,
  ViewFave,
  ViewFavesList,
} from 'components/know/fave';
import { Spinner } from 'components/show';
import { NoFaves } from 'components/show/fave';

import { registered } from 'fixtures/user';
import { getFavesResults as favesData } from 'fixtures/fave';

import App from './App';

jest.mock('utils/api');
jest.mock('components/know');
jest.mock('components/know/user');
jest.mock('components/know/fave');
jest.mock('components/show');
jest.mock('components/show/fave');

beforeEach(() => {
  mockComponents({
    FaveWizard,
    FridgeNav,
    HomePage,
    LoginForm,
    NoFaves,
    MockRedirect,
    RegistrationForm,
    Spinner,
    ViewFave,
    ViewFavesList,
  });
});

it('renders without a user and calls checkSession', async () => {
  customRender(<App />, {
    authState: {},
    favesState: {},
  });
  expect(screen.getByTestId('FridgeNav')).toBeInTheDocument();
  expect(screen.getByTestId('Spinner')).toBeInTheDocument();
  expect(screen.queryByTestId('FaveWizard')).not.toBeInTheDocument();
  await waitFor(() => expect(checkSession).toHaveBeenCalledTimes(1));
});

it('renders without a user and with sessionChecked', async () => {
  customRender(<App />, {
    authState: { sessionChecked: true },
    favesState: {},
  });
  expect(screen.getByTestId('HomePage')).toBeInTheDocument();
});

it('renders with a user and calls getFaves', async () => {
  customRender(<App />, {
    authState: { user: registered.data },
    favesState: {},
  });
  expect(screen.getByTestId('FaveWizard')).toBeInTheDocument();
  await waitFor(() => expect(getFaves).toHaveBeenCalledTimes(1));
});

it('redirects login to home if user logged in', async () => {
  customRender(<App />, {
    authState: { user: registered.data },
    favesState: {},
    route: '/user/login',
  });
  expectComponentMounted({ MockRedirect }, { props: { to: '/' } });
});

it('redirects register to home if user logged in', async () => {
  customRender(<App />, {
    authState: { user: registered.data },
    favesState: {},
    route: '/user/register',
  });
  expectComponentMounted({ MockRedirect }, { props: { to: '/' } });
});

it('renders with no faves and favesState sessionChecked', async () => {
  customRender(<App />, {
    authState: {
      user: registered.data,
      sessionChecked: true,
    },
    favesState: { sessionChecked: true },
    route: '/my-faves',
  });
  await waitFor(() => expect(screen.getByTestId('NoFaves')).toBeInTheDocument());
});

it('renders with faves', async () => {
  customRender(<App />, {
    authState: {
      user: registered.data,
      sessionChecked: true,
    },
    favesState: { faves: favesData },
    route: '/my-faves',
  });
  expect(screen.getByTestId('ViewFavesList')).toBeInTheDocument();
});

it('renders ViewFave at fave routes', () => {
  customRender(<App />, {
    authState: {
      user: registered.data,
      sessionChecked: true,
    },
    favesState: { faves: favesData },
    route: '/fave/0',
  });
  expect(screen.getByTestId('ViewFave')).toBeInTheDocument();
});

it('renders RegistrationForm at correct route', () => {
  customRender(<App />, {
    authState: {},
    favesState: {},
    route: '/user/register',
  });
  expect(screen.getByTestId('RegistrationForm')).toBeInTheDocument();
});

it('renders LoginForm at correct route', () => {
  customRender(<App />, {
    authState: {},
    favesState: {},
    route: '/user/login',
  });
  expect(screen.getByTestId('LoginForm')).toBeInTheDocument();
});
