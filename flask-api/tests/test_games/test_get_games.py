import datetime

import pytest
import requests

from refrigerator.models.games import Game
from refrigerator.utils.time import check_datetime_format

from tests.utils import check_game, check_response, find_game

# Constants
QUERY = "Bayonetta"
LIMIT = 20
OFFSET = 0


# Tests
def test_success(client, bayonetta_search, BAYONETTA_DATA, BAYONETTA_CLIMAX_DATA):
	response = bayonetta_search(client, {
		"query": QUERY,
		"limit": LIMIT,
		"offset": OFFSET
	})
	assert len(response.json["data"]) <= LIMIT
	BAYONETTA = find_game(response.json["data"], BAYONETTA_DATA["igdb_id"])
	BAYONETTA_CLIMAX = find_game(response.json["data"], BAYONETTA_CLIMAX_DATA["igdb_id"])
	check_game(BAYONETTA, **BAYONETTA_DATA)
	check_game(BAYONETTA_CLIMAX, **BAYONETTA_CLIMAX_DATA)
	assert Game.query.get(BAYONETTA["id"])
	assert Game.query.get(BAYONETTA_CLIMAX["id"])


def test_no_results(client, mock_search):
	search = mock_search("GRIwajefhaiw3y8o23899p38()(*&(Yo87g3daow38S")
	response = search(client, {
		"query": "GRIwajefhaiw3y8o23899p38()(*&(Yo87g3daow38S",
		"limit": LIMIT,
		"offset": OFFSET
	})
	check_response(response, 404, success=False, error="No Results Found.")


def test_paging(client, bayonetta_search):
	response = bayonetta_search(client, {
		"query": QUERY,
		"limit": 4,
		"offset": 0
	})
	page_1 = [game["id"] for game in response.json["data"]]
	assert len(page_1) == 4

	response = bayonetta_search(client, {
		"query": QUERY,
		"limit": 4,
		"offset": 4
	})
	page_2 = [game["id"] for game in response.json["data"]]
	assert len(page_2) == 4

	response = bayonetta_search(client, {
		"query": QUERY,
		"limit": 4,
		"offset": 8
	})
	page_3 = [game["id"] for game in response.json["data"]]
	assert len(page_3) == 3
	for i in page_1:
		assert i not in page_2
		assert i not in page_3
	for i in page_2:
		assert i not in page_3

	response = bayonetta_search(client, {
		"query": QUERY,
		"limit": 4,
		"offset": 2
	})
	page_1_point_5 = [game["id"] for game in response.json["data"]]
	assert len(page_1_point_5) == 4
	assert page_1_point_5[0] in page_1
	assert page_1_point_5[1] in page_1
	assert page_1_point_5[2] in page_2
	assert page_1_point_5[3] in page_2


def test_missing_fields(client, mock_search):
	search = mock_search("")
	response = search(client, {
			"limit": LIMIT,
			"offset": OFFSET
		})
	check_response(response, 400, success=False, error="Required Field query Not Provided.")
	response = search(client, {
		"query": QUERY,
		"offset": OFFSET
	})
	check_response(response, 400, success=False, error="Required Field limit Not Provided.")
	response = search(client, {
		"query": QUERY,
		"limit": LIMIT
	})
	check_response(response, 400, success=False, error="Required Field offset Not Provided.")


def test_over_limit(client, mock_search):
	search = mock_search("")
	response = search(client, {
		"query": QUERY,
		"limit": 40,
		"offset": OFFSET
	})
	check_response(response, 400, success=False, error="limit Exceeds Maximum 30.")


def test_incorrect_data_types(client, mock_search):
	search = mock_search("")
	response = search(client, {
		"query": QUERY,
		"limit": "Thirty",
		"offset": OFFSET
	})
	check_response(response, 400, success=False, error="Incorrect format for limit. Expected <class 'int'>.")
	response = search(client, {
		"query": QUERY,
		"limit": LIMIT,
		"offset": {"page number": 25}
	})
	check_response(response, 400, success=False, error="Incorrect format for offset. Expected <class 'int'>.")


def test_updates(client, modified_smash_data, smash_bros_search,
		SMASH_64_DATA, SMASH_WII_U_DATA):
	"""
	Most fields in the two games supplied with the modified_smash_data fixture have been modified.
	In addition, the updated_at fields for those games have been modified to be far in the past.
	Calling a search which returns those games should update the fields.
	"""
	response = smash_bros_search(client, {
		"query": "Smash Bros",
		"limit": LIMIT,
		"offset": OFFSET
	})
	smash_64 = find_game(response.json["data"], 1626)
	smash_wii_u = find_game(response.json["data"], 9602)
	check_game(smash_64, updated=True, **SMASH_64_DATA)
	check_game(smash_wii_u, updated=True, **SMASH_WII_U_DATA)


def test_cache(client, smash_bros_search, updated_at_in_future,
		MODIFIED_SMASH_64_DATA):
	"""
	The games in the updated_at_in_future fixture have had the same modifications made to them as the modified_smash_data fixture.
	In addition, their updated_at fields have been modified to be four days in the future.
	Calling a search should not update these games, and they should retain their modified values.
	"""
	response = smash_bros_search(client, {
		"query": "Smash Bros",
		"limit": LIMIT,
		"offset": OFFSET
	})
	smash_64 = find_game(response.json["data"],1626)
	check_game(smash_64, MODIFIED_SMASH_64_DATA)


def test_igdb_fail(client, mock_search):
	search = mock_search("Error", search_results=([], 500))
	response = search(client, {
			"query": "Error",
			"limit": LIMIT,
			"offset": OFFSET
		})
	check_response(response, status_code=503, success=False, error="Unable To Connect To IGDB.")
