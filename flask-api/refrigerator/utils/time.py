import re
from datetime import datetime

DATE_FORMAT = '%b %d, %Y'
DATETIME_FORMAT = '%a, %d %b %Y %H:%M:%S UTC'


def check_date_format(date_string):
	'''
	Return True if date_string matches DATE_REGEX, False otherwise.
	DATE_REGEX is built to match DATE_FORMAT.
	It will need to change if DATE_FORMAT changes.
	'''
	DATE_REGEX = re.compile(
		"[A-Z][a-z][a-z] ([0-2][0-9]|3[01]), [0-2][0-9][0-9][0-9]")
	return DATE_REGEX.match(date_string)


def check_datetime_format(datetime_string):
	'''
	Return True if datetime_string matches DATETIME_REGEX, False otherwise.
	DATETIME_REGEX is built to match DATETIME_FORMAT.
	It will need to change if DATETIME_FORMAT changes.
	'''
	DATETIME_REGEX = re.compile(
		"[A-Z][a-z][a-z], ([0-2][0-9]|3[01]) [A-Z][a-z][a-z] [0-2][0-9][0-9][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9] UTC")
	return DATETIME_REGEX.match(datetime_string)
