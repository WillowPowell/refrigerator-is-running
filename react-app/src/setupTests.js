/** * Global helpers are declared at bottom of file. */
import '@testing-library/jest-dom';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';

import { AuthProvider } from 'context/auth';
import initialAuthState from 'context/auth/initialState';
import { FavesProvider } from 'context/faves';
import initialFavesState from 'context/faves/initialState';
import { FaveWizardProvider } from 'context/faveWizard';
import initialFaveWizardState from 'context/faveWizard/initialState';

window.alert = jest.fn();

const mockPushHistory = jest.fn();
const mockReplaceHistory = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  Redirect: jest.fn(),
  useParams: jest.fn(),
  useHistory: () => ({
    push: mockPushHistory,
    replace: mockReplaceHistory,
  }),
}));

/**
* Check a call to a mock componenet created with `mockComponents`.
* Default props will be accounted for!
*
* @param {obj} component: the component to check. Pass { Component } to give access to the name.
* @param {obj} props: The props that should have been passed.
* @param {arr} anyFuncProps: props that will be chekced against `expect.any(Function)`.
* @param {bool} hasChildren: if true, will check that an array was passed as children prop.
*/
function expectComponentMounted(component, {
  i = 0,
  props = {},
  anyFuncProps = [],
  hasChildren,
  ...notAllowed
} = {}) {
  if (Object.keys(notAllowed).length > 0) {
    throw new Error(
      'Passed extra parameters to expectComponentMounted. Did you mean to send them in props?',
    );
  }
  const [componentName, componentObj] = Object.entries(component)[0];
  expect(screen.queryAllByTestId(componentName)[i]).toBeInTheDocument();
  const expectedProps = componentObj.defaultProps
    ? {
      ...componentObj.defaultProps,
      ...props,
    }
    : props;
  anyFuncProps.forEach((funcProp) => {
    expectedProps[funcProp] = expect.any(Function);
  });
  if (hasChildren) {
    expectedProps.children = expect.any(Array);
  }
  expect(componentObj).toHaveBeenCalledWith(expectedProps, expect.anything());
}

/**
* Render a UI inside MemoryRouter and Providers.
*
* @param {node} ui - the ui
* @param {str} route - the route to start the MemoryRouter at.
*
* @params {obj} authState, faveWizardState:
* Pass an empty obj to set up the state with the initialState value.
* Keys passed into the obj will be injected into the state.
*/
function customRender(ui, {
  route = '/',
  authState = null,
  favesState = null,
  faveWizardState = null,
} = {}) {
  let preparedUI = ui;
  function wrap(Provider, testState, initialState) {
    if (testState !== null) {
      preparedUI = (
        <Provider testInitialState={{
          ...initialState,
          ...testState,
        }}
        >
          {preparedUI}
        </Provider>
      );
    }
  }
  wrap(FaveWizardProvider, faveWizardState, initialFaveWizardState);
  wrap(FavesProvider, favesState, initialFavesState);
  wrap(AuthProvider, authState, initialAuthState);
  return render(
    <React.StrictMode>
      <MemoryRouter initialEntries={[route]}>
        {preparedUI}
      </MemoryRouter>
    </React.StrictMode>,
  );
}

/* Call like mockComponents({ DeleteFaveButton, EditFaveModal }); */
function mockComponents(components) {
  Object.keys(components).forEach(
    (c) => components[c].mockImplementation(
      ({ children }) => (
        <div data-testid={c}>
          {children}
        </div>
      ),
    ),
  );
}

global.expectComponentMounted = expectComponentMounted;
global.customRender = customRender;
global.mockComponents = mockComponents;
global.mockPushHistory = mockPushHistory;
global.mockReplaceHistory = mockReplaceHistory;
