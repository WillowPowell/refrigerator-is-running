/* global customRender */
import React from 'react';
import { screen, waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { patchFave } from 'utils/api';
import { withCover as fave } from 'fixtures/fave';

import EditFaveModal from './EditFaveModal';

jest.mock('utils/api');

const index = 0;
const newHoursPlayed = 1001; // Number(String(fave.hours_played) + String(1))
const newPhrase = 'It is better than I even thought!';
const updatedFave = {
  ...fave,
  hoursPlayed: newHoursPlayed,
  phrase: newPhrase,
};

const setModalActive = jest.fn();
let dialog = null;
let phraseInput = null;
let hoursPlayedInput = null;
let submitButton = null;
beforeEach(() => {
  customRender((
    <EditFaveModal
      fave={fave}
      index={index}
      modalActive
      setModalActive={setModalActive}
    />
  ), {
    authState: {},
    favesState: {},
  });

  dialog = screen.queryByRole('dialog', { name: 'Edit Fave' });
  phraseInput = screen.queryByRole('textbox', { name: `Why do you love ${fave.game.name}?` });
  hoursPlayedInput = screen.queryByRole('spinbutton', { name: 'Hours Played' });
  submitButton = screen.getByRole('button', { name: 'Submit' });

  patchFave.mockImplementation((
    authFetch,
    favesDispatch,
    faveId,
    faveIndex,
    {
      hoursPlayed,
      phrase,
    },
  ) => {
    const response = { ok: false };
    const responseJSON = { errors: ['Unexpected Error.'] };
    if (typeof authFetch !== 'function') {
      return [response, responseJSON];
    }
    if (
      faveId === fave.id
      && hoursPlayed === newHoursPlayed
      && phrase === newPhrase
    ) {
      response.ok = true;
      responseJSON.data = updatedFave;
    }
    return [response, responseJSON];
  });
});

afterEach(() => {
  dialog = null;
  phraseInput = null;
  hoursPlayedInput = null;
  submitButton = null;
});

it('renders', () => {
  expect(dialog).toBeInTheDocument();
  expect(dialog).toHaveClass('is-active');
  expect(screen.getByRole('banner', { name: `Edit Fave: ${fave.game.name}` })).toBeInTheDocument();
  expect(phraseInput).toBeInTheDocument();
  expect(phraseInput.value).toBe(fave.phrase);
  expect(hoursPlayedInput).toBeInTheDocument();
  expect(hoursPlayedInput.value).toBe(String(fave.hours_played));
  expect(submitButton).toBeInTheDocument();
  expect(submitButton).toHaveAttribute('disabled');
});

it('handles successful completion', async () => {
  userEvent.clear(phraseInput);
  userEvent.type(phraseInput, newPhrase);
  userEvent.type(hoursPlayedInput, String(1));
  userEvent.click(submitButton);
  expect(patchFave).toHaveBeenCalledWith(
    expect.any(Function),
    expect.any(Function),
    fave.id,
    index,
    {
      hoursPlayed: newHoursPlayed,
      phrase: newPhrase,
    },
  );
  await waitFor(() => expect(setModalActive).toHaveBeenCalledWith(false));
});

it('handles fail response', async () => {
  userEvent.clear(phraseInput);
  userEvent.type(phraseInput, newPhrase);
  userEvent.type(hoursPlayedInput, String(1));
  userEvent.click(submitButton);
  expect(patchFave).toHaveBeenCalledWith(
    expect.any(Function),
    expect.any(Function),
    fave.id,
    index,
    {
      hoursPlayed: newHoursPlayed,
      phrase: newPhrase,
    },
  );
  await waitFor(() => expect(setModalActive).toHaveBeenCalledWith(false));
});

it('disables submission if you go back to the original value', async () => {
  userEvent.clear(phraseInput);
  userEvent.type(phraseInput, newPhrase);
  await waitFor(() => expect(submitButton).not.toHaveAttribute('disabled'));
  userEvent.clear(phraseInput);
  userEvent.type(phraseInput, fave.phrase);
  expect(submitButton).toHaveAttribute('disabled');
});

it('disables submission if you have empty fields', async () => {
  userEvent.clear(phraseInput);
  expect(submitButton).toHaveAttribute('disabled');
});
