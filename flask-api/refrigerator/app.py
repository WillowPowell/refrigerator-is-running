import os
import sys

import logging
from logging import StreamHandler
from logging.handlers import SMTPHandler, TimedRotatingFileHandler

from smtplib import SMTPServerDisconnected

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from werkzeug.exceptions import HTTPException

from config import Config

from refrigerator.models.handlers import TwitchAuth

db = SQLAlchemy()
migrate = Migrate()

twitch_auth = TwitchAuth()


def create_app(test_config=None):
	"""
	Create and configure the app.

	:param test_config: dict to configure test apps. Includes special keys:
		`TEST_INSTANCE_PATH`.
			Testing suites should use their own instance folder.
			If "TEST_INSTANCE_PATH" is not included,
			testing functionality that requires an instance folder will fail.
		`TEST_PROD_LOGGING`
			Set to `True` to test the production logging config.
			If not set, test apps will bypass setting up most of the log handlers.
	"""
	app = Flask('refrigerator', instance_relative_config=True)

	# Load config.
	if test_config is None:
		app.config.from_object(Config)
	else:
		app.config.from_mapping(test_config)
		app.instance_path = app.config.get("TEST_INSTANCE_PATH")

	# Dev logging.
	if app.debug and not app.config.get("TEST_PROD_LOGGING"):
		app.logger.debug("app.debug detected. Started in development mode.")

	# Prod logging
	elif not app.testing or app.config.get("TEST_PROD_LOGGING"):
		# StreamHandler
		stream_handler = StreamHandler()
		if app.config.get("INFO_LOG") == "STREAM":
			stream_handler.setLevel(logging.INFO)
		else:
			stream_handler.setLevel(logging.CRITICAL)
		stream_handler.setFormatter(logging.Formatter(app.config["LOG_FORMATTER"]))
		app.logger.addHandler(stream_handler)

		# TimedRotatingFileHandler
		if app.config.get("INFO_LOG") == "FILE":
			file_handler = TimedRotatingFileHandler(app.instance_path + "/refrigerator.log",
				when=app.config["LOG_ROTATION_UNIT"],
				interval=app.config["LOG_ROTATION_INTERVAL"],
				backupCount=app.config["LOG_ROTATION_BACKUPS"])
			file_handler.setLevel(logging.INFO)
			file_handler.setFormatter(logging.Formatter(app.config["LOG_FORMATTER"]))
			app.logger.addHandler(file_handler)

		# SMTPHandler.
		# Uses subclass FridgeSMTPHandler.
		if "MAIL_SERVER" in app.config:
			app.logger.info("Found mail server configuration. Creating SMTPHandler.")
			auth = None
			if app.config["MAIL_USERNAME"] or app.config["MAIL_PASSWORD"]:
				auth = (app.config["MAIL_USERNAME"], app.config["MAIL_PASSWORD"])
			secure = () if app.config["MAIL_USE_TLS"] else None
			mail_handler = FridgeSMTPHandler(app,
				mailhost=(app.config["MAIL_SERVER"], app.config["MAIL_PORT"]),
				fromaddr=app.config.get("MAIL_FROM_ADDRESS") or app.config["MAIL_USERNAME"],
				toaddrs=app.config["MAIL_ADMINS"],
				subject="Refrigerator Log",
				credentials=auth, secure=secure)
			mail_handler.setLevel(logging.ERROR)
			mail_handler.setFormatter(logging.Formatter(app.config["LOG_FORMATTER"]))
			app.logger.addHandler(mail_handler)
			app.logger.info(f"SMTPHandler set up for {app}. "
				f"Error messages will be sent to {app.config['MAIL_ADMINS']}.")

	app.logger.info(f"{app} logging configured. Log handlers: {app.logger.handlers}")

	# Ensure that the instance folder exists.
	try:
		os.makedirs(app.instance_path)
	except OSError:
		pass

	# Register globals.
	db.init_app(app)
	migrate.init_app(app, db, render_as_batch=True)
	twitch_auth.init_app(app)

	if app.debug:
		from flask_cors import CORS
		CORS(app, supports_credentials=True)

	# Register the blueprints
	from refrigerator.blueprints import faves, games, sessions, users, welcome
	app.register_blueprint(faves.bp)
	app.register_blueprint(games.bp)
	app.register_blueprint(sessions.bp)
	app.register_blueprint(users.bp)
	app.register_blueprint(welcome.bp)

	@app.errorhandler(HTTPException)
	def handle_exception(e):
		"""
		If an HTTP error is raised, return an error response.
		You can use flask.abort(status_code, error) or flask.abort(status_code, errors)
		where error is a string and errors is a list of errors.
		"""
		response = {
			'success' : False,
			'errors' : e.description if isinstance(e.description, list) else [e.description, ]
		}
		app.logger.error(f"Caught HTTPException {e}. Returning: {(response, e.code)}")
		return response, e.code

	app.logger.info("Finished building refrigerator!")
	return app


class FridgeSMTPHandler(SMTPHandler):
	"""
	Subclass of SMTPHandler that adds app debug logs regarding sending emails,
	and catches SMTPServerDisconnected exceptions that are sometimes raised when starting the app.
	"""
	def __init__(self, app, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.app = app

	def emit(self, record):
		"""Add debug logs around emitting the record."""
		self.app.logger.debug(f"{self} received {record.levelname} log from module '{record.module}'. "
			f"Sending log to {self.app.config['MAIL_ADMINS']} "
			f"Log message: {record.msg}")
		super().emit(record)
		self.app.logger.debug("Finished sending error logs by email.")

	def getSubject(self, record):
		"""Put the error level and module in the subject line."""
		return f"{self.subject}: {record.levelname} in {record.module}"

	def handleError(self, record):
		"""Log SMTPServerDisconnected exceptions instead of printing the stack trace."""
		t, v, tb = sys.exc_info()
		if t == SMTPServerDisconnected:
			self.app.logger.info("SMTPServerDisconnected exception raised when sending log email. "
				"Likely trying to send a duplicate email. "
				f"Error details from smtplib: {v} "
				"Log message it was trying to send: "
				f"{record.levelname} in {record.module}: {record.msg}")
			del t, v, tb
		else:
			super().handleError(record)
