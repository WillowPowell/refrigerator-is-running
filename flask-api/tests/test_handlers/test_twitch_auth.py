import datetime
import logging
import os
import pytest

import requests

from refrigerator.app import create_app
from refrigerator.models.handlers import get_twitch_auth


def test_init_app_success(monkeypatch, mock_twitch_auth_responses, test_config):
	"""
	There are two paths to successful config for `init_app`.

	1. A new access token can be generated.
	2. There can be an already-valid access token in the "twitch_access_token.txt" file in the instance folder.

	The default settings for mock_twitch_auth_responses return success responses.
	"""
	monkeypatch.setattr(requests, "request", mock_twitch_auth_responses())

	# Test path 1 first.
	app = create_app(test_config=test_config)
	with app.app_context():
		assert get_twitch_auth().access_token == "abcd123"

	# Write the file and test path 2.
	with open(os.path.join(test_config["TEST_INSTANCE_PATH"], "twitch_access_token.txt"), "w") as f:
		f.write("efgh456")
	app = create_app(test_config=test_config)
	with app.app_context():
		assert get_twitch_auth().access_token == "efgh456"


def test_init_app_bad_config(caplog, monkeypatch, mock_twitch_auth_responses, test_config):
	"""
	RuntimeError is raised if Twitch creds are not supplied in config.
	Error is logged if an access token cannot be generated during startup.
	"""
	# Test missing credentials.
	missing_client_id = test_config.copy()
	missing_client_id.pop("TWITCH_CLIENT_ID")
	with pytest.raises(RuntimeError):
		app = create_app(test_config=missing_client_id)

	missing_client_secret = test_config.copy()
	missing_client_secret.pop("TWITCH_CLIENT_SECRET")
	with pytest.raises(RuntimeError):
		app = create_app(test_config=missing_client_secret)

	# Test failure to obtain token.
	monkeypatch.setattr(requests, "request", mock_twitch_auth_responses(update_response=({}, 401)))
	with caplog.at_level(logging.ERROR):
		create_app(test_config=test_config)
	assert "Unable to obtain Twitch Access Token at startup." in caplog.text


def test_bad_access_token(monkeypatch, mock_twitch_auth_responses, test_config):
	"""Mock having a bad access token saved in instance folder at startup."""
	# Setting the update response to give access token "abcd123"
	# and valid response to a 401 error.
	monkeypatch.setattr(requests, "request", mock_twitch_auth_responses(
		update_response=({"access_token":"abcd123"}, 200),
		valid_response=({}, 401)))
	# Write a different token to the file.
	with open(os.path.join(test_config["TEST_INSTANCE_PATH"], "twitch_access_token.txt"), "w") as f:
		f.write("efgh456")
	# Creating the app should check the access token in the file,
	# See that it is not valid, and update it.
	app = create_app(test_config=test_config)
	with app.app_context():
		assert get_twitch_auth().access_token == "abcd123"


def test_cache_access_token_validation(monkeypatch, mock_twitch_auth_responses, test_config):
	"""Access token validation should be cached for 30 seconds."""
	# Setting the update response to give access token "abcd123"
	# and valid response to a 401 error.
	monkeypatch.setattr(requests, "request", mock_twitch_auth_responses(
		update_response=({"access_token":"abcd123"}, 200),
		valid_response=({}, 401)))
	app = create_app(test_config=test_config)
	# Set the access token to a different value,
	# and make token_validated_at in the future.
	with app.app_context():
		twitch_auth = get_twitch_auth()
		twitch_auth.access_token = "efgh456"
		twitch_auth.token_validated_at = datetime.datetime.now() + datetime.timedelta(days=4)
		# Calling valid_access_token should return True,
		# even though the call to "https://id.twitch.tv/oauth2/validate" would return a `401`.
		if not twitch_auth.valid_access_token():
			twitch_auth.update_access_token()
		# Token should not be updated.
		assert get_twitch_auth().access_token == "efgh456"
