import fetchMock from 'fetch-mock-jest';

import { unregistered, responses } from 'fixtures/user';

import register from './register';

class RegisterMatcher {
  constructor(body) {
    this.url = '/api/users';
    this.method = 'POST';
    this.headers = { 'Content-Type': 'application/json' };
    this.body = body;
  }
}

afterEach(() => {
  fetchMock.restore();
});

it('sends the form data to the api and returns the response', async () => {
  const registerMatcher = new RegisterMatcher({
    name: unregistered.name,
    email: unregistered.email,
    password: unregistered.password,
  });
  // Arrange
  fetchMock.mock(registerMatcher, responses.registerSuccess);
  expect.assertions(2);
  // Act
  const [response, responseJSON] = await register(
    unregistered.name, unregistered.email, unregistered.password,
  );
  // Assert
  expect(response.ok).toBe(true);
  expect(responseJSON.data).toEqual(responses.registerSuccess.body.data);
});
