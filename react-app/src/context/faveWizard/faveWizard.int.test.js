import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { act, renderHook } from '@testing-library/react-hooks';

import { AuthProvider } from 'context/auth';

import { FaveWizardProvider, useFaveWizardContext } from './context';

import initialState from './initialState';

const wrapper = ({ testInitialState = null, children } = {}) => (
  <MemoryRouter>
    <AuthProvider>
      <FaveWizardProvider testInitialState={testInitialState}>
        {children}
      </FaveWizardProvider>
    </AuthProvider>
  </MemoryRouter>
);

const testState = {
  ...initialState,
  lastSearch: {
    query: 'Bayonetta',
    data: { some: 'data' },
  },
  step: 2,
  modalActive: true,
};

it('renders with initial state', () => {
  const { result } = renderHook(() => useFaveWizardContext(), { wrapper });
  expect(result.current.faveWizardState).toEqual(initialState);
});

it('sets top-level values', () => {
  const { result } = renderHook(() => useFaveWizardContext(), { wrapper });
  act(() => {
    result.current.faveWizardDispatch({
      type: 'SET',
      payload: {
        modalActive: testState.modalActive,
        step: testState.step,
      },
    });
  });
  expect(result.current.faveWizardState.modalActive).toEqual(testState.modalActive);
  expect(result.current.faveWizardState.step).toEqual(testState.step);
});

it('sets nested values', () => {
  const { result } = renderHook(() => useFaveWizardContext(), { wrapper });
  act(() => {
    result.current.faveWizardDispatch({
      type: 'SET_NESTED',
      payload: { lastSearch: testState.lastSearch },
    });
  });
  expect(result.current.faveWizardState.lastSearch.query).toEqual(testState.lastSearch.query);
  expect(result.current.faveWizardState.lastSearch.data).toEqual(testState.lastSearch.data);
});

it('resets targeted values', () => {
  const { result } = renderHook(() => useFaveWizardContext(), {
    wrapper,
    initialProps: { testInitialState: testState },
  });
  act(() => {
    result.current.faveWizardDispatch({
      type: 'RESET',
      payload: ['lastSearch', 'step'],
    });
  });
  expect(result.current.faveWizardState).toEqual({
    ...initialState,
    ...testState,
    lastSearch: initialState.lastSearch,
    step: initialState.step,
  });
  expect(result.current.faveWizardState.lastSearch).toEqual(initialState.lastSearch);
  expect(result.current.faveWizardState.step).toEqual(initialState.step);
  expect(result.current.faveWizardState.modalActive).toEqual(testState.modalActive);
});

it('resets all', () => {
  const { result } = renderHook(() => useFaveWizardContext(), {
    wrapper,
    initialProps: { testInitialState: testState },
  });
  act(() => {
    result.current.faveWizardDispatch({ type: 'RESET_ALL' });
  });
  expect(result.current.faveWizardState).toEqual(initialState);
});
