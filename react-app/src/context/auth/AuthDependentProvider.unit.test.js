/* global customRender */
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { act } from 'react-dom/test-utils';
import { screen, waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { useAuthContext } from 'context/auth';
import { registered, unregistered as registered2 } from 'fixtures/user';

import AuthDependentProvider from './AuthDependentProvider';

const MockContext = React.createContext();
const mockContextName = 'mock';

const mockInitialState = {
  userId: null,
  someKey: 'foo',
};

function mockReducer(currentState, action) {
  switch (action.type) {
    case 'SET_SOME_KEY': {
      return {
        ...currentState,
        someKey: action.payload,
      };
    }
    case 'BAD_ACTION_REMOVES_USER_ID': {
      return {
        somekey: 'foo',
      };
    }
    default: {
      throw new Error('Unrecognized action type.');
    }
  }
}

function MockProvider({ cache, children }) {
  return (
    <AuthDependentProvider
      cache={cache}
      Context={MockContext}
      contextName={mockContextName}
      initialState={mockInitialState}
      reducer={mockReducer}
    >
      {children}
    </AuthDependentProvider>
  );
}

MockProvider.propTypes = {
  cache: PropTypes.bool,
  children: PropTypes.shape().isRequired,
};

MockProvider.defaultProps = {
  cache: undefined,
};

function useMockContext() {
  return useContext(MockContext);
}

function MockChild() {
  const { mockState, mockDispatch } = useMockContext();
  const { authDispatch, authState } = useAuthContext();
  return (
    <>
      <div data-testid="mockState stringified">{JSON.stringify(mockState)}</div>
      <div data-testid="authState stringified">{JSON.stringify(authState)}</div>
      <button
        type="button"
        aria-label="Set someKey to bar"
        onClick={() => {
          mockDispatch({
            type: 'SET_SOME_KEY',
            payload: 'bar',
          });
        }}
      />
      <button
        type="button"
        aria-label="Remove userId"
        onClick={() => {
          mockDispatch({
            type: 'BAD_ACTION_REMOVES_USER_ID',
          });
        }}
      />
      <button
        type="button"
        aria-label="Set user registered2.data"
        onClick={() => {
          authDispatch({
            type: 'SET_USER',
            payload: registered2.data,
          });
        }}
      />
      <button
        type="button"
        aria-label="CLEAR_USER"
        onClick={() => {
          authDispatch({
            type: 'CLEAR_USER',
          });
        }}
      />
      <button
        type="button"
        aria-label="Set user registered.data"
        onClick={() => {
          authDispatch({
            type: 'SET_USER',
            payload: registered.data,
          });
        }}
      />
    </>
  );
}

function setSomeKeyToBar() {
  act(() => {
    userEvent.click(screen.getByRole('button', {
      name: 'Set someKey to bar',
    }));
  });
}

async function removeUserId() {
  await act(async () => {
    userEvent.click(screen.getByRole('button', {
      name: 'Remove userId',
    }));
  });
}

function testCache() {
  setSomeKeyToBar();
  return act(async () => {
    const currentAuthState = JSON.parse(screen.getByTestId('authState stringified').innerHTML);
    userEvent.click(screen.getByRole('button', {
      name: 'CLEAR_USER',
    }));
    await waitFor(() => expect(screen.getByTestId('authState stringified').innerHTML).toEqual(JSON.stringify({
      ...currentAuthState,
      user: null,
    })));
    userEvent.click(screen.getByRole('button', {
      name: 'Set user registered.data',
    }));
  });
}

it('renders with initialState', () => {
  customRender((
    <MockProvider>
      <MockChild />
    </MockProvider>
  ), {
    authState: {},
  });
  expect(screen.getByTestId('mockState stringified').innerHTML)
    .toEqual(JSON.stringify(mockInitialState));
});

it('renders with a user', async () => {
  customRender((
    <MockProvider>
      <MockChild />
    </MockProvider>
  ), {
    authState: { user: registered.data },
  });
  await waitFor(() => {
    expect(screen.getByTestId('mockState stringified').innerHTML).toEqual(JSON.stringify({
      ...mockInitialState,
      userId: registered.data.id,
    }));
  });
});

it('dispatches to the reducer', () => {
  customRender((
    <MockProvider>
      <MockChild />
    </MockProvider>
  ), {
    authState: {},
  });
  setSomeKeyToBar();
  expect(screen.getByTestId('mockState stringified').innerHTML).toEqual(JSON.stringify({
    ...mockInitialState,
    someKey: 'bar',
  }));
});

it('resets when the user changes', async () => {
  customRender((
    <MockProvider>
      <MockChild />
    </MockProvider>
  ), {
    authState: { user: registered.data },
  });
  setSomeKeyToBar();
  act(() => {
    userEvent.click(screen.getByRole('button', {
      name: 'Set user registered2.data',
    }));
  });
  await waitFor(() => expect(screen.getByTestId('mockState stringified').innerHTML).toEqual(JSON.stringify({
    ...mockInitialState,
    userId: registered2.data.id,
  })));
});

it('caches if specified', async () => {
  customRender((
    <MockProvider cache>
      <MockChild />
    </MockProvider>
  ), {
    authState: { user: registered.data },
  });
  await testCache();
  await waitFor(() => expect(screen.getByTestId('mockState stringified').innerHTML).toEqual(JSON.stringify({
    ...mockInitialState,
    userId: registered.data.id,
    someKey: 'bar',
  })));
});

it('does not cache if not specified', async () => {
  customRender((
    <MockProvider>
      <MockChild />
    </MockProvider>
  ), {
    authState: { user: registered.data },
  });
  await testCache();
  await waitFor(() => expect(screen.getByTestId('mockState stringified').innerHTML).toEqual(JSON.stringify({
    ...mockInitialState,
    userId: registered.data.id,
  })));
});

it('throws an error if you remove or change the userId', async () => {
  customRender((
    <MockProvider>
      <MockChild />
    </MockProvider>
  ), {
    authState: {},
  });
  expect.assertions(1);
  jest.spyOn(console, 'error');
  // eslint-disable-next-line no-console
  console.error.mockImplementation(() => {});
  await expect(removeUserId()).rejects.toThrow();
  // eslint-disable-next-line no-console
  console.error.mockRestore();
});
