import fetchMock from 'fetch-mock-jest';

import { searchResults } from 'fixtures/game';
import searchGames from './searchGames';

class SearchGamesMocker {
  constructor(query, limit, offset, dataset) {
    this.matcher = {
      url: `/api/games?query=${query}&limit=${limit}&offset=${offset}`,
      method: 'GET',
    };
    this.successResponse = {
      body: {
        data: dataset.slice(offset, offset + limit),
        success: true,
      },
      status: 200,
    };
  }
}

// Teardown
afterEach(() => fetchMock.restore());

it('performs an API call and returns the data', async () => {
  // Arrange
  const searchGamesMocker = new SearchGamesMocker('Bayonetta', 10, 10, searchResults);
  fetchMock.mock(searchGamesMocker.matcher, searchGamesMocker.successResponse);
  expect.assertions(2);
  // Act
  const [response, responseJSON] = await searchGames('Bayonetta', 10, 10);
  // Assert
  expect(response.ok).toBe(true);
  expect(responseJSON.data).toEqual(searchResults.slice(10, 20));
});
