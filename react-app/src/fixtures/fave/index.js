import {
  customGame as myCustomGame,
  noCover as gameWithNoCover,
  withCover as gameWithCover,
} from 'fixtures/game';
import { registered as user } from 'fixtures/user';

import lots from './lotsOfFaves';

export const lotsOfFaves = lots;

export const withCover = {
  id: 1,
  user_id: user.data.id,
  game_id: gameWithCover.id,
  game: gameWithCover,
  top_fave: false,
  phrase: 'I always wanted to be a member of a witch clan.',
  hours_played: 100,
  deleted: false,
  created_at: 'Jan 21, 2021 15:34:23 UTC',
  updated_at: null,
  deleted_at: null,
};

export const noCover = {
  id: 2,
  user_id: user.data.id,
  game_id: gameWithNoCover.id,
  game: gameWithNoCover,
  top_fave: false,
  phrase: 'Hot gossip!',
  hours_played: 100,
  deleted: false,
  created_at: 'Jan 21, 2021 15:34:23 UTC',
  updated_at: null,
  deleted_at: null,
};

export const customGame = {
  created_at: 'Fri, 09 Apr 2021 12:39:00 UTC',
  deleted: false,
  deleted_at: null,
  game: myCustomGame,
  game_id: myCustomGame.id,
  hours_played: 1000,
  id: 3,
  phrase: 'I like how the cat is named after the character from the book.',
  top_fave: false,
  updated_at: null,
  user_id: user.data.id,
};

export const getFavesResults = [withCover, noCover, customGame];
