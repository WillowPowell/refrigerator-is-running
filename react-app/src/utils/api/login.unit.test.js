import fetchMock from 'fetch-mock-jest';

import { registered } from 'fixtures/user';
import { responses } from 'fixtures/session';

import login from './login';

class LoginMatcher {
  constructor(body) {
    this.url = '/api/sessions';
    this.method = 'POST';
    this.headers = { 'Content-Type': 'application/json' };
    this.body = body;
  }
}

const dispatch = jest.fn();

afterEach(() => fetchMock.restore());

it('performs a successful API call and calls SET_USER dispatch action', async () => {
  const loginSuccessMatcher = new LoginMatcher({
    email: registered.email,
    password: registered.password,
  });
  fetchMock.mock(loginSuccessMatcher, responses.loginSuccess);
  expect.assertions(3);
  // Act
  const [response, responseJSON] = await login(dispatch, registered.email, registered.password);
  // Assert
  expect(dispatch).toHaveBeenCalledWith({
    type: 'SET_USER',
    payload: registered.data,
  });
  expect(response.ok).toBe(true);
  expect(responseJSON.data).toEqual(registered.data);
});

it('performs a failed API call and does not call SET_USER dispatch action', async () => {
  const loginFailMatcher = new LoginMatcher({
    email: registered.email,
    password: 'notmypassword',
  });
  fetchMock.mock(loginFailMatcher, responses.loginFail);
  expect.assertions(3);
  // Act
  const [response, responseJSON] = await login(dispatch, registered.email, 'notmypassword');
  // Assert
  expect(response.ok).toBe(false);
  expect(responseJSON.errors).toEqual(['Authentication Failed.']);
  expect(dispatch).not.toHaveBeenCalled();
});
