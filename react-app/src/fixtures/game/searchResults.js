import withCover from './withCover';
import noCover from './noCover';

const searchResults = [
  withCover,
  {
    cover_height: 1053,
    cover_url: 'https://images.igdb.com/igdb/image/upload/t_thumb/qocwccspgmgamvhaplgp.jpg',
    cover_url_big: 'https://images.igdb.com/igdb/image/upload/t_cover_big/qocwccspgmgamvhaplgp.jpg',
    cover_width: 800,
    created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
    first_release_date: 'Sep 20, 2014',
    id: 103,
    igdb_id: 2135,
    igdb_url: 'https://www.igdb.com/games/bayonetta-2',
    name: 'Bayonetta 2',
    summary: 'The witching hour strikes again. Brimming with intricate battles that take place in, on and all over epic set pieces, Bayonetta 2 finds our sassy heroine battling angels and demons in unearthly beautiful HD. You\u2019re bound to love how it feels to string together combos with unimaginable weapons and to summon demons using Bayonetta\u2019s Umbran Weave in this frantic stylized action game.',
    twitch_id: 247057,
    updated_at: null,
  },
  {
    cover_height: 1080,
    cover_url: 'https://images.igdb.com/igdb/image/upload/t_thumb/lytcfmcdamxitl44bltu.jpg',
    cover_url_big: 'https://images.igdb.com/igdb/image/upload/t_cover_big/lytcfmcdamxitl44bltu.jpg',
    cover_width: 756,
    created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
    first_release_date: null,
    id: 110,
    igdb_id: 76888,
    igdb_url: 'https://www.igdb.com/games/bayonetta-3',
    name: 'Bayonetta 3',
    summary: 'Bayonetta is back. Bayonetta 3 is currently in development exclusively for Nintendo Switch.',
    twitch_id: 1907063,
    updated_at: null,
  },
  {
    cover_height: 800,
    cover_url: 'https://images.igdb.com/igdb/image/upload/t_thumb/co22jk.jpg',
    cover_url_big: 'https://images.igdb.com/igdb/image/upload/t_cover_big/co22jk.jpg',
    cover_width: 600,
    created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
    first_release_date: 'Feb 02, 2015',
    id: 105,
    igdb_id: 28058,
    igdb_url: 'https://www.igdb.com/games/8-bit-bayonetta',
    name: '8-Bit Bayonetta',
    summary: "\"SEGA\u2019s favourite Umbran Witch climaxes her way onto PC! Thrill to her signature beehive do in 8-bit pixelated glory! Jump! Shoot! Score! Being bad never felt so retro. \" \n \nOriginally used as a mini-game on the 404 page of Platinum Games Japanese website in 2015. \n  \nSEGA's take on 2017 April fool joke. Ultimately the game was released on Steam to share an announcement website that has a countdown clock ending on April 11th.",
    twitch_id: 1926087,
    updated_at: null,
  },
  noCover,
  {
    cover_height: null,
    cover_url: null,
    cover_url_big: null,
    cover_width: null,
    created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
    first_release_date: 'Nov 24, 2014',
    id: 107,
    igdb_id: 51154,
    igdb_url: 'https://www.igdb.com/games/bayonetta-2-bonus-edition',
    name: 'Bayonetta 2 - Bonus Edition',
    summary: 'Bayonetta 2 (\u30d9\u30e8\u30cd\u30c3\u30bf 2 Beyonetta Ts\u016b?) is an action hack and slash video game developed by Platinum Games and published by Nintendo for the Wii U, with Sega as the franchise owners serving as its advisor. It is the direct sequel to the 2009 game, Bayonetta, and was directed by Yusuke Hashimoto and produced by Atsushi Inaba, under supervision by series creator Hideki Kamiya. It was announced on September 13, 2012, and will be exclusive to the Wii U, unlike the previous game which was only available on the PlayStation 3 and Xbox 360. The titular character, Bayonetta, sports a new costume and hairstyle and the game itself features a new two-player mode. The game is also the second Bayonetta product to receive Japanese voiceovers, using the same cast that voiced the Bayonetta: Bloody Fate anime film by Gonzo. The game was released in September 2014 and includes a port of the original Bayonetta as a separate disc inside the case. \n \nAlso Available \nBayonetta 2: Bonus Edition \nBayonetta 2: Special Edition \n \nBayonetta 2: First Print Edition \nDisc versions of both Bayonetta and Bayonetta 2, an art book, exclusive embossed packaging.',
    twitch_id: null,
    updated_at: null,
  },
  {
    cover_height: null,
    cover_url: null,
    cover_url_big: null,
    cover_width: null,
    created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
    first_release_date: null,
    id: 112,
    igdb_id: 136077,
    igdb_url: 'https://www.igdb.com/games/bayonetta-special-edition',
    name: 'Bayonetta: Special Edition',
    summary: null,
    twitch_id: null,
    updated_at: null,
  },
  {
    cover_height: null,
    cover_url: null,
    cover_url_big: null,
    cover_width: null,
    created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
    first_release_date: null,
    id: 106,
    igdb_id: 51150,
    igdb_url: 'https://www.igdb.com/games/bayonetta-2-special-edition',
    name: 'Bayonetta 2: Special Edition',
    summary: null,
    twitch_id: null,
    updated_at: null,
  },
  {
    cover_height: null,
    cover_url: null,
    cover_url_big: null,
    cover_width: null,
    created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
    first_release_date: null,
    id: 108,
    igdb_id: 51187,
    igdb_url: 'https://www.igdb.com/games/bayonetta-2-first-print-edition',
    name: 'Bayonetta 2: First Print Edition',
    summary: null,
    twitch_id: null,
    updated_at: null,
  },
  {
    cover_height: null,
    cover_url: null,
    cover_url_big: null,
    cover_width: null,
    created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
    first_release_date: null,
    id: 113,
    igdb_id: 136898,
    igdb_url: 'https://www.igdb.com/games/bayonetta-nonstop-climax-edition',
    name: 'Bayonetta: Nonstop Climax Edition',
    summary: null,
    twitch_id: 1992080,
    updated_at: null,
  },
  {
    cover_height: 800,
    cover_url: 'https://images.igdb.com/igdb/image/upload/t_thumb/co1zi6.jpg',
    cover_url_big: 'https://images.igdb.com/igdb/image/upload/t_cover_big/co1zi6.jpg',
    cover_width: 600,
    created_at: 'Thu, 01 Apr 2021 12:55:18 UTC',
    first_release_date: 'Feb 18, 2020',
    id: 111,
    igdb_id: 127258,
    igdb_url: 'https://www.igdb.com/games/bayonetta-and-vanquish-10th-anniversary-bundle',
    name: 'Bayonetta & Vanquish 10th Anniversary Bundle',
    summary: 'Experience the genesis of the Bayonetta series with the original action-adventure. Take advantage of Bayonetta\u2019s arsenal of skills to hack, slash, and open fire upon hordes of celestial foes. \n \nPlay as space soldier Sam Gideon in the hit sci-fi shooter, Vanquish. Equipped with BLADE, the experimental weapon system that can scan and copy existing weapons, he must infiltrate conquered space colony Providence and defeat legions of future-tech enemies.',
    twitch_id: null,
    updated_at: null,
  },
];

export default searchResults;
