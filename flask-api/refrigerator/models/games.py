import datetime

from refrigerator.app import db
from refrigerator.models.handlers import get_igdb
from refrigerator.utils.time import DATE_FORMAT, DATETIME_FORMAT


class Game(db.Model):
	__tablename__ = 'games'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String, nullable=False)
	first_release_date = db.Column(db.Date)
	cover_url = db.Column(db.String)
	cover_width = db.Column(db.Integer)
	cover_height = db.Column(db.Integer)
	summary = db.Column(db.String)
	igdb_id = db.Column(db.Integer, unique=True)
	igdb_url = db.Column(db.String, unique=True)
	twitch_id = db.Column(db.Integer, unique=True)
	created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now())
	updated_at = db.Column(db.DateTime, onupdate=datetime.datetime.now())

	faves = db.relationship("Fave", back_populates="game")

	def __repr__(self):
		return '<Game %r>' %self.name

	def to_response_dict(self):
		"""
		Convert Game object to a dict formatted for a JSON response.
		"""
		return {
			'id': self.id,
			'name' : self.name,
			'first_release_date' : self.first_release_date.strftime(DATE_FORMAT) if self.first_release_date else None,
			'cover_url' : self.cover_url,
			'cover_url_big' : self.cover_url.replace('t_thumb', 't_cover_big') if self.cover_url else None,
			'cover_width' : self.cover_width,
			'cover_height' : self.cover_height,
			'summary' : self.summary,
			'igdb_id' : self.igdb_id,
			'igdb_url' : self.igdb_url,
			'twitch_id' : self.twitch_id,
			'created_at': self.created_at.strftime(DATETIME_FORMAT),
			'updated_at': self.updated_at.strftime(DATETIME_FORMAT) if self.updated_at else None
		}

	def search(query, limit, offset):
		search_results = get_igdb().call('/games',fields="id,updated_at",search=query,limit=limit,offset=offset)
		if not search_results:
			return None
		Game.__update_igdb_cache(search_results)
		data = [Game.query.filter_by(igdb_id=result['id']).first().to_response_dict() for result in search_results]
		return data

	def __get_igdb_games(game_ids_list):
		game_ids = ",".join(str(id) for id in game_ids_list)
		igdb = get_igdb()
		games = igdb.call(
			'/games',
			fields="name,first_release_date,summary,id,url",
			where=f"id = ({game_ids})")
		covers = igdb.call(
			'/covers',
			fields="game,url,height,width",
			where=f"game = ({game_ids})")
		external_games = igdb.call(
			'/external_games',
			fields="game,id",
			where=f"game = ({game_ids}) & category = 14")
		return Game.igdb_to_fridge(games, covers, external_games)

	def igdb_to_fridge(games, covers, external_games):
		"""
		Prepare IGDB data for Refrigeration.
		"""
		data = []
		for game in games:
			if "id" in game:
				fridge_game = {}
				fridge_game['name'] = game.get('name', f"<Unnamed Game {game['id']} From IGDB>")
				fridge_game['summary'] = game.get('summary')
				fridge_game['igdb_id'] = game.get('id')
				fridge_game['igdb_url'] = game.get('url')
				if 'first_release_date' in game:
					fridge_game['first_release_date'] = datetime.datetime.utcfromtimestamp(game.get('first_release_date')).date()
				for cover in covers:
					if 'game' in cover:
						if game['id'] == cover['game']:
							if 'url' in cover:
								fridge_game['cover_url'] = f"https:{cover['url']}"
							fridge_game['cover_width'] = cover.get('width')
							fridge_game['cover_height'] = cover.get('height')
							break
				for external_game in external_games:
					if 'game' in external_game:
						if game['id'] == external_game['game']:
							fridge_game['twitch_id'] = external_game.get('id')
							break
				data.append(fridge_game)
		return data

	def __update_igdb_cache(search_results):
		games_needed = []
		updates_needed = []
		for result in search_results:
			if 'id' in result:
				row = Game.query.filter_by(igdb_id=result['id']).first()
				#if no game in local database, it must be fetched
				if not row:
					games_needed.append(result['id'])
				#this game is in the database, but it has been updated on IGDB, it must be fetched
				elif row.updated_at and row.updated_at < datetime.datetime.utcfromtimestamp(result.get('updated_at',0)):
					updates_needed.append(result['id'])
		if games_needed:
			new_games = Game.__get_igdb_games(games_needed)
			for new_game in new_games:
				db.session.add(Game(**new_game))
		if updates_needed:
			game_updates = Game.__get_igdb_games(updates_needed)
			for game_update in game_updates:
				row = Game.query.filter_by(igdb_id=game_update['igdb_id']).first()
				for k, v in game_update.items():
					setattr(row, k, v)
				db.session.add(row)
		db.session.commit()

	def make_game(request_json):
		"""Create and return a new game populated with request_json."""
		game = Game(
			name=request_json['name'],
			cover_url=request_json.get('cover_url'),
			cover_width=request_json.get('cover_width'),
			cover_height=request_json.get('cover_height'),
			summary=request_json.get('summary'),
			igdb_id=request_json.get('igdb_id'),
			igdb_url=request_json.get('igdb_url'),
			twitch_id=request_json.get('twitch_id'))

		if "first_release_date" in request_json:
			game.first_release_date = datetime.datetime.strptime(request_json['first_release_date'], DATE_FORMAT).date()
		db.session.add(game)
		db.session.commit()
		return game
