import datetime
import os
import pytest
import tempfile
import shutil

from werkzeug.security import generate_password_hash

from refrigerator.app import create_app, db

from refrigerator.models.faves import Fave
from refrigerator.models.games import Game
from refrigerator.models.handlers import TwitchAuth
from refrigerator.models.users import User


@pytest.fixture
def test_config():
	"""Define the default test config."""
	test_instance_path = tempfile.mkdtemp()
	db_fd, db_path = tempfile.mkstemp(dir=test_instance_path)

	yield {
		"TESTING": True,
		"TEST_INSTANCE_PATH": test_instance_path,
		"SECRET_KEY": "TESTING",
		"SQLALCHEMY_DATABASE_URI": f"sqlite:///{db_path}",
		"SQLALCHEMY_TRACK_MODIFICATIONS": False,
		"TWITCH_CLIENT_ID": os.getenv("TWITCH_CLIENT_ID"),
		"TWITCH_CLIENT_SECRET": os.getenv("TWITCH_CLIENT_SECRET"),
		"INFO_LOG": "FILE",
		"LOG_FORMATTER": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
		"LOG_ROTATION_UNIT": "d",
		"LOG_ROTATION_INTERVAL": 1,
		"LOG_ROTATION_BACKUPS": 5,
		"MAIL_SERVER": "testing",
		"MAIL_PORT": 25,
		"MAIL_USE_TLS": True,
		"MAIL_USERNAME": "sender@example.com",
		"MAIL_PASSWORD": "12345",
		"MAIL_ADMINS": ["admin_1@example.com", "admin_2@example.com"]
	}

	os.close(db_fd)
	shutil.rmtree(test_instance_path)


@pytest.fixture
def safe_create_app(monkeypatch):
	"""
	This fixture provides a "safe" version of `create_app`,
	bypassing `TwithAuth.init_app` to ensure tests do not hit external APIs unless desired.
	"""
	def create_safe(*args, **kwargs):
		monkeypatch.setattr(TwitchAuth, "init_app", lambda *args, **kwargs: None)
		return create_app(*args, **kwargs)
	return create_safe


@pytest.fixture
def app(safe_create_app, test_config):
	"""
	Create a new app instance for each test.
	This uses safe_create_app which bypasses TwitchAuth
	to avoid unnecessary traffic.

	Use the twitch_enabled_app and twitch_enabled_client fixtures
	to make real API calls.
	"""

	#Set up the app.
	app = safe_create_app(test_config=test_config)

	with app.app_context():
		db.create_all()

		yield app

		db.session.remove()
		db.drop_all()


@pytest.fixture
def client(app):
	"""Test client for app fixture."""
	return app.test_client()


@pytest.fixture
def REGISTERED_EMAIL():
 return "galactic_captain@example.com"


@pytest.fixture
def REGISTERED_PASSWORD():
 return "bx08^O$/xa1Pxac@bx18xd5fXecxc3~"


@pytest.fixture
def REGISTERED_NAME():
 return "Rosalina"


@pytest.fixture
def registered_user(app, REGISTERED_EMAIL, REGISTERED_PASSWORD, REGISTERED_NAME):
	user = User(
		email=REGISTERED_EMAIL,
		password=generate_password_hash(REGISTERED_PASSWORD),
		name=REGISTERED_NAME)
	db.session.add(user)
	db.session.commit()
	return user


@pytest.fixture
def logged_in_client(client, registered_user, REGISTERED_EMAIL, REGISTERED_PASSWORD):
	with client:
		client.post("/api/sessions", json={
				"email": REGISTERED_EMAIL,
				"password": REGISTERED_PASSWORD
			})
		return client


@pytest.fixture
def REGISTERED_EMAIL_2():
 return "willow@example.com"


@pytest.fixture
def REGISTERED_PASSWORD_2():
 return "bx08^O$/xa1Pxac@Bx18xd5fXecxc2~"


@pytest.fixture
def REGISTERED_NAME_2():
 return "Willow"


@pytest.fixture
def registered_user_2(app, REGISTERED_EMAIL_2, REGISTERED_PASSWORD_2, REGISTERED_NAME_2):
	user = User(
		email=REGISTERED_EMAIL_2,
		password=generate_password_hash(REGISTERED_PASSWORD_2),
		name=REGISTERED_NAME_2)
	db.session.add(user)
	db.session.commit()
	return user


@pytest.fixture
def logged_in_client_2(client, registered_user_2, REGISTERED_EMAIL_2, REGISTERED_PASSWORD_2):
	with client:
		client.post("/api/sessions", json={
				"email": REGISTERED_EMAIL_2,
				"password": REGISTERED_PASSWORD_2
			})
		return client


@pytest.fixture
def registered_game(app):
	game = Game(name="Vronsky Goes to Vilnius")
	game.first_release_date = datetime.date(2001, 10, 17)
	game.cover_url = "https://lh3.googleusercontent.com/pw/ACtC-3eicHUJ5K4-IhtNDNkxg769Wpr97rLbPTgI2q-N914HEeSMDzULIObKehGHAGA7dyEz6l-1JmLQF0wfJ_1qmQ6QhUcwuf_VWu5X9cUKTQkVKFtbUrt2L4SyQ1xUcF8PFpnGceLBDlBhiNcYQpET3d7y5A=w678-h903-no"
	game.cover_width = 677
	game.cover_height = 903
	game.summary = "A great time was had by all!"
	game.igdb_id = 123
	game.igdb_url = "https://www.igdb.com/games/comrade-vronsky"
	game.twitch_id = 456
	db.session.add(game)
	db.session.commit()
	return game

@pytest.fixture
def registered_game_2(app):
	game = Game(name="Boopa Goes to Brussels")
	game.first_release_date = datetime.date(2003, 12, 25)
	game.cover_url = "https://lh3.googleusercontent.com/pw/ACtC-3fsYBGcHnaETZTmuXaURnL9jhDRquubJHt6QhYhZlUesjUkpuEUcNaDi9GIo2oyO6UiW1zxVOZDcoj3iP7Lp163jfhNBQsog2-mf9vOf43_d4gJAdk0gYGnhp0nIp2_yeqk8RJ5JGYIR_v48M7mmsZgew=w678-h903-no"
	game.cover_width = 677
	game.cover_height = 903
	game.summary = "The little guy tagged along."
	game.igdb_id = 876
	game.igdb_url = "https://www.igdb.com/games/boopa-boo"
	game.twitch_id = 321
	db.session.add(game)
	db.session.commit()
	return game
