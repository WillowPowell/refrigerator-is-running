import datetime
import json

from requests.exceptions import HTTPError

from flask import abort, Blueprint, request

from refrigerator.app import db
from refrigerator.models.games import Game
from refrigerator.utils.response import make_response, validate_data

bp = Blueprint('games', __name__, url_prefix='/api/games')


@bp.route('', methods=("POST", "GET"))
def games():
	"""Interact with the games endpoint."""
	if request.method == "POST":
		return post_game(request)

	return get_games(request)


@validate_data(
	required_fields=("name", ),
	date_fields=("first_release_date", ),
	name=str,
	first_release_date=str,
	cover_url=str,
	cover_width=int,
	cover_height=int,
	summary=str,
	igdb_id=int,
	igdb_url=str,
	twitch_id=int)
def post_game(request):
	"""Create a custom game."""
	status_code = 201
	success = True
	data = {}
	errors = []

	#Check for conflicts.
	if 'twitch_id' in request.json:
		conflict = Game.query.filter_by(twitch_id=request.json['twitch_id']).first()
		if conflict:
			errors.append(f"Game {conflict.id} With Twitch ID {request.json['twitch_id']} Exists")
	if 'igdb_id' in request.json:
		conflict = Game.query.filter_by(igdb_id=request.json['igdb_id']).first()
		if conflict:
			errors.append(f"Game {conflict.id} With IGDB ID {request.json['igdb_id']} Exists")
	if errors:
		abort(409, errors)

	# If the data is good, make and return the game.
	data = Game.make_game(request.json).to_response_dict()
	return make_response(status_code, success, data, errors)


@validate_data(
	required_fields=("query", "limit", "offset"),
	constraints=(("limit", lambda x: x <= 30, "limit Exceeds Maximum 30."), ),
	query_string=True,
	query=str,
	limit=int,
	offset=int)
def get_games(request):
	"""
	Search for Games!

	Search results are populated from the IGDB "/games" endpoint.
	"""
	status_code = 200
	success = True
	try:
		data = Game.search(query=request.args['query'], limit=request.args['limit'], offset=request.args['offset'])
	except HTTPError:
		abort(503, "Unable To Connect To IGDB.")
	if not data:
		abort(404, "No Results Found.")
	return make_response(data=data)
