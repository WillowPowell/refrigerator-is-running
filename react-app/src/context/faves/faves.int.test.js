import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { act, renderHook } from '@testing-library/react-hooks';

import { AuthProvider } from 'context/auth';

import { withCover as fave, getFavesResults as faves } from 'fixtures/fave';

import { FavesProvider, useFavesContext } from './context';

import initialState from './initialState';

const wrapper = ({ testInitialState = null, children } = {}) => (
  <MemoryRouter>
    <AuthProvider>
      <FavesProvider testInitialState={testInitialState}>
        {children}
      </FavesProvider>
    </AuthProvider>
  </MemoryRouter>
);

const testState = {
  ...initialState,
  faves,
};
const updatedFave = {
  ...fave,
  phrase: 'It is even better than I thought.',
};
const index = 0;

it('renders with initial state', () => {
  const { result } = renderHook(() => useFavesContext(), { wrapper });
  expect(result.current.favesState).toEqual({
    ...initialState,
    userId: null,
  });
});

it('calls the standard reducer', () => {
  const { result } = renderHook(() => useFavesContext(), { wrapper });
  act(() => {
    result.current.favesDispatch({
      type: 'SET_SESSION_CHECKED',
      payload: true,
    });
  });
  expect(result.current.favesState.sessionChecked).toEqual(true);
});

it('sets a single fave', () => {
  const { result } = renderHook(() => useFavesContext(), {
    wrapper,
    initialProps: { testInitialState: testState },
  });
  act(() => {
    result.current.favesDispatch({
      type: 'SET_FAVE',
      index,
      payload: updatedFave,
    });
  });
  expect(result.current.favesState.faves[0]).toEqual(updatedFave);
});
