/* global customRender */
import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import CardHead from './CardHead';

import Modal from '.';

const setActive = jest.fn();

function renderModal(isCard = false, children = <div />) {
  customRender((
    <Modal
      active
      isCard={isCard}
      name="My Modal"
      setActive={setActive}
    >
      {children}
    </Modal>
  ));
}

it('closes when you press escape', () => {
  renderModal();
  fireEvent.keyDown(window, { key: 'Escape', code: 'Escape' });
  expect(setActive).toHaveBeenCalledWith(false);
});

it('closes when you click the x', () => {
  renderModal();
  userEvent.click(screen.getByRole('button', { name: 'Close Modal' }));
  expect(setActive).toHaveBeenCalledWith(false);
});

it('closes when you click the background', () => {
  renderModal();
  userEvent.click(screen.getByTestId('modalBackground'));
  expect(setActive).toHaveBeenCalledWith(false);
});

it('card closes when you click the x', () => {
  renderModal(true, <CardHead setActive={setActive} title="My Card Head" />);
  userEvent.click(screen.getByRole('button', { name: 'Close Modal' }));
  expect(setActive).toHaveBeenCalledWith(false);
});
