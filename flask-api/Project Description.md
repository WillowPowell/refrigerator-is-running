ORIGINAL FRIDGE PROJECT

Build a website that lets you keep track of the food in your refrigerator
Eventually you’ll build a recipe app telling you what you can cook based on what’s in your fridge

Do it in GitHub or on GitLabs (not owned by Microsoft)
Create a repository that we both have access to
I’ll submit pull requests and he’ll review my code

1. Add food to fridge
2. Give name, description, expiration date
3. Page that adds food
4. Page to view fridge – doesn’t show expired food

Should look decent
Use Flask (py framework)– documentation is great
-generate HTML, handle form requests

Bulma (CSS framework)

NO JAVASCRIPT (not needed)
-need to learn what you need js for and what you don’t

Need a database – need to store the elements
Use SQLite
-in memory database
-this will be hard

Reach out when I get stuck

GAME STATION STARTER PROJECT

Do these in order:

1. /myGame should only allow you to put in values from a list of games

2. List of games fetched from database