/* global customRender */
/* global mockReplaceHistory */
import React from 'react';
import { screen, waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { login } from 'utils/api';
import { registered } from 'fixtures/user';

import LoginForm from './LoginForm';

jest.mock('utils/api');

async function fillOutForm(eml, pwd) {
  userEvent.type(screen.getByLabelText(/password/i), pwd);
  userEvent.type(screen.getByRole('textbox', { name: 'Email' }), eml);
  return waitFor(() => expect(screen.getByRole('button', { name: 'Submit' })).not.toHaveAttribute('disabled'));
}

async function submit() {
  login.mockImplementation(async (dispatch, email, password) => {
    const response = { ok: false };
    const responseJSON = { errors: ['Unexpected Error.'] };
    if (email === registered.email) {
      if (password === registered.password) {
        response.ok = true;
        responseJSON.data = registered.data;
        dispatch({
          type: 'SET_USER',
          payload: registered.data,
        });
      } else if (password === 'notmypassword') {
        responseJSON.errors = ['Authentication Failed.'];
      }
    }
    return [response, responseJSON];
  });
  userEvent.click(screen.getByRole('button', { name: 'Submit' }));
  return waitFor(() => expect(login).toHaveBeenCalled());
}

it('renders', () => {
  customRender(<LoginForm />, {
    authState: {},
  });
  expect(screen.getByText(/log in/i)).toBeInTheDocument();
  expect(screen.getByRole('textbox', { name: 'Email' })).toBeInTheDocument();
  expect(screen.getByLabelText(/password/i)).toBeInTheDocument();
  expect(screen.getByRole('button', { name: 'Submit' })).toBeInTheDocument();
  expect(screen.getByRole('link', { name: 'Register' })).toBeInTheDocument();
});

it('handles a successful login', async () => {
  customRender(<LoginForm />, { authState: {} });
  await fillOutForm(registered.email, registered.password);
  await submit();
  expect(login).toHaveBeenCalledWith(
    expect.any(Function),
    registered.email,
    registered.password,
  );
  expect(mockReplaceHistory).toHaveBeenCalledWith('/');
});

it('handles a successful login with a redirect target', async () => {
  customRender(<LoginForm />, {
    authState: {},
    route: `/?target=${encodeURIComponent('/selidor')}`,
  });
  await fillOutForm(registered.email, registered.password);
  await submit();
  expect(mockReplaceHistory).toHaveBeenCalledWith('/selidor');
});

it('handles a failed login', async () => {
  customRender(<LoginForm />, { authState: {} });
  await fillOutForm(registered.email, 'notmypassword');
  await submit();
  expect(window.alert).toHaveBeenCalledWith('Authentication Failed.');
});
