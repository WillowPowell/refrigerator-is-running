const customGame = {
  cover_height: null,
  cover_url: null,
  cover_url_big: null,
  cover_width: null,
  created_at: 'Sun, 04 Apr 2021 09:33:12 UTC',
  first_release_date: 'Sun, 04 Apr 2021',
  id: 124124,
  igdb_id: null,
  igdb_url: null,
  name: 'Vronsky Goes To Vilnius',
  summary: 'It was a jolly good time.',
  twitch_id: null,
  updated_at: null,
};

export default customGame;
