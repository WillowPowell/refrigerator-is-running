/* global mockPushHistory */
import fetchMock from 'fetch-mock-jest';
import React from 'react';
import { MemoryRouter } from 'react-router';
import { act, renderHook } from '@testing-library/react-hooks';

import { registered } from 'fixtures/user';

import { AuthProvider, useAuthContext } from './context';
import useAuthFetch from './useAuthFetch';

const wrapper = ({ children, initialAuthState, initialEntry = '/' } = {}) => (
  <MemoryRouter initialEntries={[initialEntry]}>
    <AuthProvider testInitialState={initialAuthState}>
      {children}
    </AuthProvider>
  </MemoryRouter>
);

afterEach(() => {
  fetchMock.restore();
});

it('sends the call just like fetch, and returns the response', async () => {
  fetchMock.get('/api/mentors', {
    data: 'starets Zosima',
  }, {
    headers: { 'Content-Type': 'application/json' },
  });
  const { result } = renderHook(() => ({ authFetch: useAuthFetch() }), { wrapper });
  let response = null;
  let responseJSON = null;
  await act(async () => {
    response = await result.current.authFetch('/api/mentors', {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
      body: {
        student: 'Alyosha',
      },
    });
    responseJSON = await response.json();
  });
  expect(response.ok).toBe(true);
  expect(responseJSON.data).toEqual('starets Zosima');
});

it('dispatches CLEAR_USER and pushes to login route with target on 401', async () => {
  fetchMock.get('/api/mentors', 401);
  const startingLocation = '/province/of/K';
  // Not quite a unit test, but useAuthFetch is nothing without useAuthContext.
  const { result } = renderHook(() => ({
    authFetch: useAuthFetch(),
    authContext: useAuthContext(),
  }), {
    wrapper,
    initialProps: {
      initialAuthState: { user: registered.data },
      initialEntry: startingLocation,
    },
  });
  await act(() => result.current.authFetch('/api/mentors', { method: 'GET' }));
  expect(result.current.authContext.authState.user).toBe(null);
  expect(mockPushHistory).toHaveBeenCalledWith(`/user/login?target=${
    encodeURIComponent(startingLocation)
  }`);
});
