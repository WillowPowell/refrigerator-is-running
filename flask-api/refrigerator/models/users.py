import re
import datetime

from refrigerator.app import db
from refrigerator.utils.time import DATETIME_FORMAT

class User(db.Model):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String, unique=True, nullable=False)
	password = db.Column(db.String, nullable=False)
	name = db.Column(db.String, nullable=False)
	verified = db.Column(db.Boolean, nullable=False, default=False)
	created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now())
	updated_at = db.Column(db.DateTime, onupdate=datetime.datetime.now())

	faves = db.relationship("Fave", back_populates="user")
	
	def __repr__(self):
		return '<User %r>' %self.email

	def to_response_dict(self):
		return {
		"id" : self.id,
		"email" : self.email,
		"name" : self.name,
		"verified" : self.verified,
		"created_at" : datetime.datetime.strftime(self.created_at, DATETIME_FORMAT),
		"updated_at" : datetime.datetime.strftime(self.created_at, DATETIME_FORMAT) if self.updated_at else None
	}

def password_strength(password):
	"""
	Verify the strength of password
	A password is considered strong if it has 4 of the following 5 traits:
		8 characters length or more
		1 digit or more
		1 symbol or more
		1 uppercase letter or more
		1 lowercase letter or more
	:return: True if username valid, False if invalid
	"""

	#calculate the length
	length_fail = len(password) < 7

	#searching for digits
	digit_fail = re.search(r"\d", password) is None

	#searching for uppercase
	uppercase_fail = re.search(r"[A-Z]", password) is None

	#searching for lowercase
	lowercase_fail = re.search(r"[a-z]", password) is None

	#searching for symbols
	symbol_fail = re.search(r"\W", password) is None

	#overall result
	failures = (length_fail, digit_fail, uppercase_fail, lowercase_fail, symbol_fail)

	count_failures = sum(1 for ct in failures if ct)

	if count_failures > 1:
		return False
	else:
		return True
