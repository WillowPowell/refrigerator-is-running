import addFave from './addFave';
import checkSession from './checkSession';
import deleteFave from './deleteFave';
import getFaves from './getFaves';
import login from './login';
import logout from './logout';
import patchFave from './patchFave';
import register from './register';
import searchGames from './searchGames';

export {
  addFave,
  checkSession,
  deleteFave,
  getFaves,
  login,
  logout,
  patchFave,
  register,
  searchGames,
};
