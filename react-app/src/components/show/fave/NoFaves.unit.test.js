/* global customRender */
import React from 'react';
import { screen } from '@testing-library/dom';

import { appIconLarge } from 'utils';

import NoFaves from './NoFaves';

beforeEach(() => {
  customRender(<NoFaves />);
});

it('renders', () => {
  expect(screen.getByText(/congratulations!/i)).toBeInTheDocument();
  expect(screen.queryAllByRole('img', { name: appIconLarge.alt })[0]).toBeInTheDocument();
});
