/* global customRender */
import React from 'react';
import { screen, waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { phraseStyles } from './styles';

import PrettyPhrase from '.';

const phrase = "I'm just rabid about this game; absolutely feral.";

beforeEach(() => {
  customRender(<PrettyPhrase text={phrase} />);
});

it('renders the first style', () => {
  expect(screen.getByRole('figure', {
    name: `The phrase "${phrase}" in ${phraseStyles[0].name} style.`,
  })).toBeInTheDocument();
});

it('changes styles', async () => {
  userEvent.click(screen.getByRole('button', { name: 'View Next Style' }));
  await waitFor(() => expect(screen.queryByRole('figure', {
    name: `The phrase "${phrase}" in ${phraseStyles[1].name} style.`,
  })).toBeInTheDocument());
});

it('goes back to the first style after cycling through them all', async () => {
  for (let i = 0; i < phraseStyles.length - 1; i += 1) {
    userEvent.click(screen.getByRole('button', { name: 'View Next Style' }));
    // eslint-disable-next-line no-await-in-loop
    await waitFor(() => expect(screen.queryByRole('figure', {
      name: `The phrase "${phrase}" in ${phraseStyles[i + 1].name} style.`,
    })).toBeInTheDocument());
  }
  userEvent.click(screen.getByRole('button', { name: 'View Next Style' }));
  await waitFor(() => expect(screen.queryByRole('figure', {
    name: `The phrase "${phrase}" in ${phraseStyles[0].name} style.`,
  })).toBeInTheDocument());
});
