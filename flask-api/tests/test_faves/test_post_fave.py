import datetime

from refrigerator.models.faves import Fave
from refrigerator.utils.time import check_datetime_format

from tests.utils import check_response

GAME_ID_TO_ADD = 1
PHRASE_TO_ADD = "I like that the cat is named after that rascal from that book."
HOURS_PLAYED_TO_ADD = 100
TOP_FAVE_TO_ADD = False

#Helpers
def post_fave(client, body):
	return client.post("/api/faves", json=body)

#Tests
def test_success(logged_in_client, registered_user, registered_game):
	response = post_fave(logged_in_client, {
		"game_id" : GAME_ID_TO_ADD,
		"top_fave" : TOP_FAVE_TO_ADD,
		"phrase" : PHRASE_TO_ADD,
		"hours_played" : HOURS_PLAYED_TO_ADD
	})
	check_response(response, 201)
	fave = response.json.get("data")
	assert fave
	assert "id" in fave
	assert fave["user_id"] == registered_user.id
	assert fave["game_id"] == GAME_ID_TO_ADD
	assert fave["top_fave"] == TOP_FAVE_TO_ADD
	assert fave["phrase"] == PHRASE_TO_ADD
	assert fave["hours_played"] == HOURS_PLAYED_TO_ADD
	assert check_datetime_format(fave["created_at"])
	assert fave["updated_at"] == None
	assert fave["deleted_at"] == None
	fave = Fave.query.get(fave["id"])
	assert fave
	assert repr(fave) == f"<Fave {fave.id}>"

def test_game_not_found(logged_in_client, registered_user, registered_game):
	response = post_fave(logged_in_client, {
		"game_id" : GAME_ID_TO_ADD + 1,
		"top_fave" : TOP_FAVE_TO_ADD,
		"phrase" : PHRASE_TO_ADD,
		"hours_played" : HOURS_PLAYED_TO_ADD
	})
	check_response(response, 404, error="Game Not Found.", success=False)

def test_incorrect_data_type(logged_in_client, registered_user, registered_game):
	response = post_fave(logged_in_client, {
		"game_id" : GAME_ID_TO_ADD,
		"top_fave" : f"{TOP_FAVE_TO_ADD}",
		"phrase" : PHRASE_TO_ADD,
		"hours_played" : HOURS_PLAYED_TO_ADD
	})
	check_response(response, 400, error="Incorrect format for top_fave. Expected <class 'bool'>.", success=False)
	response = post_fave(logged_in_client, {
		"game_id" : GAME_ID_TO_ADD,
		"top_fave" : TOP_FAVE_TO_ADD,
		"phrase" : 100,
		"hours_played" : HOURS_PLAYED_TO_ADD
	})
	check_response(response, 400, error="Incorrect format for phrase. Expected <class 'str'>.", success=False)

def test_missing_required_fields(logged_in_client, registered_user):
	response = post_fave(logged_in_client, {
		"top_fave" : "False",
		"phrase" : PHRASE_TO_ADD,
		"hours_played" : 100
	})
	check_response(response, 400, error="No game_id Provided.", success=False)

def test_not_logged_in(client):
	response = post_fave(client, {
		"game_id" : GAME_ID_TO_ADD,
		"top_fave" : TOP_FAVE_TO_ADD,
		"phrase" : PHRASE_TO_ADD,
		"hours_played" : HOURS_PLAYED_TO_ADD
	})
	check_response(response, 401, error="Unauthorized.", success=False)

def test_duplicate_request(logged_in_client, registered_fave):
	"""If multiple identical requests are sent, just returns the existing Fave."""
	response = post_fave(logged_in_client, {
		"game_id" : GAME_ID_TO_ADD,
		"top_fave" : TOP_FAVE_TO_ADD,
		"phrase" : PHRASE_TO_ADD,
		"hours_played" : HOURS_PLAYED_TO_ADD
	})
	check_response(response, 201)
	assert response.json["data"]["id"] == registered_fave.id

def test_conflict(logged_in_client, registered_fave, registered_user):
	"""
	If a conflicting Fave is found, and data differs from existing Fave,
	should return a 409 response.
	"""
	response = post_fave(logged_in_client, {
		"game_id" : GAME_ID_TO_ADD,
		"top_fave" : TOP_FAVE_TO_ADD,
		"phrase" : "I can't remember if I've told you this before, but I really like the cat's name!",
		"hours_played" : HOURS_PLAYED_TO_ADD
	})
	check_response(response, 409, success=False,
		error=f"Conflict with fave {registered_fave.id}. Has game_id {GAME_ID_TO_ADD} and user_id {registered_user.id}.")

def test_restore_deleted(logged_in_client, deleted_fave):
	"""If a conflicting Fave was deleted, behaves the same way as adding a new fave."""
	UPDATED_PHRASE = "I can't remember if I've told you this before, but I really like the cat's name!"
	response = post_fave(logged_in_client, {
		"game_id" : GAME_ID_TO_ADD,
		"top_fave" : TOP_FAVE_TO_ADD,
		"phrase" : UPDATED_PHRASE,
		"hours_played" : HOURS_PLAYED_TO_ADD
	})
	check_response(response, 201)
	assert response.json["data"]["id"] == deleted_fave.id
	assert response.json["data"]["phrase"] == UPDATED_PHRASE
