/* global customRender */
/* global expectComponentMounted */
/* global mockComponents */
/* global mockReplaceHistory */
import React from 'react';
import { act } from 'react-dom/test-utils';
import { useParams as mockUseParams } from 'react-router-dom';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { placeholderImage } from 'utils';

import {
  customGame,
  lotsOfFaves,
  noCover,
  withCover,
} from 'fixtures/fave';

import EditFaveModal from './EditFaveModal';
import ViewFave from './ViewFave';

jest.mock('./EditFaveModal');

const mockIndex = 0;

beforeEach(() => {
  mockUseParams.mockImplementation(() => ({ index: mockIndex }));
  mockComponents({ EditFaveModal });
});

it('renders a fave with a cover', () => {
  customRender(<ViewFave />, {
    authState: {},
    favesState: { faves: [withCover] },
  });
  const cover = screen.getByRole('img', { name: `${withCover.game.name} Cover` });
  expect(cover).toBeInTheDocument();
  expect(cover.src).toBe(withCover.game.cover_url_big);
  expect(cover.height).toEqual(withCover.game.cover_height);
  expect(cover.width).toEqual(withCover.game.cover_width);
  // ExcitementGenerator
  expect(screen.getByRole('heading', { name: withCover.game.name })).toBeInTheDocument();
  expect(screen.getByText(`Added as a fave on ${withCover.created_at.split(' ', 4).join(' ')}.`)).toBeInTheDocument();
  expect(screen.getByText(`You've played for ${withCover.hours_played} hours.`)).toBeInTheDocument();
  expect(screen.getByRole('figure', { name: new RegExp(withCover.phrase) })).toBeInTheDocument();
  // GameInfo
  const firstReleasedRow = screen.queryByRole('row', { name: 'First Released' });
  expect(firstReleasedRow).toBeInTheDocument();
  expect(firstReleasedRow.children[1].innerHTML).toEqual(withCover.game.first_release_date);
  const summaryRow = screen.queryByRole('row', { name: 'Summary' });
  expect(summaryRow).toBeInTheDocument();
  expect(summaryRow.children[1].children[0].innerHTML).toEqual(withCover.game.summary);
  expect(screen.queryByRole('link', { name: 'More Info on IGDB' }).href).toEqual(withCover.game.igdb_url);
  // Edit Fave Modal
  expect(screen.getByRole('button', { name: 'Edit Fave' })).toBeInTheDocument();
  expectComponentMounted({ EditFaveModal }, {
    props: {
      fave: withCover,
      index: mockIndex,
      modalActive: false,
    },
    anyFuncProps: ['setModalActive'],
  });
});

it('renders a fave with no cover', () => {
  customRender(<ViewFave />, {
    authState: {},
    favesState: { faves: [noCover] },
  });
  const placeholder = screen.getByRole('img', { name: placeholderImage.alt });
  expect(placeholder).toBeInTheDocument();
  expect(placeholder.src).toContain(placeholderImage.src);
  expect(placeholder.height).toEqual(placeholder.height);
  expect(placeholder.width).toEqual(placeholder.width);
});

it('renders a fave with a custom game', () => {
  customRender(<ViewFave />, {
    authState: {},
    favesState: { faves: [customGame] },
  });
  expect(screen.getByText('This is a custom game.')).toBeInTheDocument();
});

it('pushes to Home if the index is bad', () => {
  mockUseParams.mockImplementation(() => ({
    index: lotsOfFaves.length,
  }));
  customRender(<ViewFave />, {
    authState: {},
    favesState: {
      faves: lotsOfFaves,
      sessionChecked: true,
    },
  });
  expect(mockReplaceHistory).toHaveBeenCalledWith('/');
});

it('pushes to Home if there are no faves', () => {
  customRender(<ViewFave />, {
    authState: {},
    favesState: {
      sessionChecked: true,
    },
  });
  expect(mockReplaceHistory).toHaveBeenCalledWith('/');
});

it('does not push to Home if not sessionChecked', () => {
  customRender(<ViewFave />, {
    authState: {},
    favesState: {},
  });
  expect(mockReplaceHistory).not.toHaveBeenCalled();
});

it('opens the EditFaveModal when edit button is clicked', () => {
  customRender(<ViewFave />, {
    authState: {},
    favesState: { faves: [withCover] },
  });
  act(() => userEvent.click(screen.getByRole('button', { name: 'Edit Fave' })));
  expectComponentMounted({ EditFaveModal }, {
    props: {
      fave: withCover,
      index: mockIndex,
      modalActive: true,
    },
    anyFuncProps: ['setModalActive'],
  });
});
