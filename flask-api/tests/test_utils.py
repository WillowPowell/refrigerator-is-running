import pytest

from refrigerator.models.faves import Fave
from refrigerator.models.games import Game
from refrigerator.utils.response import owns_resource


# Helpers
@owns_resource(Game)
def my_game(games_id=1):
	"""This will raise because the Game resource does not have a user_id column."""
	pass


@owns_resource(Fave)
def my_fave(id=0):
	"""This will raise because the argument is not named after the faves table."""
	pass


# Tests
def test_owns_resource_errors(client):
	"""Test the errors produced by the owns resource wrapper."""
	with pytest.raises(
			TypeError,
			match="requires the resource to contain a user_id column"):
		my_game()
	with pytest.raises(
			TypeError,
			match="requires the wrapped function to have a keyword argument that matches {resource.__tablename__}_id"):
		my_fave(id=1)
