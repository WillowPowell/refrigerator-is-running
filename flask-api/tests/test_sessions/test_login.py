from flask import session

from tests.utils import check_response, check_user


#Helpers
def login(client, body):
	return client.post("/api/sessions", json=body)


#Tests
def test_success(client, registered_user, REGISTERED_EMAIL, REGISTERED_PASSWORD, REGISTERED_NAME):
	with client:
		response = login(client, {
				"email": REGISTERED_EMAIL,
				"password": REGISTERED_PASSWORD
			})
		assert session["user_id"] == registered_user.id
	check_response(response, 201)
	check_user(response, REGISTERED_EMAIL, REGISTERED_NAME)


def test_invalid_credentials(client, registered_user, REGISTERED_EMAIL, REGISTERED_PASSWORD):
	with client:
		response = login(client, {
		"email": "thatsnotmyemail@example.com",
		"password": REGISTERED_PASSWORD
		})
		assert not session
	check_response(response, 401, success=False, error="Authentication Failed.")
	with client:
		response = login(client, {
		"email": REGISTERED_EMAIL,
		"password": "thatsnotmypassword!"
		})
		assert not session
	check_response(response, 401, success=False, error="Authentication Failed.")


def test_missing_fields(client, registered_user, REGISTERED_EMAIL, REGISTERED_PASSWORD):
	with client:
		response = login(client, {
			"password": REGISTERED_PASSWORD
			})
		assert not session
	check_response(response, 400, success=False, error="Required Field email Not Provided.")
	with client:
		assert not session
		response = login(client, {
		"email": REGISTERED_EMAIL
		})
		assert not session
	check_response(response, 400, success=False, error="Required Field password Not Provided.")


def test_bad_data(client, REGISTERED_EMAIL, REGISTERED_PASSWORD):
	with client:
		response = login(client, {
			"email": 1234,
			"password": REGISTERED_PASSWORD
		})
		assert not session
	check_response(response, 400, success=False, error="Incorrect format for email. Expected <class 'str'>.")
	with client:
		response = login(client, {
			"email": REGISTERED_EMAIL,
			"password": 9101112
		})
		assert not session
	check_response(response, 400, success=False, error="Incorrect format for password. Expected <class 'str'>.")
